/**
 * generated by Xtext 2.25.0
 */
package org.xtext.example.methodname.methodName.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.xtext.example.methodname.methodName.CardinalityModifier;
import org.xtext.example.methodname.methodName.Expr;
import org.xtext.example.methodname.methodName.GroupExpr;
import org.xtext.example.methodname.methodName.MethodNamePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Group Expr</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.example.methodname.methodName.impl.GroupExprImpl#isHasNot <em>Has Not</em>}</li>
 *   <li>{@link org.xtext.example.methodname.methodName.impl.GroupExprImpl#getExpr <em>Expr</em>}</li>
 *   <li>{@link org.xtext.example.methodname.methodName.impl.GroupExprImpl#getCard <em>Card</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GroupExprImpl extends ExprImpl implements GroupExpr
{
  /**
   * The default value of the '{@link #isHasNot() <em>Has Not</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isHasNot()
   * @generated
   * @ordered
   */
  protected static final boolean HAS_NOT_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isHasNot() <em>Has Not</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isHasNot()
   * @generated
   * @ordered
   */
  protected boolean hasNot = HAS_NOT_EDEFAULT;

  /**
   * The cached value of the '{@link #getExpr() <em>Expr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExpr()
   * @generated
   * @ordered
   */
  protected Expr expr;

  /**
   * The default value of the '{@link #getCard() <em>Card</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCard()
   * @generated
   * @ordered
   */
  protected static final CardinalityModifier CARD_EDEFAULT = CardinalityModifier.CARD_1;

  /**
   * The cached value of the '{@link #getCard() <em>Card</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCard()
   * @generated
   * @ordered
   */
  protected CardinalityModifier card = CARD_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected GroupExprImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MethodNamePackage.Literals.GROUP_EXPR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean isHasNot()
  {
    return hasNot;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setHasNot(boolean newHasNot)
  {
    boolean oldHasNot = hasNot;
    hasNot = newHasNot;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodNamePackage.GROUP_EXPR__HAS_NOT, oldHasNot, hasNot));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Expr getExpr()
  {
    return expr;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExpr(Expr newExpr, NotificationChain msgs)
  {
    Expr oldExpr = expr;
    expr = newExpr;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MethodNamePackage.GROUP_EXPR__EXPR, oldExpr, newExpr);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setExpr(Expr newExpr)
  {
    if (newExpr != expr)
    {
      NotificationChain msgs = null;
      if (expr != null)
        msgs = ((InternalEObject)expr).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MethodNamePackage.GROUP_EXPR__EXPR, null, msgs);
      if (newExpr != null)
        msgs = ((InternalEObject)newExpr).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MethodNamePackage.GROUP_EXPR__EXPR, null, msgs);
      msgs = basicSetExpr(newExpr, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodNamePackage.GROUP_EXPR__EXPR, newExpr, newExpr));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public CardinalityModifier getCard()
  {
    return card;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setCard(CardinalityModifier newCard)
  {
    CardinalityModifier oldCard = card;
    card = newCard == null ? CARD_EDEFAULT : newCard;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodNamePackage.GROUP_EXPR__CARD, oldCard, card));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MethodNamePackage.GROUP_EXPR__EXPR:
        return basicSetExpr(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MethodNamePackage.GROUP_EXPR__HAS_NOT:
        return isHasNot();
      case MethodNamePackage.GROUP_EXPR__EXPR:
        return getExpr();
      case MethodNamePackage.GROUP_EXPR__CARD:
        return getCard();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MethodNamePackage.GROUP_EXPR__HAS_NOT:
        setHasNot((Boolean)newValue);
        return;
      case MethodNamePackage.GROUP_EXPR__EXPR:
        setExpr((Expr)newValue);
        return;
      case MethodNamePackage.GROUP_EXPR__CARD:
        setCard((CardinalityModifier)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MethodNamePackage.GROUP_EXPR__HAS_NOT:
        setHasNot(HAS_NOT_EDEFAULT);
        return;
      case MethodNamePackage.GROUP_EXPR__EXPR:
        setExpr((Expr)null);
        return;
      case MethodNamePackage.GROUP_EXPR__CARD:
        setCard(CARD_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MethodNamePackage.GROUP_EXPR__HAS_NOT:
        return hasNot != HAS_NOT_EDEFAULT;
      case MethodNamePackage.GROUP_EXPR__EXPR:
        return expr != null;
      case MethodNamePackage.GROUP_EXPR__CARD:
        return card != CARD_EDEFAULT;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (hasNot: ");
    result.append(hasNot);
    result.append(", card: ");
    result.append(card);
    result.append(')');
    return result.toString();
  }

} //GroupExprImpl
