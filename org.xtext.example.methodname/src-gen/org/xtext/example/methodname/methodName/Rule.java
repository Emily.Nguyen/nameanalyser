/**
 * generated by Xtext 2.25.0
 */
package org.xtext.example.methodname.methodName;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.example.methodname.methodName.Rule#getName <em>Name</em>}</li>
 *   <li>{@link org.xtext.example.methodname.methodName.Rule#getKind <em>Kind</em>}</li>
 *   <li>{@link org.xtext.example.methodname.methodName.Rule#getDecls <em>Decls</em>}</li>
 *   <li>{@link org.xtext.example.methodname.methodName.Rule#getCompOp <em>Comp Op</em>}</li>
 *   <li>{@link org.xtext.example.methodname.methodName.Rule#getCompValue <em>Comp Value</em>}</li>
 * </ul>
 *
 * @see org.xtext.example.methodname.methodName.MethodNamePackage#getRule()
 * @model
 * @generated
 */
public interface Rule extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.xtext.example.methodname.methodName.MethodNamePackage#getRule_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.xtext.example.methodname.methodName.Rule#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Kind</b></em>' attribute.
   * The literals are from the enumeration {@link org.xtext.example.methodname.methodName.Kind}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Kind</em>' attribute.
   * @see org.xtext.example.methodname.methodName.Kind
   * @see #setKind(Kind)
   * @see org.xtext.example.methodname.methodName.MethodNamePackage#getRule_Kind()
   * @model
   * @generated
   */
  Kind getKind();

  /**
   * Sets the value of the '{@link org.xtext.example.methodname.methodName.Rule#getKind <em>Kind</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Kind</em>' attribute.
   * @see org.xtext.example.methodname.methodName.Kind
   * @see #getKind()
   * @generated
   */
  void setKind(Kind value);

  /**
   * Returns the value of the '<em><b>Decls</b></em>' reference list.
   * The list contents are of type {@link org.xtext.example.methodname.methodName.Declaration}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Decls</em>' reference list.
   * @see org.xtext.example.methodname.methodName.MethodNamePackage#getRule_Decls()
   * @model
   * @generated
   */
  EList<Declaration> getDecls();

  /**
   * Returns the value of the '<em><b>Comp Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Comp Op</em>' attribute.
   * @see #setCompOp(String)
   * @see org.xtext.example.methodname.methodName.MethodNamePackage#getRule_CompOp()
   * @model
   * @generated
   */
  String getCompOp();

  /**
   * Sets the value of the '{@link org.xtext.example.methodname.methodName.Rule#getCompOp <em>Comp Op</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Comp Op</em>' attribute.
   * @see #getCompOp()
   * @generated
   */
  void setCompOp(String value);

  /**
   * Returns the value of the '<em><b>Comp Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Comp Value</em>' attribute.
   * @see #setCompValue(int)
   * @see org.xtext.example.methodname.methodName.MethodNamePackage#getRule_CompValue()
   * @model
   * @generated
   */
  int getCompValue();

  /**
   * Sets the value of the '{@link org.xtext.example.methodname.methodName.Rule#getCompValue <em>Comp Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Comp Value</em>' attribute.
   * @see #getCompValue()
   * @generated
   */
  void setCompValue(int value);

} // Rule
