package org.xtext.example.methodname.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.methodname.services.MethodNameGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMethodNameParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'declarations'", "'rules'", "'cases'", "'<->'", "'def'", "'for'", "'{'", "'filter'", "'||'", "'such'", "'that'", "'size'", "'>'", "'<'", "'=='", "'<='", "'>='", "'}'", "'declare'", "'case'", "'|'", "'.'", "'...'", "'!'", "'('", "')'", "'['", "']'", "'#'", "'interface'", "'package'", "'class'", "'method'", "'variable'", "'SENT_UNSPECIFIED'", "'neutral'", "'positive'", "'negative'", "'CC'", "'CD'", "'DT'", "'EX'", "'FW'", "'IN'", "'JJ'", "'JJR'", "'JJS'", "'LS'", "'MD'", "'NN'", "'NNS'", "'NNP'", "'NNPS'", "'PDT'", "'POS'", "'PRP'", "'RB'", "'RBR'", "'RBS'", "'RP'", "'SYM'", "'TO'", "'UH'", "'VB'", "'VBD'", "'VBG'", "'VBN'", "'VBP'", "'VBZ'", "'WDT'", "'WP'", "'WRB'", "'CARD_1'", "'*'", "'+'", "'?'", "'always'", "'very-often'", "'often'", "'rarely'", "'very-seldom'", "'never'"
    };
    public static final int T__50=50;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=4;
    public static final int RULE_INT=6;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__67=67;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__91=91;
    public static final int T__92=92;
    public static final int T__90=90;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__74=74;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__88=88;
    public static final int T__89=89;
    public static final int T__84=84;
    public static final int T__85=85;
    public static final int T__86=86;
    public static final int T__87=87;

    // delegates
    // delegators


        public InternalMethodNameParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMethodNameParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMethodNameParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMethodName.g"; }



     	private MethodNameGrammarAccess grammarAccess;

        public InternalMethodNameParser(TokenStream input, MethodNameGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Model";
       	}

       	@Override
       	protected MethodNameGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleModel"
    // InternalMethodName.g:65:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // InternalMethodName.g:65:46: (iv_ruleModel= ruleModel EOF )
            // InternalMethodName.g:66:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalMethodName.g:72:1: ruleModel returns [EObject current=null] : (otherlv_0= 'declarations' ( (lv_declarations_1_0= ruleDeclaration ) )* ( (lv_fixedDeclarations_2_0= ruleFixedDeclaration ) )+ otherlv_3= 'rules' ( (lv_rules_4_0= ruleRule ) )* otherlv_5= 'cases' ( (lv_elements_6_0= ruleCase ) )* ) ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_declarations_1_0 = null;

        EObject lv_fixedDeclarations_2_0 = null;

        EObject lv_rules_4_0 = null;

        EObject lv_elements_6_0 = null;



        	enterRule();

        try {
            // InternalMethodName.g:78:2: ( (otherlv_0= 'declarations' ( (lv_declarations_1_0= ruleDeclaration ) )* ( (lv_fixedDeclarations_2_0= ruleFixedDeclaration ) )+ otherlv_3= 'rules' ( (lv_rules_4_0= ruleRule ) )* otherlv_5= 'cases' ( (lv_elements_6_0= ruleCase ) )* ) )
            // InternalMethodName.g:79:2: (otherlv_0= 'declarations' ( (lv_declarations_1_0= ruleDeclaration ) )* ( (lv_fixedDeclarations_2_0= ruleFixedDeclaration ) )+ otherlv_3= 'rules' ( (lv_rules_4_0= ruleRule ) )* otherlv_5= 'cases' ( (lv_elements_6_0= ruleCase ) )* )
            {
            // InternalMethodName.g:79:2: (otherlv_0= 'declarations' ( (lv_declarations_1_0= ruleDeclaration ) )* ( (lv_fixedDeclarations_2_0= ruleFixedDeclaration ) )+ otherlv_3= 'rules' ( (lv_rules_4_0= ruleRule ) )* otherlv_5= 'cases' ( (lv_elements_6_0= ruleCase ) )* )
            // InternalMethodName.g:80:3: otherlv_0= 'declarations' ( (lv_declarations_1_0= ruleDeclaration ) )* ( (lv_fixedDeclarations_2_0= ruleFixedDeclaration ) )+ otherlv_3= 'rules' ( (lv_rules_4_0= ruleRule ) )* otherlv_5= 'cases' ( (lv_elements_6_0= ruleCase ) )*
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getModelAccess().getDeclarationsKeyword_0());
            		
            // InternalMethodName.g:84:3: ( (lv_declarations_1_0= ruleDeclaration ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_ID) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalMethodName.g:85:4: (lv_declarations_1_0= ruleDeclaration )
            	    {
            	    // InternalMethodName.g:85:4: (lv_declarations_1_0= ruleDeclaration )
            	    // InternalMethodName.g:86:5: lv_declarations_1_0= ruleDeclaration
            	    {

            	    					newCompositeNode(grammarAccess.getModelAccess().getDeclarationsDeclarationParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_declarations_1_0=ruleDeclaration();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"declarations",
            	    						lv_declarations_1_0,
            	    						"org.xtext.example.methodname.MethodName.Declaration");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalMethodName.g:103:3: ( (lv_fixedDeclarations_2_0= ruleFixedDeclaration ) )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>=40 && LA2_0<=44)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalMethodName.g:104:4: (lv_fixedDeclarations_2_0= ruleFixedDeclaration )
            	    {
            	    // InternalMethodName.g:104:4: (lv_fixedDeclarations_2_0= ruleFixedDeclaration )
            	    // InternalMethodName.g:105:5: lv_fixedDeclarations_2_0= ruleFixedDeclaration
            	    {

            	    					newCompositeNode(grammarAccess.getModelAccess().getFixedDeclarationsFixedDeclarationParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_4);
            	    lv_fixedDeclarations_2_0=ruleFixedDeclaration();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"fixedDeclarations",
            	    						lv_fixedDeclarations_2_0,
            	    						"org.xtext.example.methodname.MethodName.FixedDeclaration");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);

            otherlv_3=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_3, grammarAccess.getModelAccess().getRulesKeyword_3());
            		
            // InternalMethodName.g:126:3: ( (lv_rules_4_0= ruleRule ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==15) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalMethodName.g:127:4: (lv_rules_4_0= ruleRule )
            	    {
            	    // InternalMethodName.g:127:4: (lv_rules_4_0= ruleRule )
            	    // InternalMethodName.g:128:5: lv_rules_4_0= ruleRule
            	    {

            	    					newCompositeNode(grammarAccess.getModelAccess().getRulesRuleParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_5);
            	    lv_rules_4_0=ruleRule();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"rules",
            	    						lv_rules_4_0,
            	    						"org.xtext.example.methodname.MethodName.Rule");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_5=(Token)match(input,13,FOLLOW_6); 

            			newLeafNode(otherlv_5, grammarAccess.getModelAccess().getCasesKeyword_5());
            		
            // InternalMethodName.g:149:3: ( (lv_elements_6_0= ruleCase ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==30) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalMethodName.g:150:4: (lv_elements_6_0= ruleCase )
            	    {
            	    // InternalMethodName.g:150:4: (lv_elements_6_0= ruleCase )
            	    // InternalMethodName.g:151:5: lv_elements_6_0= ruleCase
            	    {

            	    					newCompositeNode(grammarAccess.getModelAccess().getElementsCaseParserRuleCall_6_0());
            	    				
            	    pushFollow(FOLLOW_6);
            	    lv_elements_6_0=ruleCase();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"elements",
            	    						lv_elements_6_0,
            	    						"org.xtext.example.methodname.MethodName.Case");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleDeclaration"
    // InternalMethodName.g:172:1: entryRuleDeclaration returns [EObject current=null] : iv_ruleDeclaration= ruleDeclaration EOF ;
    public final EObject entryRuleDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeclaration = null;


        try {
            // InternalMethodName.g:172:52: (iv_ruleDeclaration= ruleDeclaration EOF )
            // InternalMethodName.g:173:2: iv_ruleDeclaration= ruleDeclaration EOF
            {
             newCompositeNode(grammarAccess.getDeclarationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDeclaration=ruleDeclaration();

            state._fsp--;

             current =iv_ruleDeclaration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeclaration"


    // $ANTLR start "ruleDeclaration"
    // InternalMethodName.g:179:1: ruleDeclaration returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '<->' ( (lv_path_2_0= RULE_STRING ) ) ) ;
    public final EObject ruleDeclaration() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token lv_path_2_0=null;


        	enterRule();

        try {
            // InternalMethodName.g:185:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '<->' ( (lv_path_2_0= RULE_STRING ) ) ) )
            // InternalMethodName.g:186:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '<->' ( (lv_path_2_0= RULE_STRING ) ) )
            {
            // InternalMethodName.g:186:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '<->' ( (lv_path_2_0= RULE_STRING ) ) )
            // InternalMethodName.g:187:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '<->' ( (lv_path_2_0= RULE_STRING ) )
            {
            // InternalMethodName.g:187:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalMethodName.g:188:4: (lv_name_0_0= RULE_ID )
            {
            // InternalMethodName.g:188:4: (lv_name_0_0= RULE_ID )
            // InternalMethodName.g:189:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_7); 

            					newLeafNode(lv_name_0_0, grammarAccess.getDeclarationAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDeclarationRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,14,FOLLOW_8); 

            			newLeafNode(otherlv_1, grammarAccess.getDeclarationAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_1());
            		
            // InternalMethodName.g:209:3: ( (lv_path_2_0= RULE_STRING ) )
            // InternalMethodName.g:210:4: (lv_path_2_0= RULE_STRING )
            {
            // InternalMethodName.g:210:4: (lv_path_2_0= RULE_STRING )
            // InternalMethodName.g:211:5: lv_path_2_0= RULE_STRING
            {
            lv_path_2_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_path_2_0, grammarAccess.getDeclarationAccess().getPathSTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDeclarationRule());
            					}
            					setWithLastConsumed(
            						current,
            						"path",
            						lv_path_2_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeclaration"


    // $ANTLR start "entryRuleFixedDeclaration"
    // InternalMethodName.g:231:1: entryRuleFixedDeclaration returns [EObject current=null] : iv_ruleFixedDeclaration= ruleFixedDeclaration EOF ;
    public final EObject entryRuleFixedDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFixedDeclaration = null;


        try {
            // InternalMethodName.g:231:57: (iv_ruleFixedDeclaration= ruleFixedDeclaration EOF )
            // InternalMethodName.g:232:2: iv_ruleFixedDeclaration= ruleFixedDeclaration EOF
            {
             newCompositeNode(grammarAccess.getFixedDeclarationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFixedDeclaration=ruleFixedDeclaration();

            state._fsp--;

             current =iv_ruleFixedDeclaration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFixedDeclaration"


    // $ANTLR start "ruleFixedDeclaration"
    // InternalMethodName.g:238:1: ruleFixedDeclaration returns [EObject current=null] : ( ( (lv_kind_0_0= ruleKind ) ) otherlv_1= '<->' ( (lv_path_2_0= RULE_STRING ) ) ) ;
    public final EObject ruleFixedDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_path_2_0=null;
        Enumerator lv_kind_0_0 = null;



        	enterRule();

        try {
            // InternalMethodName.g:244:2: ( ( ( (lv_kind_0_0= ruleKind ) ) otherlv_1= '<->' ( (lv_path_2_0= RULE_STRING ) ) ) )
            // InternalMethodName.g:245:2: ( ( (lv_kind_0_0= ruleKind ) ) otherlv_1= '<->' ( (lv_path_2_0= RULE_STRING ) ) )
            {
            // InternalMethodName.g:245:2: ( ( (lv_kind_0_0= ruleKind ) ) otherlv_1= '<->' ( (lv_path_2_0= RULE_STRING ) ) )
            // InternalMethodName.g:246:3: ( (lv_kind_0_0= ruleKind ) ) otherlv_1= '<->' ( (lv_path_2_0= RULE_STRING ) )
            {
            // InternalMethodName.g:246:3: ( (lv_kind_0_0= ruleKind ) )
            // InternalMethodName.g:247:4: (lv_kind_0_0= ruleKind )
            {
            // InternalMethodName.g:247:4: (lv_kind_0_0= ruleKind )
            // InternalMethodName.g:248:5: lv_kind_0_0= ruleKind
            {

            					newCompositeNode(grammarAccess.getFixedDeclarationAccess().getKindKindEnumRuleCall_0_0());
            				
            pushFollow(FOLLOW_7);
            lv_kind_0_0=ruleKind();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFixedDeclarationRule());
            					}
            					set(
            						current,
            						"kind",
            						lv_kind_0_0,
            						"org.xtext.example.methodname.MethodName.Kind");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,14,FOLLOW_8); 

            			newLeafNode(otherlv_1, grammarAccess.getFixedDeclarationAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_1());
            		
            // InternalMethodName.g:269:3: ( (lv_path_2_0= RULE_STRING ) )
            // InternalMethodName.g:270:4: (lv_path_2_0= RULE_STRING )
            {
            // InternalMethodName.g:270:4: (lv_path_2_0= RULE_STRING )
            // InternalMethodName.g:271:5: lv_path_2_0= RULE_STRING
            {
            lv_path_2_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_path_2_0, grammarAccess.getFixedDeclarationAccess().getPathSTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFixedDeclarationRule());
            					}
            					setWithLastConsumed(
            						current,
            						"path",
            						lv_path_2_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFixedDeclaration"


    // $ANTLR start "entryRuleRule"
    // InternalMethodName.g:291:1: entryRuleRule returns [EObject current=null] : iv_ruleRule= ruleRule EOF ;
    public final EObject entryRuleRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRule = null;


        try {
            // InternalMethodName.g:291:45: (iv_ruleRule= ruleRule EOF )
            // InternalMethodName.g:292:2: iv_ruleRule= ruleRule EOF
            {
             newCompositeNode(grammarAccess.getRuleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRule=ruleRule();

            state._fsp--;

             current =iv_ruleRule; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRule"


    // $ANTLR start "ruleRule"
    // InternalMethodName.g:298:1: ruleRule returns [EObject current=null] : ( (otherlv_0= 'def' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'for' ( (lv_kind_3_0= ruleKind ) ) otherlv_4= '{' otherlv_5= 'filter' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '||' ( (otherlv_8= RULE_ID ) ) )* otherlv_9= 'such' otherlv_10= 'that' otherlv_11= 'size' ( ( (lv_compOp_12_1= '>' | lv_compOp_12_2= '<' | lv_compOp_12_3= '==' | lv_compOp_12_4= '<=' | lv_compOp_12_5= '>=' ) ) ) ( (lv_compValue_13_0= RULE_INT ) ) otherlv_14= '}' ) | (otherlv_15= 'def' otherlv_16= 'declare' ( (lv_name_17_0= RULE_ID ) ) ) ) ;
    public final EObject ruleRule() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token lv_compOp_12_1=null;
        Token lv_compOp_12_2=null;
        Token lv_compOp_12_3=null;
        Token lv_compOp_12_4=null;
        Token lv_compOp_12_5=null;
        Token lv_compValue_13_0=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token lv_name_17_0=null;
        Enumerator lv_kind_3_0 = null;



        	enterRule();

        try {
            // InternalMethodName.g:304:2: ( ( (otherlv_0= 'def' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'for' ( (lv_kind_3_0= ruleKind ) ) otherlv_4= '{' otherlv_5= 'filter' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '||' ( (otherlv_8= RULE_ID ) ) )* otherlv_9= 'such' otherlv_10= 'that' otherlv_11= 'size' ( ( (lv_compOp_12_1= '>' | lv_compOp_12_2= '<' | lv_compOp_12_3= '==' | lv_compOp_12_4= '<=' | lv_compOp_12_5= '>=' ) ) ) ( (lv_compValue_13_0= RULE_INT ) ) otherlv_14= '}' ) | (otherlv_15= 'def' otherlv_16= 'declare' ( (lv_name_17_0= RULE_ID ) ) ) ) )
            // InternalMethodName.g:305:2: ( (otherlv_0= 'def' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'for' ( (lv_kind_3_0= ruleKind ) ) otherlv_4= '{' otherlv_5= 'filter' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '||' ( (otherlv_8= RULE_ID ) ) )* otherlv_9= 'such' otherlv_10= 'that' otherlv_11= 'size' ( ( (lv_compOp_12_1= '>' | lv_compOp_12_2= '<' | lv_compOp_12_3= '==' | lv_compOp_12_4= '<=' | lv_compOp_12_5= '>=' ) ) ) ( (lv_compValue_13_0= RULE_INT ) ) otherlv_14= '}' ) | (otherlv_15= 'def' otherlv_16= 'declare' ( (lv_name_17_0= RULE_ID ) ) ) )
            {
            // InternalMethodName.g:305:2: ( (otherlv_0= 'def' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'for' ( (lv_kind_3_0= ruleKind ) ) otherlv_4= '{' otherlv_5= 'filter' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '||' ( (otherlv_8= RULE_ID ) ) )* otherlv_9= 'such' otherlv_10= 'that' otherlv_11= 'size' ( ( (lv_compOp_12_1= '>' | lv_compOp_12_2= '<' | lv_compOp_12_3= '==' | lv_compOp_12_4= '<=' | lv_compOp_12_5= '>=' ) ) ) ( (lv_compValue_13_0= RULE_INT ) ) otherlv_14= '}' ) | (otherlv_15= 'def' otherlv_16= 'declare' ( (lv_name_17_0= RULE_ID ) ) ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==15) ) {
                int LA7_1 = input.LA(2);

                if ( (LA7_1==29) ) {
                    alt7=2;
                }
                else if ( (LA7_1==RULE_ID) ) {
                    alt7=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 7, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalMethodName.g:306:3: (otherlv_0= 'def' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'for' ( (lv_kind_3_0= ruleKind ) ) otherlv_4= '{' otherlv_5= 'filter' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '||' ( (otherlv_8= RULE_ID ) ) )* otherlv_9= 'such' otherlv_10= 'that' otherlv_11= 'size' ( ( (lv_compOp_12_1= '>' | lv_compOp_12_2= '<' | lv_compOp_12_3= '==' | lv_compOp_12_4= '<=' | lv_compOp_12_5= '>=' ) ) ) ( (lv_compValue_13_0= RULE_INT ) ) otherlv_14= '}' )
                    {
                    // InternalMethodName.g:306:3: (otherlv_0= 'def' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'for' ( (lv_kind_3_0= ruleKind ) ) otherlv_4= '{' otherlv_5= 'filter' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '||' ( (otherlv_8= RULE_ID ) ) )* otherlv_9= 'such' otherlv_10= 'that' otherlv_11= 'size' ( ( (lv_compOp_12_1= '>' | lv_compOp_12_2= '<' | lv_compOp_12_3= '==' | lv_compOp_12_4= '<=' | lv_compOp_12_5= '>=' ) ) ) ( (lv_compValue_13_0= RULE_INT ) ) otherlv_14= '}' )
                    // InternalMethodName.g:307:4: otherlv_0= 'def' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'for' ( (lv_kind_3_0= ruleKind ) ) otherlv_4= '{' otherlv_5= 'filter' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '||' ( (otherlv_8= RULE_ID ) ) )* otherlv_9= 'such' otherlv_10= 'that' otherlv_11= 'size' ( ( (lv_compOp_12_1= '>' | lv_compOp_12_2= '<' | lv_compOp_12_3= '==' | lv_compOp_12_4= '<=' | lv_compOp_12_5= '>=' ) ) ) ( (lv_compValue_13_0= RULE_INT ) ) otherlv_14= '}'
                    {
                    otherlv_0=(Token)match(input,15,FOLLOW_9); 

                    				newLeafNode(otherlv_0, grammarAccess.getRuleAccess().getDefKeyword_0_0());
                    			
                    // InternalMethodName.g:311:4: ( (lv_name_1_0= RULE_ID ) )
                    // InternalMethodName.g:312:5: (lv_name_1_0= RULE_ID )
                    {
                    // InternalMethodName.g:312:5: (lv_name_1_0= RULE_ID )
                    // InternalMethodName.g:313:6: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_10); 

                    						newLeafNode(lv_name_1_0, grammarAccess.getRuleAccess().getNameIDTerminalRuleCall_0_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRuleRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"name",
                    							lv_name_1_0,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }


                    }

                    otherlv_2=(Token)match(input,16,FOLLOW_3); 

                    				newLeafNode(otherlv_2, grammarAccess.getRuleAccess().getForKeyword_0_2());
                    			
                    // InternalMethodName.g:333:4: ( (lv_kind_3_0= ruleKind ) )
                    // InternalMethodName.g:334:5: (lv_kind_3_0= ruleKind )
                    {
                    // InternalMethodName.g:334:5: (lv_kind_3_0= ruleKind )
                    // InternalMethodName.g:335:6: lv_kind_3_0= ruleKind
                    {

                    						newCompositeNode(grammarAccess.getRuleAccess().getKindKindEnumRuleCall_0_3_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_kind_3_0=ruleKind();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRuleRule());
                    						}
                    						set(
                    							current,
                    							"kind",
                    							lv_kind_3_0,
                    							"org.xtext.example.methodname.MethodName.Kind");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_4=(Token)match(input,17,FOLLOW_12); 

                    				newLeafNode(otherlv_4, grammarAccess.getRuleAccess().getLeftCurlyBracketKeyword_0_4());
                    			
                    otherlv_5=(Token)match(input,18,FOLLOW_9); 

                    				newLeafNode(otherlv_5, grammarAccess.getRuleAccess().getFilterKeyword_0_5());
                    			
                    // InternalMethodName.g:360:4: ( (otherlv_6= RULE_ID ) )
                    // InternalMethodName.g:361:5: (otherlv_6= RULE_ID )
                    {
                    // InternalMethodName.g:361:5: (otherlv_6= RULE_ID )
                    // InternalMethodName.g:362:6: otherlv_6= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRuleRule());
                    						}
                    					
                    otherlv_6=(Token)match(input,RULE_ID,FOLLOW_13); 

                    						newLeafNode(otherlv_6, grammarAccess.getRuleAccess().getDeclsDeclarationCrossReference_0_6_0());
                    					

                    }


                    }

                    // InternalMethodName.g:373:4: (otherlv_7= '||' ( (otherlv_8= RULE_ID ) ) )*
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0==19) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // InternalMethodName.g:374:5: otherlv_7= '||' ( (otherlv_8= RULE_ID ) )
                    	    {
                    	    otherlv_7=(Token)match(input,19,FOLLOW_9); 

                    	    					newLeafNode(otherlv_7, grammarAccess.getRuleAccess().getVerticalLineVerticalLineKeyword_0_7_0());
                    	    				
                    	    // InternalMethodName.g:378:5: ( (otherlv_8= RULE_ID ) )
                    	    // InternalMethodName.g:379:6: (otherlv_8= RULE_ID )
                    	    {
                    	    // InternalMethodName.g:379:6: (otherlv_8= RULE_ID )
                    	    // InternalMethodName.g:380:7: otherlv_8= RULE_ID
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getRuleRule());
                    	    							}
                    	    						
                    	    otherlv_8=(Token)match(input,RULE_ID,FOLLOW_13); 

                    	    							newLeafNode(otherlv_8, grammarAccess.getRuleAccess().getDeclsDeclarationCrossReference_0_7_1_0());
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);

                    otherlv_9=(Token)match(input,20,FOLLOW_14); 

                    				newLeafNode(otherlv_9, grammarAccess.getRuleAccess().getSuchKeyword_0_8());
                    			
                    otherlv_10=(Token)match(input,21,FOLLOW_15); 

                    				newLeafNode(otherlv_10, grammarAccess.getRuleAccess().getThatKeyword_0_9());
                    			
                    otherlv_11=(Token)match(input,22,FOLLOW_16); 

                    				newLeafNode(otherlv_11, grammarAccess.getRuleAccess().getSizeKeyword_0_10());
                    			
                    // InternalMethodName.g:404:4: ( ( (lv_compOp_12_1= '>' | lv_compOp_12_2= '<' | lv_compOp_12_3= '==' | lv_compOp_12_4= '<=' | lv_compOp_12_5= '>=' ) ) )
                    // InternalMethodName.g:405:5: ( (lv_compOp_12_1= '>' | lv_compOp_12_2= '<' | lv_compOp_12_3= '==' | lv_compOp_12_4= '<=' | lv_compOp_12_5= '>=' ) )
                    {
                    // InternalMethodName.g:405:5: ( (lv_compOp_12_1= '>' | lv_compOp_12_2= '<' | lv_compOp_12_3= '==' | lv_compOp_12_4= '<=' | lv_compOp_12_5= '>=' ) )
                    // InternalMethodName.g:406:6: (lv_compOp_12_1= '>' | lv_compOp_12_2= '<' | lv_compOp_12_3= '==' | lv_compOp_12_4= '<=' | lv_compOp_12_5= '>=' )
                    {
                    // InternalMethodName.g:406:6: (lv_compOp_12_1= '>' | lv_compOp_12_2= '<' | lv_compOp_12_3= '==' | lv_compOp_12_4= '<=' | lv_compOp_12_5= '>=' )
                    int alt6=5;
                    switch ( input.LA(1) ) {
                    case 23:
                        {
                        alt6=1;
                        }
                        break;
                    case 24:
                        {
                        alt6=2;
                        }
                        break;
                    case 25:
                        {
                        alt6=3;
                        }
                        break;
                    case 26:
                        {
                        alt6=4;
                        }
                        break;
                    case 27:
                        {
                        alt6=5;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 6, 0, input);

                        throw nvae;
                    }

                    switch (alt6) {
                        case 1 :
                            // InternalMethodName.g:407:7: lv_compOp_12_1= '>'
                            {
                            lv_compOp_12_1=(Token)match(input,23,FOLLOW_17); 

                            							newLeafNode(lv_compOp_12_1, grammarAccess.getRuleAccess().getCompOpGreaterThanSignKeyword_0_11_0_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getRuleRule());
                            							}
                            							setWithLastConsumed(current, "compOp", lv_compOp_12_1, null);
                            						

                            }
                            break;
                        case 2 :
                            // InternalMethodName.g:418:7: lv_compOp_12_2= '<'
                            {
                            lv_compOp_12_2=(Token)match(input,24,FOLLOW_17); 

                            							newLeafNode(lv_compOp_12_2, grammarAccess.getRuleAccess().getCompOpLessThanSignKeyword_0_11_0_1());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getRuleRule());
                            							}
                            							setWithLastConsumed(current, "compOp", lv_compOp_12_2, null);
                            						

                            }
                            break;
                        case 3 :
                            // InternalMethodName.g:429:7: lv_compOp_12_3= '=='
                            {
                            lv_compOp_12_3=(Token)match(input,25,FOLLOW_17); 

                            							newLeafNode(lv_compOp_12_3, grammarAccess.getRuleAccess().getCompOpEqualsSignEqualsSignKeyword_0_11_0_2());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getRuleRule());
                            							}
                            							setWithLastConsumed(current, "compOp", lv_compOp_12_3, null);
                            						

                            }
                            break;
                        case 4 :
                            // InternalMethodName.g:440:7: lv_compOp_12_4= '<='
                            {
                            lv_compOp_12_4=(Token)match(input,26,FOLLOW_17); 

                            							newLeafNode(lv_compOp_12_4, grammarAccess.getRuleAccess().getCompOpLessThanSignEqualsSignKeyword_0_11_0_3());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getRuleRule());
                            							}
                            							setWithLastConsumed(current, "compOp", lv_compOp_12_4, null);
                            						

                            }
                            break;
                        case 5 :
                            // InternalMethodName.g:451:7: lv_compOp_12_5= '>='
                            {
                            lv_compOp_12_5=(Token)match(input,27,FOLLOW_17); 

                            							newLeafNode(lv_compOp_12_5, grammarAccess.getRuleAccess().getCompOpGreaterThanSignEqualsSignKeyword_0_11_0_4());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getRuleRule());
                            							}
                            							setWithLastConsumed(current, "compOp", lv_compOp_12_5, null);
                            						

                            }
                            break;

                    }


                    }


                    }

                    // InternalMethodName.g:464:4: ( (lv_compValue_13_0= RULE_INT ) )
                    // InternalMethodName.g:465:5: (lv_compValue_13_0= RULE_INT )
                    {
                    // InternalMethodName.g:465:5: (lv_compValue_13_0= RULE_INT )
                    // InternalMethodName.g:466:6: lv_compValue_13_0= RULE_INT
                    {
                    lv_compValue_13_0=(Token)match(input,RULE_INT,FOLLOW_18); 

                    						newLeafNode(lv_compValue_13_0, grammarAccess.getRuleAccess().getCompValueINTTerminalRuleCall_0_12_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRuleRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"compValue",
                    							lv_compValue_13_0,
                    							"org.eclipse.xtext.common.Terminals.INT");
                    					

                    }


                    }

                    otherlv_14=(Token)match(input,28,FOLLOW_2); 

                    				newLeafNode(otherlv_14, grammarAccess.getRuleAccess().getRightCurlyBracketKeyword_0_13());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMethodName.g:488:3: (otherlv_15= 'def' otherlv_16= 'declare' ( (lv_name_17_0= RULE_ID ) ) )
                    {
                    // InternalMethodName.g:488:3: (otherlv_15= 'def' otherlv_16= 'declare' ( (lv_name_17_0= RULE_ID ) ) )
                    // InternalMethodName.g:489:4: otherlv_15= 'def' otherlv_16= 'declare' ( (lv_name_17_0= RULE_ID ) )
                    {
                    otherlv_15=(Token)match(input,15,FOLLOW_19); 

                    				newLeafNode(otherlv_15, grammarAccess.getRuleAccess().getDefKeyword_1_0());
                    			
                    otherlv_16=(Token)match(input,29,FOLLOW_9); 

                    				newLeafNode(otherlv_16, grammarAccess.getRuleAccess().getDeclareKeyword_1_1());
                    			
                    // InternalMethodName.g:497:4: ( (lv_name_17_0= RULE_ID ) )
                    // InternalMethodName.g:498:5: (lv_name_17_0= RULE_ID )
                    {
                    // InternalMethodName.g:498:5: (lv_name_17_0= RULE_ID )
                    // InternalMethodName.g:499:6: lv_name_17_0= RULE_ID
                    {
                    lv_name_17_0=(Token)match(input,RULE_ID,FOLLOW_2); 

                    						newLeafNode(lv_name_17_0, grammarAccess.getRuleAccess().getNameIDTerminalRuleCall_1_2_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRuleRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"name",
                    							lv_name_17_0,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }


                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRule"


    // $ANTLR start "entryRuleCase"
    // InternalMethodName.g:520:1: entryRuleCase returns [EObject current=null] : iv_ruleCase= ruleCase EOF ;
    public final EObject entryRuleCase() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCase = null;


        try {
            // InternalMethodName.g:520:45: (iv_ruleCase= ruleCase EOF )
            // InternalMethodName.g:521:2: iv_ruleCase= ruleCase EOF
            {
             newCompositeNode(grammarAccess.getCaseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCase=ruleCase();

            state._fsp--;

             current =iv_ruleCase; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCase"


    // $ANTLR start "ruleCase"
    // InternalMethodName.g:527:1: ruleCase returns [EObject current=null] : (otherlv_0= 'case' otherlv_1= 'for' ( (lv_kind_2_0= ruleKind ) ) ( (lv_condition_3_0= ruleExpr ) ) otherlv_4= '{' ( (lv_properties_5_0= ruleProperty ) )* otherlv_6= '}' ) ;
    public final EObject ruleCase() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Enumerator lv_kind_2_0 = null;

        EObject lv_condition_3_0 = null;

        EObject lv_properties_5_0 = null;



        	enterRule();

        try {
            // InternalMethodName.g:533:2: ( (otherlv_0= 'case' otherlv_1= 'for' ( (lv_kind_2_0= ruleKind ) ) ( (lv_condition_3_0= ruleExpr ) ) otherlv_4= '{' ( (lv_properties_5_0= ruleProperty ) )* otherlv_6= '}' ) )
            // InternalMethodName.g:534:2: (otherlv_0= 'case' otherlv_1= 'for' ( (lv_kind_2_0= ruleKind ) ) ( (lv_condition_3_0= ruleExpr ) ) otherlv_4= '{' ( (lv_properties_5_0= ruleProperty ) )* otherlv_6= '}' )
            {
            // InternalMethodName.g:534:2: (otherlv_0= 'case' otherlv_1= 'for' ( (lv_kind_2_0= ruleKind ) ) ( (lv_condition_3_0= ruleExpr ) ) otherlv_4= '{' ( (lv_properties_5_0= ruleProperty ) )* otherlv_6= '}' )
            // InternalMethodName.g:535:3: otherlv_0= 'case' otherlv_1= 'for' ( (lv_kind_2_0= ruleKind ) ) ( (lv_condition_3_0= ruleExpr ) ) otherlv_4= '{' ( (lv_properties_5_0= ruleProperty ) )* otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,30,FOLLOW_10); 

            			newLeafNode(otherlv_0, grammarAccess.getCaseAccess().getCaseKeyword_0());
            		
            otherlv_1=(Token)match(input,16,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getCaseAccess().getForKeyword_1());
            		
            // InternalMethodName.g:543:3: ( (lv_kind_2_0= ruleKind ) )
            // InternalMethodName.g:544:4: (lv_kind_2_0= ruleKind )
            {
            // InternalMethodName.g:544:4: (lv_kind_2_0= ruleKind )
            // InternalMethodName.g:545:5: lv_kind_2_0= ruleKind
            {

            					newCompositeNode(grammarAccess.getCaseAccess().getKindKindEnumRuleCall_2_0());
            				
            pushFollow(FOLLOW_20);
            lv_kind_2_0=ruleKind();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCaseRule());
            					}
            					set(
            						current,
            						"kind",
            						lv_kind_2_0,
            						"org.xtext.example.methodname.MethodName.Kind");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalMethodName.g:562:3: ( (lv_condition_3_0= ruleExpr ) )
            // InternalMethodName.g:563:4: (lv_condition_3_0= ruleExpr )
            {
            // InternalMethodName.g:563:4: (lv_condition_3_0= ruleExpr )
            // InternalMethodName.g:564:5: lv_condition_3_0= ruleExpr
            {

            					newCompositeNode(grammarAccess.getCaseAccess().getConditionExprParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_11);
            lv_condition_3_0=ruleExpr();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCaseRule());
            					}
            					set(
            						current,
            						"condition",
            						lv_condition_3_0,
            						"org.xtext.example.methodname.MethodName.Expr");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,17,FOLLOW_21); 

            			newLeafNode(otherlv_4, grammarAccess.getCaseAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalMethodName.g:585:3: ( (lv_properties_5_0= ruleProperty ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==RULE_ID||(LA8_0>=87 && LA8_0<=92)) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalMethodName.g:586:4: (lv_properties_5_0= ruleProperty )
            	    {
            	    // InternalMethodName.g:586:4: (lv_properties_5_0= ruleProperty )
            	    // InternalMethodName.g:587:5: lv_properties_5_0= ruleProperty
            	    {

            	    					newCompositeNode(grammarAccess.getCaseAccess().getPropertiesPropertyParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_21);
            	    lv_properties_5_0=ruleProperty();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getCaseRule());
            	    					}
            	    					add(
            	    						current,
            	    						"properties",
            	    						lv_properties_5_0,
            	    						"org.xtext.example.methodname.MethodName.Property");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            otherlv_6=(Token)match(input,28,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getCaseAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCase"


    // $ANTLR start "entryRuleExpr"
    // InternalMethodName.g:612:1: entryRuleExpr returns [EObject current=null] : iv_ruleExpr= ruleExpr EOF ;
    public final EObject entryRuleExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpr = null;


        try {
            // InternalMethodName.g:612:45: (iv_ruleExpr= ruleExpr EOF )
            // InternalMethodName.g:613:2: iv_ruleExpr= ruleExpr EOF
            {
             newCompositeNode(grammarAccess.getExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExpr=ruleExpr();

            state._fsp--;

             current =iv_ruleExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpr"


    // $ANTLR start "ruleExpr"
    // InternalMethodName.g:619:1: ruleExpr returns [EObject current=null] : this_Or_0= ruleOr ;
    public final EObject ruleExpr() throws RecognitionException {
        EObject current = null;

        EObject this_Or_0 = null;



        	enterRule();

        try {
            // InternalMethodName.g:625:2: (this_Or_0= ruleOr )
            // InternalMethodName.g:626:2: this_Or_0= ruleOr
            {

            		newCompositeNode(grammarAccess.getExprAccess().getOrParserRuleCall());
            	
            pushFollow(FOLLOW_2);
            this_Or_0=ruleOr();

            state._fsp--;


            		current = this_Or_0;
            		afterParserOrEnumRuleCall();
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpr"


    // $ANTLR start "entryRuleOr"
    // InternalMethodName.g:637:1: entryRuleOr returns [EObject current=null] : iv_ruleOr= ruleOr EOF ;
    public final EObject entryRuleOr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOr = null;


        try {
            // InternalMethodName.g:637:43: (iv_ruleOr= ruleOr EOF )
            // InternalMethodName.g:638:2: iv_ruleOr= ruleOr EOF
            {
             newCompositeNode(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOr=ruleOr();

            state._fsp--;

             current =iv_ruleOr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalMethodName.g:644:1: ruleOr returns [EObject current=null] : (this_And_0= ruleAnd ( () otherlv_2= '|' ( (lv_right_3_0= ruleAnd ) ) )* ) ;
    public final EObject ruleOr() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_And_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMethodName.g:650:2: ( (this_And_0= ruleAnd ( () otherlv_2= '|' ( (lv_right_3_0= ruleAnd ) ) )* ) )
            // InternalMethodName.g:651:2: (this_And_0= ruleAnd ( () otherlv_2= '|' ( (lv_right_3_0= ruleAnd ) ) )* )
            {
            // InternalMethodName.g:651:2: (this_And_0= ruleAnd ( () otherlv_2= '|' ( (lv_right_3_0= ruleAnd ) ) )* )
            // InternalMethodName.g:652:3: this_And_0= ruleAnd ( () otherlv_2= '|' ( (lv_right_3_0= ruleAnd ) ) )*
            {

            			newCompositeNode(grammarAccess.getOrAccess().getAndParserRuleCall_0());
            		
            pushFollow(FOLLOW_22);
            this_And_0=ruleAnd();

            state._fsp--;


            			current = this_And_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMethodName.g:660:3: ( () otherlv_2= '|' ( (lv_right_3_0= ruleAnd ) ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==31) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalMethodName.g:661:4: () otherlv_2= '|' ( (lv_right_3_0= ruleAnd ) )
            	    {
            	    // InternalMethodName.g:661:4: ()
            	    // InternalMethodName.g:662:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getOrAccess().getOrLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,31,FOLLOW_20); 

            	    				newLeafNode(otherlv_2, grammarAccess.getOrAccess().getVerticalLineKeyword_1_1());
            	    			
            	    // InternalMethodName.g:672:4: ( (lv_right_3_0= ruleAnd ) )
            	    // InternalMethodName.g:673:5: (lv_right_3_0= ruleAnd )
            	    {
            	    // InternalMethodName.g:673:5: (lv_right_3_0= ruleAnd )
            	    // InternalMethodName.g:674:6: lv_right_3_0= ruleAnd
            	    {

            	    						newCompositeNode(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_22);
            	    lv_right_3_0=ruleAnd();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getOrRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.methodname.MethodName.And");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleAnd"
    // InternalMethodName.g:696:1: entryRuleAnd returns [EObject current=null] : iv_ruleAnd= ruleAnd EOF ;
    public final EObject entryRuleAnd() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnd = null;


        try {
            // InternalMethodName.g:696:44: (iv_ruleAnd= ruleAnd EOF )
            // InternalMethodName.g:697:2: iv_ruleAnd= ruleAnd EOF
            {
             newCompositeNode(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnd=ruleAnd();

            state._fsp--;

             current =iv_ruleAnd; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalMethodName.g:703:1: ruleAnd returns [EObject current=null] : (this_Primary_0= rulePrimary ( () ( ( (lv_concatKind_2_1= '.' | lv_concatKind_2_2= '...' ) ) ) ( (lv_right_3_0= rulePrimary ) ) )* ) ;
    public final EObject ruleAnd() throws RecognitionException {
        EObject current = null;

        Token lv_concatKind_2_1=null;
        Token lv_concatKind_2_2=null;
        EObject this_Primary_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalMethodName.g:709:2: ( (this_Primary_0= rulePrimary ( () ( ( (lv_concatKind_2_1= '.' | lv_concatKind_2_2= '...' ) ) ) ( (lv_right_3_0= rulePrimary ) ) )* ) )
            // InternalMethodName.g:710:2: (this_Primary_0= rulePrimary ( () ( ( (lv_concatKind_2_1= '.' | lv_concatKind_2_2= '...' ) ) ) ( (lv_right_3_0= rulePrimary ) ) )* )
            {
            // InternalMethodName.g:710:2: (this_Primary_0= rulePrimary ( () ( ( (lv_concatKind_2_1= '.' | lv_concatKind_2_2= '...' ) ) ) ( (lv_right_3_0= rulePrimary ) ) )* )
            // InternalMethodName.g:711:3: this_Primary_0= rulePrimary ( () ( ( (lv_concatKind_2_1= '.' | lv_concatKind_2_2= '...' ) ) ) ( (lv_right_3_0= rulePrimary ) ) )*
            {

            			newCompositeNode(grammarAccess.getAndAccess().getPrimaryParserRuleCall_0());
            		
            pushFollow(FOLLOW_23);
            this_Primary_0=rulePrimary();

            state._fsp--;


            			current = this_Primary_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalMethodName.g:719:3: ( () ( ( (lv_concatKind_2_1= '.' | lv_concatKind_2_2= '...' ) ) ) ( (lv_right_3_0= rulePrimary ) ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0>=32 && LA11_0<=33)) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalMethodName.g:720:4: () ( ( (lv_concatKind_2_1= '.' | lv_concatKind_2_2= '...' ) ) ) ( (lv_right_3_0= rulePrimary ) )
            	    {
            	    // InternalMethodName.g:720:4: ()
            	    // InternalMethodName.g:721:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getAndAccess().getAndLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalMethodName.g:727:4: ( ( (lv_concatKind_2_1= '.' | lv_concatKind_2_2= '...' ) ) )
            	    // InternalMethodName.g:728:5: ( (lv_concatKind_2_1= '.' | lv_concatKind_2_2= '...' ) )
            	    {
            	    // InternalMethodName.g:728:5: ( (lv_concatKind_2_1= '.' | lv_concatKind_2_2= '...' ) )
            	    // InternalMethodName.g:729:6: (lv_concatKind_2_1= '.' | lv_concatKind_2_2= '...' )
            	    {
            	    // InternalMethodName.g:729:6: (lv_concatKind_2_1= '.' | lv_concatKind_2_2= '...' )
            	    int alt10=2;
            	    int LA10_0 = input.LA(1);

            	    if ( (LA10_0==32) ) {
            	        alt10=1;
            	    }
            	    else if ( (LA10_0==33) ) {
            	        alt10=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 10, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt10) {
            	        case 1 :
            	            // InternalMethodName.g:730:7: lv_concatKind_2_1= '.'
            	            {
            	            lv_concatKind_2_1=(Token)match(input,32,FOLLOW_20); 

            	            							newLeafNode(lv_concatKind_2_1, grammarAccess.getAndAccess().getConcatKindFullStopKeyword_1_1_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getAndRule());
            	            							}
            	            							setWithLastConsumed(current, "concatKind", lv_concatKind_2_1, null);
            	            						

            	            }
            	            break;
            	        case 2 :
            	            // InternalMethodName.g:741:7: lv_concatKind_2_2= '...'
            	            {
            	            lv_concatKind_2_2=(Token)match(input,33,FOLLOW_20); 

            	            							newLeafNode(lv_concatKind_2_2, grammarAccess.getAndAccess().getConcatKindFullStopFullStopFullStopKeyword_1_1_0_1());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getAndRule());
            	            							}
            	            							setWithLastConsumed(current, "concatKind", lv_concatKind_2_2, null);
            	            						

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalMethodName.g:754:4: ( (lv_right_3_0= rulePrimary ) )
            	    // InternalMethodName.g:755:5: (lv_right_3_0= rulePrimary )
            	    {
            	    // InternalMethodName.g:755:5: (lv_right_3_0= rulePrimary )
            	    // InternalMethodName.g:756:6: lv_right_3_0= rulePrimary
            	    {

            	    						newCompositeNode(grammarAccess.getAndAccess().getRightPrimaryParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_23);
            	    lv_right_3_0=rulePrimary();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAndRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.xtext.example.methodname.MethodName.Primary");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRulePrimary"
    // InternalMethodName.g:778:1: entryRulePrimary returns [EObject current=null] : iv_rulePrimary= rulePrimary EOF ;
    public final EObject entryRulePrimary() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimary = null;


        try {
            // InternalMethodName.g:778:48: (iv_rulePrimary= rulePrimary EOF )
            // InternalMethodName.g:779:2: iv_rulePrimary= rulePrimary EOF
            {
             newCompositeNode(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimary=rulePrimary();

            state._fsp--;

             current =iv_rulePrimary; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalMethodName.g:785:1: rulePrimary returns [EObject current=null] : (this_GroupExpr_0= ruleGroupExpr | this_Atomic_1= ruleAtomic ) ;
    public final EObject rulePrimary() throws RecognitionException {
        EObject current = null;

        EObject this_GroupExpr_0 = null;

        EObject this_Atomic_1 = null;



        	enterRule();

        try {
            // InternalMethodName.g:791:2: ( (this_GroupExpr_0= ruleGroupExpr | this_Atomic_1= ruleAtomic ) )
            // InternalMethodName.g:792:2: (this_GroupExpr_0= ruleGroupExpr | this_Atomic_1= ruleAtomic )
            {
            // InternalMethodName.g:792:2: (this_GroupExpr_0= ruleGroupExpr | this_Atomic_1= ruleAtomic )
            int alt12=2;
            switch ( input.LA(1) ) {
            case 34:
                {
                int LA12_1 = input.LA(2);

                if ( (LA12_1==RULE_STRING||LA12_1==39||(LA12_1>=49 && LA12_1<=82)) ) {
                    alt12=2;
                }
                else if ( (LA12_1==35) ) {
                    alt12=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 12, 1, input);

                    throw nvae;
                }
                }
                break;
            case 35:
                {
                alt12=1;
                }
                break;
            case RULE_STRING:
            case 39:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
            case 67:
            case 68:
            case 69:
            case 70:
            case 71:
            case 72:
            case 73:
            case 74:
            case 75:
            case 76:
            case 77:
            case 78:
            case 79:
            case 80:
            case 81:
            case 82:
                {
                alt12=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }

            switch (alt12) {
                case 1 :
                    // InternalMethodName.g:793:3: this_GroupExpr_0= ruleGroupExpr
                    {

                    			newCompositeNode(grammarAccess.getPrimaryAccess().getGroupExprParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_GroupExpr_0=ruleGroupExpr();

                    state._fsp--;


                    			current = this_GroupExpr_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalMethodName.g:802:3: this_Atomic_1= ruleAtomic
                    {

                    			newCompositeNode(grammarAccess.getPrimaryAccess().getAtomicParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Atomic_1=ruleAtomic();

                    state._fsp--;


                    			current = this_Atomic_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleGroupExpr"
    // InternalMethodName.g:814:1: entryRuleGroupExpr returns [EObject current=null] : iv_ruleGroupExpr= ruleGroupExpr EOF ;
    public final EObject entryRuleGroupExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGroupExpr = null;


        try {
            // InternalMethodName.g:814:50: (iv_ruleGroupExpr= ruleGroupExpr EOF )
            // InternalMethodName.g:815:2: iv_ruleGroupExpr= ruleGroupExpr EOF
            {
             newCompositeNode(grammarAccess.getGroupExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGroupExpr=ruleGroupExpr();

            state._fsp--;

             current =iv_ruleGroupExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGroupExpr"


    // $ANTLR start "ruleGroupExpr"
    // InternalMethodName.g:821:1: ruleGroupExpr returns [EObject current=null] : ( ( (lv_hasNot_0_0= '!' ) )? otherlv_1= '(' ( (lv_expr_2_0= ruleExpr ) ) otherlv_3= ')' ( (lv_card_4_0= ruleCardinalityModifier ) )? ) ;
    public final EObject ruleGroupExpr() throws RecognitionException {
        EObject current = null;

        Token lv_hasNot_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_expr_2_0 = null;

        Enumerator lv_card_4_0 = null;



        	enterRule();

        try {
            // InternalMethodName.g:827:2: ( ( ( (lv_hasNot_0_0= '!' ) )? otherlv_1= '(' ( (lv_expr_2_0= ruleExpr ) ) otherlv_3= ')' ( (lv_card_4_0= ruleCardinalityModifier ) )? ) )
            // InternalMethodName.g:828:2: ( ( (lv_hasNot_0_0= '!' ) )? otherlv_1= '(' ( (lv_expr_2_0= ruleExpr ) ) otherlv_3= ')' ( (lv_card_4_0= ruleCardinalityModifier ) )? )
            {
            // InternalMethodName.g:828:2: ( ( (lv_hasNot_0_0= '!' ) )? otherlv_1= '(' ( (lv_expr_2_0= ruleExpr ) ) otherlv_3= ')' ( (lv_card_4_0= ruleCardinalityModifier ) )? )
            // InternalMethodName.g:829:3: ( (lv_hasNot_0_0= '!' ) )? otherlv_1= '(' ( (lv_expr_2_0= ruleExpr ) ) otherlv_3= ')' ( (lv_card_4_0= ruleCardinalityModifier ) )?
            {
            // InternalMethodName.g:829:3: ( (lv_hasNot_0_0= '!' ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==34) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalMethodName.g:830:4: (lv_hasNot_0_0= '!' )
                    {
                    // InternalMethodName.g:830:4: (lv_hasNot_0_0= '!' )
                    // InternalMethodName.g:831:5: lv_hasNot_0_0= '!'
                    {
                    lv_hasNot_0_0=(Token)match(input,34,FOLLOW_24); 

                    					newLeafNode(lv_hasNot_0_0, grammarAccess.getGroupExprAccess().getHasNotExclamationMarkKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getGroupExprRule());
                    					}
                    					setWithLastConsumed(current, "hasNot", lv_hasNot_0_0 != null, "!");
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,35,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getGroupExprAccess().getLeftParenthesisKeyword_1());
            		
            // InternalMethodName.g:847:3: ( (lv_expr_2_0= ruleExpr ) )
            // InternalMethodName.g:848:4: (lv_expr_2_0= ruleExpr )
            {
            // InternalMethodName.g:848:4: (lv_expr_2_0= ruleExpr )
            // InternalMethodName.g:849:5: lv_expr_2_0= ruleExpr
            {

            					newCompositeNode(grammarAccess.getGroupExprAccess().getExprExprParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_25);
            lv_expr_2_0=ruleExpr();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getGroupExprRule());
            					}
            					set(
            						current,
            						"expr",
            						lv_expr_2_0,
            						"org.xtext.example.methodname.MethodName.Expr");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,36,FOLLOW_26); 

            			newLeafNode(otherlv_3, grammarAccess.getGroupExprAccess().getRightParenthesisKeyword_3());
            		
            // InternalMethodName.g:870:3: ( (lv_card_4_0= ruleCardinalityModifier ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( ((LA14_0>=83 && LA14_0<=86)) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalMethodName.g:871:4: (lv_card_4_0= ruleCardinalityModifier )
                    {
                    // InternalMethodName.g:871:4: (lv_card_4_0= ruleCardinalityModifier )
                    // InternalMethodName.g:872:5: lv_card_4_0= ruleCardinalityModifier
                    {

                    					newCompositeNode(grammarAccess.getGroupExprAccess().getCardCardinalityModifierEnumRuleCall_4_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_card_4_0=ruleCardinalityModifier();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getGroupExprRule());
                    					}
                    					set(
                    						current,
                    						"card",
                    						lv_card_4_0,
                    						"org.xtext.example.methodname.MethodName.CardinalityModifier");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGroupExpr"


    // $ANTLR start "entryRuleAtomic"
    // InternalMethodName.g:893:1: entryRuleAtomic returns [EObject current=null] : iv_ruleAtomic= ruleAtomic EOF ;
    public final EObject entryRuleAtomic() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAtomic = null;


        try {
            // InternalMethodName.g:893:47: (iv_ruleAtomic= ruleAtomic EOF )
            // InternalMethodName.g:894:2: iv_ruleAtomic= ruleAtomic EOF
            {
             newCompositeNode(grammarAccess.getAtomicRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAtomic=ruleAtomic();

            state._fsp--;

             current =iv_ruleAtomic; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAtomic"


    // $ANTLR start "ruleAtomic"
    // InternalMethodName.g:900:1: ruleAtomic returns [EObject current=null] : ( ( () ( (lv_hasNot_1_0= '!' ) )? ( (lv_value_2_0= RULE_STRING ) ) ( (lv_card_3_0= ruleCardinalityModifier ) )? (otherlv_4= '[' ( (lv_sentiment_5_0= ruleSentiment ) ) otherlv_6= ']' )? ) | ( () ( (lv_hasNot_8_0= '!' ) )? ( (lv_pos_9_0= rulePOS ) ) ( (lv_card_10_0= ruleCardinalityModifier ) )? (otherlv_11= '[' ( (lv_sentiment_12_0= ruleSentiment ) ) otherlv_13= ']' )? ) | ( () ( (lv_hasNot_15_0= '!' ) )? otherlv_16= '#' ( (lv_value_17_0= RULE_STRING ) ) ( (lv_card_18_0= ruleCardinalityModifier ) )? (otherlv_19= '[' ( (lv_sentiment_20_0= ruleSentiment ) ) otherlv_21= ']' )? ) ) ;
    public final EObject ruleAtomic() throws RecognitionException {
        EObject current = null;

        Token lv_hasNot_1_0=null;
        Token lv_value_2_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token lv_hasNot_8_0=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token lv_hasNot_15_0=null;
        Token otherlv_16=null;
        Token lv_value_17_0=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Enumerator lv_card_3_0 = null;

        Enumerator lv_sentiment_5_0 = null;

        Enumerator lv_pos_9_0 = null;

        Enumerator lv_card_10_0 = null;

        Enumerator lv_sentiment_12_0 = null;

        Enumerator lv_card_18_0 = null;

        Enumerator lv_sentiment_20_0 = null;



        	enterRule();

        try {
            // InternalMethodName.g:906:2: ( ( ( () ( (lv_hasNot_1_0= '!' ) )? ( (lv_value_2_0= RULE_STRING ) ) ( (lv_card_3_0= ruleCardinalityModifier ) )? (otherlv_4= '[' ( (lv_sentiment_5_0= ruleSentiment ) ) otherlv_6= ']' )? ) | ( () ( (lv_hasNot_8_0= '!' ) )? ( (lv_pos_9_0= rulePOS ) ) ( (lv_card_10_0= ruleCardinalityModifier ) )? (otherlv_11= '[' ( (lv_sentiment_12_0= ruleSentiment ) ) otherlv_13= ']' )? ) | ( () ( (lv_hasNot_15_0= '!' ) )? otherlv_16= '#' ( (lv_value_17_0= RULE_STRING ) ) ( (lv_card_18_0= ruleCardinalityModifier ) )? (otherlv_19= '[' ( (lv_sentiment_20_0= ruleSentiment ) ) otherlv_21= ']' )? ) ) )
            // InternalMethodName.g:907:2: ( ( () ( (lv_hasNot_1_0= '!' ) )? ( (lv_value_2_0= RULE_STRING ) ) ( (lv_card_3_0= ruleCardinalityModifier ) )? (otherlv_4= '[' ( (lv_sentiment_5_0= ruleSentiment ) ) otherlv_6= ']' )? ) | ( () ( (lv_hasNot_8_0= '!' ) )? ( (lv_pos_9_0= rulePOS ) ) ( (lv_card_10_0= ruleCardinalityModifier ) )? (otherlv_11= '[' ( (lv_sentiment_12_0= ruleSentiment ) ) otherlv_13= ']' )? ) | ( () ( (lv_hasNot_15_0= '!' ) )? otherlv_16= '#' ( (lv_value_17_0= RULE_STRING ) ) ( (lv_card_18_0= ruleCardinalityModifier ) )? (otherlv_19= '[' ( (lv_sentiment_20_0= ruleSentiment ) ) otherlv_21= ']' )? ) )
            {
            // InternalMethodName.g:907:2: ( ( () ( (lv_hasNot_1_0= '!' ) )? ( (lv_value_2_0= RULE_STRING ) ) ( (lv_card_3_0= ruleCardinalityModifier ) )? (otherlv_4= '[' ( (lv_sentiment_5_0= ruleSentiment ) ) otherlv_6= ']' )? ) | ( () ( (lv_hasNot_8_0= '!' ) )? ( (lv_pos_9_0= rulePOS ) ) ( (lv_card_10_0= ruleCardinalityModifier ) )? (otherlv_11= '[' ( (lv_sentiment_12_0= ruleSentiment ) ) otherlv_13= ']' )? ) | ( () ( (lv_hasNot_15_0= '!' ) )? otherlv_16= '#' ( (lv_value_17_0= RULE_STRING ) ) ( (lv_card_18_0= ruleCardinalityModifier ) )? (otherlv_19= '[' ( (lv_sentiment_20_0= ruleSentiment ) ) otherlv_21= ']' )? ) )
            int alt24=3;
            switch ( input.LA(1) ) {
            case 34:
                {
                switch ( input.LA(2) ) {
                case 49:
                case 50:
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 59:
                case 60:
                case 61:
                case 62:
                case 63:
                case 64:
                case 65:
                case 66:
                case 67:
                case 68:
                case 69:
                case 70:
                case 71:
                case 72:
                case 73:
                case 74:
                case 75:
                case 76:
                case 77:
                case 78:
                case 79:
                case 80:
                case 81:
                case 82:
                    {
                    alt24=2;
                    }
                    break;
                case 39:
                    {
                    alt24=3;
                    }
                    break;
                case RULE_STRING:
                    {
                    alt24=1;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 24, 1, input);

                    throw nvae;
                }

                }
                break;
            case RULE_STRING:
                {
                alt24=1;
                }
                break;
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
            case 67:
            case 68:
            case 69:
            case 70:
            case 71:
            case 72:
            case 73:
            case 74:
            case 75:
            case 76:
            case 77:
            case 78:
            case 79:
            case 80:
            case 81:
            case 82:
                {
                alt24=2;
                }
                break;
            case 39:
                {
                alt24=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }

            switch (alt24) {
                case 1 :
                    // InternalMethodName.g:908:3: ( () ( (lv_hasNot_1_0= '!' ) )? ( (lv_value_2_0= RULE_STRING ) ) ( (lv_card_3_0= ruleCardinalityModifier ) )? (otherlv_4= '[' ( (lv_sentiment_5_0= ruleSentiment ) ) otherlv_6= ']' )? )
                    {
                    // InternalMethodName.g:908:3: ( () ( (lv_hasNot_1_0= '!' ) )? ( (lv_value_2_0= RULE_STRING ) ) ( (lv_card_3_0= ruleCardinalityModifier ) )? (otherlv_4= '[' ( (lv_sentiment_5_0= ruleSentiment ) ) otherlv_6= ']' )? )
                    // InternalMethodName.g:909:4: () ( (lv_hasNot_1_0= '!' ) )? ( (lv_value_2_0= RULE_STRING ) ) ( (lv_card_3_0= ruleCardinalityModifier ) )? (otherlv_4= '[' ( (lv_sentiment_5_0= ruleSentiment ) ) otherlv_6= ']' )?
                    {
                    // InternalMethodName.g:909:4: ()
                    // InternalMethodName.g:910:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicAccess().getStringConstAction_0_0(),
                    						current);
                    				

                    }

                    // InternalMethodName.g:916:4: ( (lv_hasNot_1_0= '!' ) )?
                    int alt15=2;
                    int LA15_0 = input.LA(1);

                    if ( (LA15_0==34) ) {
                        alt15=1;
                    }
                    switch (alt15) {
                        case 1 :
                            // InternalMethodName.g:917:5: (lv_hasNot_1_0= '!' )
                            {
                            // InternalMethodName.g:917:5: (lv_hasNot_1_0= '!' )
                            // InternalMethodName.g:918:6: lv_hasNot_1_0= '!'
                            {
                            lv_hasNot_1_0=(Token)match(input,34,FOLLOW_8); 

                            						newLeafNode(lv_hasNot_1_0, grammarAccess.getAtomicAccess().getHasNotExclamationMarkKeyword_0_1_0());
                            					

                            						if (current==null) {
                            							current = createModelElement(grammarAccess.getAtomicRule());
                            						}
                            						setWithLastConsumed(current, "hasNot", lv_hasNot_1_0 != null, "!");
                            					

                            }


                            }
                            break;

                    }

                    // InternalMethodName.g:930:4: ( (lv_value_2_0= RULE_STRING ) )
                    // InternalMethodName.g:931:5: (lv_value_2_0= RULE_STRING )
                    {
                    // InternalMethodName.g:931:5: (lv_value_2_0= RULE_STRING )
                    // InternalMethodName.g:932:6: lv_value_2_0= RULE_STRING
                    {
                    lv_value_2_0=(Token)match(input,RULE_STRING,FOLLOW_27); 

                    						newLeafNode(lv_value_2_0, grammarAccess.getAtomicAccess().getValueSTRINGTerminalRuleCall_0_2_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_2_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }

                    // InternalMethodName.g:948:4: ( (lv_card_3_0= ruleCardinalityModifier ) )?
                    int alt16=2;
                    int LA16_0 = input.LA(1);

                    if ( ((LA16_0>=83 && LA16_0<=86)) ) {
                        alt16=1;
                    }
                    switch (alt16) {
                        case 1 :
                            // InternalMethodName.g:949:5: (lv_card_3_0= ruleCardinalityModifier )
                            {
                            // InternalMethodName.g:949:5: (lv_card_3_0= ruleCardinalityModifier )
                            // InternalMethodName.g:950:6: lv_card_3_0= ruleCardinalityModifier
                            {

                            						newCompositeNode(grammarAccess.getAtomicAccess().getCardCardinalityModifierEnumRuleCall_0_3_0());
                            					
                            pushFollow(FOLLOW_28);
                            lv_card_3_0=ruleCardinalityModifier();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getAtomicRule());
                            						}
                            						set(
                            							current,
                            							"card",
                            							lv_card_3_0,
                            							"org.xtext.example.methodname.MethodName.CardinalityModifier");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalMethodName.g:967:4: (otherlv_4= '[' ( (lv_sentiment_5_0= ruleSentiment ) ) otherlv_6= ']' )?
                    int alt17=2;
                    int LA17_0 = input.LA(1);

                    if ( (LA17_0==37) ) {
                        alt17=1;
                    }
                    switch (alt17) {
                        case 1 :
                            // InternalMethodName.g:968:5: otherlv_4= '[' ( (lv_sentiment_5_0= ruleSentiment ) ) otherlv_6= ']'
                            {
                            otherlv_4=(Token)match(input,37,FOLLOW_29); 

                            					newLeafNode(otherlv_4, grammarAccess.getAtomicAccess().getLeftSquareBracketKeyword_0_4_0());
                            				
                            // InternalMethodName.g:972:5: ( (lv_sentiment_5_0= ruleSentiment ) )
                            // InternalMethodName.g:973:6: (lv_sentiment_5_0= ruleSentiment )
                            {
                            // InternalMethodName.g:973:6: (lv_sentiment_5_0= ruleSentiment )
                            // InternalMethodName.g:974:7: lv_sentiment_5_0= ruleSentiment
                            {

                            							newCompositeNode(grammarAccess.getAtomicAccess().getSentimentSentimentEnumRuleCall_0_4_1_0());
                            						
                            pushFollow(FOLLOW_30);
                            lv_sentiment_5_0=ruleSentiment();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getAtomicRule());
                            							}
                            							set(
                            								current,
                            								"sentiment",
                            								lv_sentiment_5_0,
                            								"org.xtext.example.methodname.MethodName.Sentiment");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }

                            otherlv_6=(Token)match(input,38,FOLLOW_2); 

                            					newLeafNode(otherlv_6, grammarAccess.getAtomicAccess().getRightSquareBracketKeyword_0_4_2());
                            				

                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalMethodName.g:998:3: ( () ( (lv_hasNot_8_0= '!' ) )? ( (lv_pos_9_0= rulePOS ) ) ( (lv_card_10_0= ruleCardinalityModifier ) )? (otherlv_11= '[' ( (lv_sentiment_12_0= ruleSentiment ) ) otherlv_13= ']' )? )
                    {
                    // InternalMethodName.g:998:3: ( () ( (lv_hasNot_8_0= '!' ) )? ( (lv_pos_9_0= rulePOS ) ) ( (lv_card_10_0= ruleCardinalityModifier ) )? (otherlv_11= '[' ( (lv_sentiment_12_0= ruleSentiment ) ) otherlv_13= ']' )? )
                    // InternalMethodName.g:999:4: () ( (lv_hasNot_8_0= '!' ) )? ( (lv_pos_9_0= rulePOS ) ) ( (lv_card_10_0= ruleCardinalityModifier ) )? (otherlv_11= '[' ( (lv_sentiment_12_0= ruleSentiment ) ) otherlv_13= ']' )?
                    {
                    // InternalMethodName.g:999:4: ()
                    // InternalMethodName.g:1000:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicAccess().getPOSValueAction_1_0(),
                    						current);
                    				

                    }

                    // InternalMethodName.g:1006:4: ( (lv_hasNot_8_0= '!' ) )?
                    int alt18=2;
                    int LA18_0 = input.LA(1);

                    if ( (LA18_0==34) ) {
                        alt18=1;
                    }
                    switch (alt18) {
                        case 1 :
                            // InternalMethodName.g:1007:5: (lv_hasNot_8_0= '!' )
                            {
                            // InternalMethodName.g:1007:5: (lv_hasNot_8_0= '!' )
                            // InternalMethodName.g:1008:6: lv_hasNot_8_0= '!'
                            {
                            lv_hasNot_8_0=(Token)match(input,34,FOLLOW_31); 

                            						newLeafNode(lv_hasNot_8_0, grammarAccess.getAtomicAccess().getHasNotExclamationMarkKeyword_1_1_0());
                            					

                            						if (current==null) {
                            							current = createModelElement(grammarAccess.getAtomicRule());
                            						}
                            						setWithLastConsumed(current, "hasNot", lv_hasNot_8_0 != null, "!");
                            					

                            }


                            }
                            break;

                    }

                    // InternalMethodName.g:1020:4: ( (lv_pos_9_0= rulePOS ) )
                    // InternalMethodName.g:1021:5: (lv_pos_9_0= rulePOS )
                    {
                    // InternalMethodName.g:1021:5: (lv_pos_9_0= rulePOS )
                    // InternalMethodName.g:1022:6: lv_pos_9_0= rulePOS
                    {

                    						newCompositeNode(grammarAccess.getAtomicAccess().getPosPOSEnumRuleCall_1_2_0());
                    					
                    pushFollow(FOLLOW_27);
                    lv_pos_9_0=rulePOS();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAtomicRule());
                    						}
                    						set(
                    							current,
                    							"pos",
                    							lv_pos_9_0,
                    							"org.xtext.example.methodname.MethodName.POS");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalMethodName.g:1039:4: ( (lv_card_10_0= ruleCardinalityModifier ) )?
                    int alt19=2;
                    int LA19_0 = input.LA(1);

                    if ( ((LA19_0>=83 && LA19_0<=86)) ) {
                        alt19=1;
                    }
                    switch (alt19) {
                        case 1 :
                            // InternalMethodName.g:1040:5: (lv_card_10_0= ruleCardinalityModifier )
                            {
                            // InternalMethodName.g:1040:5: (lv_card_10_0= ruleCardinalityModifier )
                            // InternalMethodName.g:1041:6: lv_card_10_0= ruleCardinalityModifier
                            {

                            						newCompositeNode(grammarAccess.getAtomicAccess().getCardCardinalityModifierEnumRuleCall_1_3_0());
                            					
                            pushFollow(FOLLOW_28);
                            lv_card_10_0=ruleCardinalityModifier();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getAtomicRule());
                            						}
                            						set(
                            							current,
                            							"card",
                            							lv_card_10_0,
                            							"org.xtext.example.methodname.MethodName.CardinalityModifier");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalMethodName.g:1058:4: (otherlv_11= '[' ( (lv_sentiment_12_0= ruleSentiment ) ) otherlv_13= ']' )?
                    int alt20=2;
                    int LA20_0 = input.LA(1);

                    if ( (LA20_0==37) ) {
                        alt20=1;
                    }
                    switch (alt20) {
                        case 1 :
                            // InternalMethodName.g:1059:5: otherlv_11= '[' ( (lv_sentiment_12_0= ruleSentiment ) ) otherlv_13= ']'
                            {
                            otherlv_11=(Token)match(input,37,FOLLOW_29); 

                            					newLeafNode(otherlv_11, grammarAccess.getAtomicAccess().getLeftSquareBracketKeyword_1_4_0());
                            				
                            // InternalMethodName.g:1063:5: ( (lv_sentiment_12_0= ruleSentiment ) )
                            // InternalMethodName.g:1064:6: (lv_sentiment_12_0= ruleSentiment )
                            {
                            // InternalMethodName.g:1064:6: (lv_sentiment_12_0= ruleSentiment )
                            // InternalMethodName.g:1065:7: lv_sentiment_12_0= ruleSentiment
                            {

                            							newCompositeNode(grammarAccess.getAtomicAccess().getSentimentSentimentEnumRuleCall_1_4_1_0());
                            						
                            pushFollow(FOLLOW_30);
                            lv_sentiment_12_0=ruleSentiment();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getAtomicRule());
                            							}
                            							set(
                            								current,
                            								"sentiment",
                            								lv_sentiment_12_0,
                            								"org.xtext.example.methodname.MethodName.Sentiment");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }

                            otherlv_13=(Token)match(input,38,FOLLOW_2); 

                            					newLeafNode(otherlv_13, grammarAccess.getAtomicAccess().getRightSquareBracketKeyword_1_4_2());
                            				

                            }
                            break;

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalMethodName.g:1089:3: ( () ( (lv_hasNot_15_0= '!' ) )? otherlv_16= '#' ( (lv_value_17_0= RULE_STRING ) ) ( (lv_card_18_0= ruleCardinalityModifier ) )? (otherlv_19= '[' ( (lv_sentiment_20_0= ruleSentiment ) ) otherlv_21= ']' )? )
                    {
                    // InternalMethodName.g:1089:3: ( () ( (lv_hasNot_15_0= '!' ) )? otherlv_16= '#' ( (lv_value_17_0= RULE_STRING ) ) ( (lv_card_18_0= ruleCardinalityModifier ) )? (otherlv_19= '[' ( (lv_sentiment_20_0= ruleSentiment ) ) otherlv_21= ']' )? )
                    // InternalMethodName.g:1090:4: () ( (lv_hasNot_15_0= '!' ) )? otherlv_16= '#' ( (lv_value_17_0= RULE_STRING ) ) ( (lv_card_18_0= ruleCardinalityModifier ) )? (otherlv_19= '[' ( (lv_sentiment_20_0= ruleSentiment ) ) otherlv_21= ']' )?
                    {
                    // InternalMethodName.g:1090:4: ()
                    // InternalMethodName.g:1091:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicAccess().getSynonymConstAction_2_0(),
                    						current);
                    				

                    }

                    // InternalMethodName.g:1097:4: ( (lv_hasNot_15_0= '!' ) )?
                    int alt21=2;
                    int LA21_0 = input.LA(1);

                    if ( (LA21_0==34) ) {
                        alt21=1;
                    }
                    switch (alt21) {
                        case 1 :
                            // InternalMethodName.g:1098:5: (lv_hasNot_15_0= '!' )
                            {
                            // InternalMethodName.g:1098:5: (lv_hasNot_15_0= '!' )
                            // InternalMethodName.g:1099:6: lv_hasNot_15_0= '!'
                            {
                            lv_hasNot_15_0=(Token)match(input,34,FOLLOW_32); 

                            						newLeafNode(lv_hasNot_15_0, grammarAccess.getAtomicAccess().getHasNotExclamationMarkKeyword_2_1_0());
                            					

                            						if (current==null) {
                            							current = createModelElement(grammarAccess.getAtomicRule());
                            						}
                            						setWithLastConsumed(current, "hasNot", lv_hasNot_15_0 != null, "!");
                            					

                            }


                            }
                            break;

                    }

                    otherlv_16=(Token)match(input,39,FOLLOW_8); 

                    				newLeafNode(otherlv_16, grammarAccess.getAtomicAccess().getNumberSignKeyword_2_2());
                    			
                    // InternalMethodName.g:1115:4: ( (lv_value_17_0= RULE_STRING ) )
                    // InternalMethodName.g:1116:5: (lv_value_17_0= RULE_STRING )
                    {
                    // InternalMethodName.g:1116:5: (lv_value_17_0= RULE_STRING )
                    // InternalMethodName.g:1117:6: lv_value_17_0= RULE_STRING
                    {
                    lv_value_17_0=(Token)match(input,RULE_STRING,FOLLOW_27); 

                    						newLeafNode(lv_value_17_0, grammarAccess.getAtomicAccess().getValueSTRINGTerminalRuleCall_2_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_17_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }

                    // InternalMethodName.g:1133:4: ( (lv_card_18_0= ruleCardinalityModifier ) )?
                    int alt22=2;
                    int LA22_0 = input.LA(1);

                    if ( ((LA22_0>=83 && LA22_0<=86)) ) {
                        alt22=1;
                    }
                    switch (alt22) {
                        case 1 :
                            // InternalMethodName.g:1134:5: (lv_card_18_0= ruleCardinalityModifier )
                            {
                            // InternalMethodName.g:1134:5: (lv_card_18_0= ruleCardinalityModifier )
                            // InternalMethodName.g:1135:6: lv_card_18_0= ruleCardinalityModifier
                            {

                            						newCompositeNode(grammarAccess.getAtomicAccess().getCardCardinalityModifierEnumRuleCall_2_4_0());
                            					
                            pushFollow(FOLLOW_28);
                            lv_card_18_0=ruleCardinalityModifier();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getAtomicRule());
                            						}
                            						set(
                            							current,
                            							"card",
                            							lv_card_18_0,
                            							"org.xtext.example.methodname.MethodName.CardinalityModifier");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalMethodName.g:1152:4: (otherlv_19= '[' ( (lv_sentiment_20_0= ruleSentiment ) ) otherlv_21= ']' )?
                    int alt23=2;
                    int LA23_0 = input.LA(1);

                    if ( (LA23_0==37) ) {
                        alt23=1;
                    }
                    switch (alt23) {
                        case 1 :
                            // InternalMethodName.g:1153:5: otherlv_19= '[' ( (lv_sentiment_20_0= ruleSentiment ) ) otherlv_21= ']'
                            {
                            otherlv_19=(Token)match(input,37,FOLLOW_29); 

                            					newLeafNode(otherlv_19, grammarAccess.getAtomicAccess().getLeftSquareBracketKeyword_2_5_0());
                            				
                            // InternalMethodName.g:1157:5: ( (lv_sentiment_20_0= ruleSentiment ) )
                            // InternalMethodName.g:1158:6: (lv_sentiment_20_0= ruleSentiment )
                            {
                            // InternalMethodName.g:1158:6: (lv_sentiment_20_0= ruleSentiment )
                            // InternalMethodName.g:1159:7: lv_sentiment_20_0= ruleSentiment
                            {

                            							newCompositeNode(grammarAccess.getAtomicAccess().getSentimentSentimentEnumRuleCall_2_5_1_0());
                            						
                            pushFollow(FOLLOW_30);
                            lv_sentiment_20_0=ruleSentiment();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getAtomicRule());
                            							}
                            							set(
                            								current,
                            								"sentiment",
                            								lv_sentiment_20_0,
                            								"org.xtext.example.methodname.MethodName.Sentiment");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }

                            otherlv_21=(Token)match(input,38,FOLLOW_2); 

                            					newLeafNode(otherlv_21, grammarAccess.getAtomicAccess().getRightSquareBracketKeyword_2_5_2());
                            				

                            }
                            break;

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAtomic"


    // $ANTLR start "entryRuleProperty"
    // InternalMethodName.g:1186:1: entryRuleProperty returns [EObject current=null] : iv_ruleProperty= ruleProperty EOF ;
    public final EObject entryRuleProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProperty = null;


        try {
            // InternalMethodName.g:1186:49: (iv_ruleProperty= ruleProperty EOF )
            // InternalMethodName.g:1187:2: iv_ruleProperty= ruleProperty EOF
            {
             newCompositeNode(grammarAccess.getPropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleProperty=ruleProperty();

            state._fsp--;

             current =iv_ruleProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProperty"


    // $ANTLR start "ruleProperty"
    // InternalMethodName.g:1193:1: ruleProperty returns [EObject current=null] : ( ( (lv_frequency_0_0= ruleFrequency ) )? ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleProperty() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Enumerator lv_frequency_0_0 = null;



        	enterRule();

        try {
            // InternalMethodName.g:1199:2: ( ( ( (lv_frequency_0_0= ruleFrequency ) )? ( (otherlv_1= RULE_ID ) ) ) )
            // InternalMethodName.g:1200:2: ( ( (lv_frequency_0_0= ruleFrequency ) )? ( (otherlv_1= RULE_ID ) ) )
            {
            // InternalMethodName.g:1200:2: ( ( (lv_frequency_0_0= ruleFrequency ) )? ( (otherlv_1= RULE_ID ) ) )
            // InternalMethodName.g:1201:3: ( (lv_frequency_0_0= ruleFrequency ) )? ( (otherlv_1= RULE_ID ) )
            {
            // InternalMethodName.g:1201:3: ( (lv_frequency_0_0= ruleFrequency ) )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( ((LA25_0>=87 && LA25_0<=92)) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalMethodName.g:1202:4: (lv_frequency_0_0= ruleFrequency )
                    {
                    // InternalMethodName.g:1202:4: (lv_frequency_0_0= ruleFrequency )
                    // InternalMethodName.g:1203:5: lv_frequency_0_0= ruleFrequency
                    {

                    					newCompositeNode(grammarAccess.getPropertyAccess().getFrequencyFrequencyEnumRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_9);
                    lv_frequency_0_0=ruleFrequency();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getPropertyRule());
                    					}
                    					set(
                    						current,
                    						"frequency",
                    						lv_frequency_0_0,
                    						"org.xtext.example.methodname.MethodName.Frequency");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalMethodName.g:1220:3: ( (otherlv_1= RULE_ID ) )
            // InternalMethodName.g:1221:4: (otherlv_1= RULE_ID )
            {
            // InternalMethodName.g:1221:4: (otherlv_1= RULE_ID )
            // InternalMethodName.g:1222:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPropertyRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_1, grammarAccess.getPropertyAccess().getCriterionRuleCrossReference_1_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProperty"


    // $ANTLR start "ruleKind"
    // InternalMethodName.g:1237:1: ruleKind returns [Enumerator current=null] : ( (enumLiteral_0= 'interface' ) | (enumLiteral_1= 'package' ) | (enumLiteral_2= 'class' ) | (enumLiteral_3= 'method' ) | (enumLiteral_4= 'variable' ) ) ;
    public final Enumerator ruleKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;


        	enterRule();

        try {
            // InternalMethodName.g:1243:2: ( ( (enumLiteral_0= 'interface' ) | (enumLiteral_1= 'package' ) | (enumLiteral_2= 'class' ) | (enumLiteral_3= 'method' ) | (enumLiteral_4= 'variable' ) ) )
            // InternalMethodName.g:1244:2: ( (enumLiteral_0= 'interface' ) | (enumLiteral_1= 'package' ) | (enumLiteral_2= 'class' ) | (enumLiteral_3= 'method' ) | (enumLiteral_4= 'variable' ) )
            {
            // InternalMethodName.g:1244:2: ( (enumLiteral_0= 'interface' ) | (enumLiteral_1= 'package' ) | (enumLiteral_2= 'class' ) | (enumLiteral_3= 'method' ) | (enumLiteral_4= 'variable' ) )
            int alt26=5;
            switch ( input.LA(1) ) {
            case 40:
                {
                alt26=1;
                }
                break;
            case 41:
                {
                alt26=2;
                }
                break;
            case 42:
                {
                alt26=3;
                }
                break;
            case 43:
                {
                alt26=4;
                }
                break;
            case 44:
                {
                alt26=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;
            }

            switch (alt26) {
                case 1 :
                    // InternalMethodName.g:1245:3: (enumLiteral_0= 'interface' )
                    {
                    // InternalMethodName.g:1245:3: (enumLiteral_0= 'interface' )
                    // InternalMethodName.g:1246:4: enumLiteral_0= 'interface'
                    {
                    enumLiteral_0=(Token)match(input,40,FOLLOW_2); 

                    				current = grammarAccess.getKindAccess().getKIND_INTERFACEEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getKindAccess().getKIND_INTERFACEEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMethodName.g:1253:3: (enumLiteral_1= 'package' )
                    {
                    // InternalMethodName.g:1253:3: (enumLiteral_1= 'package' )
                    // InternalMethodName.g:1254:4: enumLiteral_1= 'package'
                    {
                    enumLiteral_1=(Token)match(input,41,FOLLOW_2); 

                    				current = grammarAccess.getKindAccess().getKIND_PACKAGEEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getKindAccess().getKIND_PACKAGEEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalMethodName.g:1261:3: (enumLiteral_2= 'class' )
                    {
                    // InternalMethodName.g:1261:3: (enumLiteral_2= 'class' )
                    // InternalMethodName.g:1262:4: enumLiteral_2= 'class'
                    {
                    enumLiteral_2=(Token)match(input,42,FOLLOW_2); 

                    				current = grammarAccess.getKindAccess().getKIND_CLASSEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getKindAccess().getKIND_CLASSEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalMethodName.g:1269:3: (enumLiteral_3= 'method' )
                    {
                    // InternalMethodName.g:1269:3: (enumLiteral_3= 'method' )
                    // InternalMethodName.g:1270:4: enumLiteral_3= 'method'
                    {
                    enumLiteral_3=(Token)match(input,43,FOLLOW_2); 

                    				current = grammarAccess.getKindAccess().getKIND_METHODEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getKindAccess().getKIND_METHODEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalMethodName.g:1277:3: (enumLiteral_4= 'variable' )
                    {
                    // InternalMethodName.g:1277:3: (enumLiteral_4= 'variable' )
                    // InternalMethodName.g:1278:4: enumLiteral_4= 'variable'
                    {
                    enumLiteral_4=(Token)match(input,44,FOLLOW_2); 

                    				current = grammarAccess.getKindAccess().getKIND_VAREnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_4, grammarAccess.getKindAccess().getKIND_VAREnumLiteralDeclaration_4());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleKind"


    // $ANTLR start "ruleSentiment"
    // InternalMethodName.g:1288:1: ruleSentiment returns [Enumerator current=null] : ( (enumLiteral_0= 'SENT_UNSPECIFIED' ) | (enumLiteral_1= 'neutral' ) | (enumLiteral_2= 'positive' ) | (enumLiteral_3= 'negative' ) ) ;
    public final Enumerator ruleSentiment() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalMethodName.g:1294:2: ( ( (enumLiteral_0= 'SENT_UNSPECIFIED' ) | (enumLiteral_1= 'neutral' ) | (enumLiteral_2= 'positive' ) | (enumLiteral_3= 'negative' ) ) )
            // InternalMethodName.g:1295:2: ( (enumLiteral_0= 'SENT_UNSPECIFIED' ) | (enumLiteral_1= 'neutral' ) | (enumLiteral_2= 'positive' ) | (enumLiteral_3= 'negative' ) )
            {
            // InternalMethodName.g:1295:2: ( (enumLiteral_0= 'SENT_UNSPECIFIED' ) | (enumLiteral_1= 'neutral' ) | (enumLiteral_2= 'positive' ) | (enumLiteral_3= 'negative' ) )
            int alt27=4;
            switch ( input.LA(1) ) {
            case 45:
                {
                alt27=1;
                }
                break;
            case 46:
                {
                alt27=2;
                }
                break;
            case 47:
                {
                alt27=3;
                }
                break;
            case 48:
                {
                alt27=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;
            }

            switch (alt27) {
                case 1 :
                    // InternalMethodName.g:1296:3: (enumLiteral_0= 'SENT_UNSPECIFIED' )
                    {
                    // InternalMethodName.g:1296:3: (enumLiteral_0= 'SENT_UNSPECIFIED' )
                    // InternalMethodName.g:1297:4: enumLiteral_0= 'SENT_UNSPECIFIED'
                    {
                    enumLiteral_0=(Token)match(input,45,FOLLOW_2); 

                    				current = grammarAccess.getSentimentAccess().getSENT_UNSPECIFIEDEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getSentimentAccess().getSENT_UNSPECIFIEDEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMethodName.g:1304:3: (enumLiteral_1= 'neutral' )
                    {
                    // InternalMethodName.g:1304:3: (enumLiteral_1= 'neutral' )
                    // InternalMethodName.g:1305:4: enumLiteral_1= 'neutral'
                    {
                    enumLiteral_1=(Token)match(input,46,FOLLOW_2); 

                    				current = grammarAccess.getSentimentAccess().getSENT_NEUTRALEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getSentimentAccess().getSENT_NEUTRALEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalMethodName.g:1312:3: (enumLiteral_2= 'positive' )
                    {
                    // InternalMethodName.g:1312:3: (enumLiteral_2= 'positive' )
                    // InternalMethodName.g:1313:4: enumLiteral_2= 'positive'
                    {
                    enumLiteral_2=(Token)match(input,47,FOLLOW_2); 

                    				current = grammarAccess.getSentimentAccess().getSENT_POSITIVEEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getSentimentAccess().getSENT_POSITIVEEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalMethodName.g:1320:3: (enumLiteral_3= 'negative' )
                    {
                    // InternalMethodName.g:1320:3: (enumLiteral_3= 'negative' )
                    // InternalMethodName.g:1321:4: enumLiteral_3= 'negative'
                    {
                    enumLiteral_3=(Token)match(input,48,FOLLOW_2); 

                    				current = grammarAccess.getSentimentAccess().getSENT_NEGATIVEEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getSentimentAccess().getSENT_NEGATIVEEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSentiment"


    // $ANTLR start "rulePOS"
    // InternalMethodName.g:1331:1: rulePOS returns [Enumerator current=null] : ( (enumLiteral_0= 'CC' ) | (enumLiteral_1= 'CD' ) | (enumLiteral_2= 'DT' ) | (enumLiteral_3= 'EX' ) | (enumLiteral_4= 'FW' ) | (enumLiteral_5= 'IN' ) | (enumLiteral_6= 'JJ' ) | (enumLiteral_7= 'JJR' ) | (enumLiteral_8= 'JJS' ) | (enumLiteral_9= 'LS' ) | (enumLiteral_10= 'MD' ) | (enumLiteral_11= 'NN' ) | (enumLiteral_12= 'NNS' ) | (enumLiteral_13= 'NNP' ) | (enumLiteral_14= 'NNPS' ) | (enumLiteral_15= 'PDT' ) | (enumLiteral_16= 'POS' ) | (enumLiteral_17= 'PRP' ) | (enumLiteral_18= 'RB' ) | (enumLiteral_19= 'RBR' ) | (enumLiteral_20= 'RBS' ) | (enumLiteral_21= 'RP' ) | (enumLiteral_22= 'SYM' ) | (enumLiteral_23= 'TO' ) | (enumLiteral_24= 'UH' ) | (enumLiteral_25= 'VB' ) | (enumLiteral_26= 'VBD' ) | (enumLiteral_27= 'VBG' ) | (enumLiteral_28= 'VBN' ) | (enumLiteral_29= 'VBP' ) | (enumLiteral_30= 'VBZ' ) | (enumLiteral_31= 'WDT' ) | (enumLiteral_32= 'WP' ) | (enumLiteral_33= 'WRB' ) ) ;
    public final Enumerator rulePOS() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;
        Token enumLiteral_8=null;
        Token enumLiteral_9=null;
        Token enumLiteral_10=null;
        Token enumLiteral_11=null;
        Token enumLiteral_12=null;
        Token enumLiteral_13=null;
        Token enumLiteral_14=null;
        Token enumLiteral_15=null;
        Token enumLiteral_16=null;
        Token enumLiteral_17=null;
        Token enumLiteral_18=null;
        Token enumLiteral_19=null;
        Token enumLiteral_20=null;
        Token enumLiteral_21=null;
        Token enumLiteral_22=null;
        Token enumLiteral_23=null;
        Token enumLiteral_24=null;
        Token enumLiteral_25=null;
        Token enumLiteral_26=null;
        Token enumLiteral_27=null;
        Token enumLiteral_28=null;
        Token enumLiteral_29=null;
        Token enumLiteral_30=null;
        Token enumLiteral_31=null;
        Token enumLiteral_32=null;
        Token enumLiteral_33=null;


        	enterRule();

        try {
            // InternalMethodName.g:1337:2: ( ( (enumLiteral_0= 'CC' ) | (enumLiteral_1= 'CD' ) | (enumLiteral_2= 'DT' ) | (enumLiteral_3= 'EX' ) | (enumLiteral_4= 'FW' ) | (enumLiteral_5= 'IN' ) | (enumLiteral_6= 'JJ' ) | (enumLiteral_7= 'JJR' ) | (enumLiteral_8= 'JJS' ) | (enumLiteral_9= 'LS' ) | (enumLiteral_10= 'MD' ) | (enumLiteral_11= 'NN' ) | (enumLiteral_12= 'NNS' ) | (enumLiteral_13= 'NNP' ) | (enumLiteral_14= 'NNPS' ) | (enumLiteral_15= 'PDT' ) | (enumLiteral_16= 'POS' ) | (enumLiteral_17= 'PRP' ) | (enumLiteral_18= 'RB' ) | (enumLiteral_19= 'RBR' ) | (enumLiteral_20= 'RBS' ) | (enumLiteral_21= 'RP' ) | (enumLiteral_22= 'SYM' ) | (enumLiteral_23= 'TO' ) | (enumLiteral_24= 'UH' ) | (enumLiteral_25= 'VB' ) | (enumLiteral_26= 'VBD' ) | (enumLiteral_27= 'VBG' ) | (enumLiteral_28= 'VBN' ) | (enumLiteral_29= 'VBP' ) | (enumLiteral_30= 'VBZ' ) | (enumLiteral_31= 'WDT' ) | (enumLiteral_32= 'WP' ) | (enumLiteral_33= 'WRB' ) ) )
            // InternalMethodName.g:1338:2: ( (enumLiteral_0= 'CC' ) | (enumLiteral_1= 'CD' ) | (enumLiteral_2= 'DT' ) | (enumLiteral_3= 'EX' ) | (enumLiteral_4= 'FW' ) | (enumLiteral_5= 'IN' ) | (enumLiteral_6= 'JJ' ) | (enumLiteral_7= 'JJR' ) | (enumLiteral_8= 'JJS' ) | (enumLiteral_9= 'LS' ) | (enumLiteral_10= 'MD' ) | (enumLiteral_11= 'NN' ) | (enumLiteral_12= 'NNS' ) | (enumLiteral_13= 'NNP' ) | (enumLiteral_14= 'NNPS' ) | (enumLiteral_15= 'PDT' ) | (enumLiteral_16= 'POS' ) | (enumLiteral_17= 'PRP' ) | (enumLiteral_18= 'RB' ) | (enumLiteral_19= 'RBR' ) | (enumLiteral_20= 'RBS' ) | (enumLiteral_21= 'RP' ) | (enumLiteral_22= 'SYM' ) | (enumLiteral_23= 'TO' ) | (enumLiteral_24= 'UH' ) | (enumLiteral_25= 'VB' ) | (enumLiteral_26= 'VBD' ) | (enumLiteral_27= 'VBG' ) | (enumLiteral_28= 'VBN' ) | (enumLiteral_29= 'VBP' ) | (enumLiteral_30= 'VBZ' ) | (enumLiteral_31= 'WDT' ) | (enumLiteral_32= 'WP' ) | (enumLiteral_33= 'WRB' ) )
            {
            // InternalMethodName.g:1338:2: ( (enumLiteral_0= 'CC' ) | (enumLiteral_1= 'CD' ) | (enumLiteral_2= 'DT' ) | (enumLiteral_3= 'EX' ) | (enumLiteral_4= 'FW' ) | (enumLiteral_5= 'IN' ) | (enumLiteral_6= 'JJ' ) | (enumLiteral_7= 'JJR' ) | (enumLiteral_8= 'JJS' ) | (enumLiteral_9= 'LS' ) | (enumLiteral_10= 'MD' ) | (enumLiteral_11= 'NN' ) | (enumLiteral_12= 'NNS' ) | (enumLiteral_13= 'NNP' ) | (enumLiteral_14= 'NNPS' ) | (enumLiteral_15= 'PDT' ) | (enumLiteral_16= 'POS' ) | (enumLiteral_17= 'PRP' ) | (enumLiteral_18= 'RB' ) | (enumLiteral_19= 'RBR' ) | (enumLiteral_20= 'RBS' ) | (enumLiteral_21= 'RP' ) | (enumLiteral_22= 'SYM' ) | (enumLiteral_23= 'TO' ) | (enumLiteral_24= 'UH' ) | (enumLiteral_25= 'VB' ) | (enumLiteral_26= 'VBD' ) | (enumLiteral_27= 'VBG' ) | (enumLiteral_28= 'VBN' ) | (enumLiteral_29= 'VBP' ) | (enumLiteral_30= 'VBZ' ) | (enumLiteral_31= 'WDT' ) | (enumLiteral_32= 'WP' ) | (enumLiteral_33= 'WRB' ) )
            int alt28=34;
            switch ( input.LA(1) ) {
            case 49:
                {
                alt28=1;
                }
                break;
            case 50:
                {
                alt28=2;
                }
                break;
            case 51:
                {
                alt28=3;
                }
                break;
            case 52:
                {
                alt28=4;
                }
                break;
            case 53:
                {
                alt28=5;
                }
                break;
            case 54:
                {
                alt28=6;
                }
                break;
            case 55:
                {
                alt28=7;
                }
                break;
            case 56:
                {
                alt28=8;
                }
                break;
            case 57:
                {
                alt28=9;
                }
                break;
            case 58:
                {
                alt28=10;
                }
                break;
            case 59:
                {
                alt28=11;
                }
                break;
            case 60:
                {
                alt28=12;
                }
                break;
            case 61:
                {
                alt28=13;
                }
                break;
            case 62:
                {
                alt28=14;
                }
                break;
            case 63:
                {
                alt28=15;
                }
                break;
            case 64:
                {
                alt28=16;
                }
                break;
            case 65:
                {
                alt28=17;
                }
                break;
            case 66:
                {
                alt28=18;
                }
                break;
            case 67:
                {
                alt28=19;
                }
                break;
            case 68:
                {
                alt28=20;
                }
                break;
            case 69:
                {
                alt28=21;
                }
                break;
            case 70:
                {
                alt28=22;
                }
                break;
            case 71:
                {
                alt28=23;
                }
                break;
            case 72:
                {
                alt28=24;
                }
                break;
            case 73:
                {
                alt28=25;
                }
                break;
            case 74:
                {
                alt28=26;
                }
                break;
            case 75:
                {
                alt28=27;
                }
                break;
            case 76:
                {
                alt28=28;
                }
                break;
            case 77:
                {
                alt28=29;
                }
                break;
            case 78:
                {
                alt28=30;
                }
                break;
            case 79:
                {
                alt28=31;
                }
                break;
            case 80:
                {
                alt28=32;
                }
                break;
            case 81:
                {
                alt28=33;
                }
                break;
            case 82:
                {
                alt28=34;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }

            switch (alt28) {
                case 1 :
                    // InternalMethodName.g:1339:3: (enumLiteral_0= 'CC' )
                    {
                    // InternalMethodName.g:1339:3: (enumLiteral_0= 'CC' )
                    // InternalMethodName.g:1340:4: enumLiteral_0= 'CC'
                    {
                    enumLiteral_0=(Token)match(input,49,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_CCEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getPOSAccess().getPOS_CCEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMethodName.g:1347:3: (enumLiteral_1= 'CD' )
                    {
                    // InternalMethodName.g:1347:3: (enumLiteral_1= 'CD' )
                    // InternalMethodName.g:1348:4: enumLiteral_1= 'CD'
                    {
                    enumLiteral_1=(Token)match(input,50,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_CDEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getPOSAccess().getPOS_CDEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalMethodName.g:1355:3: (enumLiteral_2= 'DT' )
                    {
                    // InternalMethodName.g:1355:3: (enumLiteral_2= 'DT' )
                    // InternalMethodName.g:1356:4: enumLiteral_2= 'DT'
                    {
                    enumLiteral_2=(Token)match(input,51,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_DTEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getPOSAccess().getPOS_DTEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalMethodName.g:1363:3: (enumLiteral_3= 'EX' )
                    {
                    // InternalMethodName.g:1363:3: (enumLiteral_3= 'EX' )
                    // InternalMethodName.g:1364:4: enumLiteral_3= 'EX'
                    {
                    enumLiteral_3=(Token)match(input,52,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_EXEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getPOSAccess().getPOS_EXEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalMethodName.g:1371:3: (enumLiteral_4= 'FW' )
                    {
                    // InternalMethodName.g:1371:3: (enumLiteral_4= 'FW' )
                    // InternalMethodName.g:1372:4: enumLiteral_4= 'FW'
                    {
                    enumLiteral_4=(Token)match(input,53,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_FWEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_4, grammarAccess.getPOSAccess().getPOS_FWEnumLiteralDeclaration_4());
                    			

                    }


                    }
                    break;
                case 6 :
                    // InternalMethodName.g:1379:3: (enumLiteral_5= 'IN' )
                    {
                    // InternalMethodName.g:1379:3: (enumLiteral_5= 'IN' )
                    // InternalMethodName.g:1380:4: enumLiteral_5= 'IN'
                    {
                    enumLiteral_5=(Token)match(input,54,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_INEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_5, grammarAccess.getPOSAccess().getPOS_INEnumLiteralDeclaration_5());
                    			

                    }


                    }
                    break;
                case 7 :
                    // InternalMethodName.g:1387:3: (enumLiteral_6= 'JJ' )
                    {
                    // InternalMethodName.g:1387:3: (enumLiteral_6= 'JJ' )
                    // InternalMethodName.g:1388:4: enumLiteral_6= 'JJ'
                    {
                    enumLiteral_6=(Token)match(input,55,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_JJEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_6, grammarAccess.getPOSAccess().getPOS_JJEnumLiteralDeclaration_6());
                    			

                    }


                    }
                    break;
                case 8 :
                    // InternalMethodName.g:1395:3: (enumLiteral_7= 'JJR' )
                    {
                    // InternalMethodName.g:1395:3: (enumLiteral_7= 'JJR' )
                    // InternalMethodName.g:1396:4: enumLiteral_7= 'JJR'
                    {
                    enumLiteral_7=(Token)match(input,56,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_JJREnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_7, grammarAccess.getPOSAccess().getPOS_JJREnumLiteralDeclaration_7());
                    			

                    }


                    }
                    break;
                case 9 :
                    // InternalMethodName.g:1403:3: (enumLiteral_8= 'JJS' )
                    {
                    // InternalMethodName.g:1403:3: (enumLiteral_8= 'JJS' )
                    // InternalMethodName.g:1404:4: enumLiteral_8= 'JJS'
                    {
                    enumLiteral_8=(Token)match(input,57,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_JJSEnumLiteralDeclaration_8().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_8, grammarAccess.getPOSAccess().getPOS_JJSEnumLiteralDeclaration_8());
                    			

                    }


                    }
                    break;
                case 10 :
                    // InternalMethodName.g:1411:3: (enumLiteral_9= 'LS' )
                    {
                    // InternalMethodName.g:1411:3: (enumLiteral_9= 'LS' )
                    // InternalMethodName.g:1412:4: enumLiteral_9= 'LS'
                    {
                    enumLiteral_9=(Token)match(input,58,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_LSEnumLiteralDeclaration_9().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_9, grammarAccess.getPOSAccess().getPOS_LSEnumLiteralDeclaration_9());
                    			

                    }


                    }
                    break;
                case 11 :
                    // InternalMethodName.g:1419:3: (enumLiteral_10= 'MD' )
                    {
                    // InternalMethodName.g:1419:3: (enumLiteral_10= 'MD' )
                    // InternalMethodName.g:1420:4: enumLiteral_10= 'MD'
                    {
                    enumLiteral_10=(Token)match(input,59,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_MDEnumLiteralDeclaration_10().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_10, grammarAccess.getPOSAccess().getPOS_MDEnumLiteralDeclaration_10());
                    			

                    }


                    }
                    break;
                case 12 :
                    // InternalMethodName.g:1427:3: (enumLiteral_11= 'NN' )
                    {
                    // InternalMethodName.g:1427:3: (enumLiteral_11= 'NN' )
                    // InternalMethodName.g:1428:4: enumLiteral_11= 'NN'
                    {
                    enumLiteral_11=(Token)match(input,60,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_NNEnumLiteralDeclaration_11().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_11, grammarAccess.getPOSAccess().getPOS_NNEnumLiteralDeclaration_11());
                    			

                    }


                    }
                    break;
                case 13 :
                    // InternalMethodName.g:1435:3: (enumLiteral_12= 'NNS' )
                    {
                    // InternalMethodName.g:1435:3: (enumLiteral_12= 'NNS' )
                    // InternalMethodName.g:1436:4: enumLiteral_12= 'NNS'
                    {
                    enumLiteral_12=(Token)match(input,61,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_NNSEnumLiteralDeclaration_12().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_12, grammarAccess.getPOSAccess().getPOS_NNSEnumLiteralDeclaration_12());
                    			

                    }


                    }
                    break;
                case 14 :
                    // InternalMethodName.g:1443:3: (enumLiteral_13= 'NNP' )
                    {
                    // InternalMethodName.g:1443:3: (enumLiteral_13= 'NNP' )
                    // InternalMethodName.g:1444:4: enumLiteral_13= 'NNP'
                    {
                    enumLiteral_13=(Token)match(input,62,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_NNPEnumLiteralDeclaration_13().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_13, grammarAccess.getPOSAccess().getPOS_NNPEnumLiteralDeclaration_13());
                    			

                    }


                    }
                    break;
                case 15 :
                    // InternalMethodName.g:1451:3: (enumLiteral_14= 'NNPS' )
                    {
                    // InternalMethodName.g:1451:3: (enumLiteral_14= 'NNPS' )
                    // InternalMethodName.g:1452:4: enumLiteral_14= 'NNPS'
                    {
                    enumLiteral_14=(Token)match(input,63,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_NNPSEnumLiteralDeclaration_14().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_14, grammarAccess.getPOSAccess().getPOS_NNPSEnumLiteralDeclaration_14());
                    			

                    }


                    }
                    break;
                case 16 :
                    // InternalMethodName.g:1459:3: (enumLiteral_15= 'PDT' )
                    {
                    // InternalMethodName.g:1459:3: (enumLiteral_15= 'PDT' )
                    // InternalMethodName.g:1460:4: enumLiteral_15= 'PDT'
                    {
                    enumLiteral_15=(Token)match(input,64,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_PDTEnumLiteralDeclaration_15().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_15, grammarAccess.getPOSAccess().getPOS_PDTEnumLiteralDeclaration_15());
                    			

                    }


                    }
                    break;
                case 17 :
                    // InternalMethodName.g:1467:3: (enumLiteral_16= 'POS' )
                    {
                    // InternalMethodName.g:1467:3: (enumLiteral_16= 'POS' )
                    // InternalMethodName.g:1468:4: enumLiteral_16= 'POS'
                    {
                    enumLiteral_16=(Token)match(input,65,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_POSEnumLiteralDeclaration_16().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_16, grammarAccess.getPOSAccess().getPOS_POSEnumLiteralDeclaration_16());
                    			

                    }


                    }
                    break;
                case 18 :
                    // InternalMethodName.g:1475:3: (enumLiteral_17= 'PRP' )
                    {
                    // InternalMethodName.g:1475:3: (enumLiteral_17= 'PRP' )
                    // InternalMethodName.g:1476:4: enumLiteral_17= 'PRP'
                    {
                    enumLiteral_17=(Token)match(input,66,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_PRPEnumLiteralDeclaration_17().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_17, grammarAccess.getPOSAccess().getPOS_PRPEnumLiteralDeclaration_17());
                    			

                    }


                    }
                    break;
                case 19 :
                    // InternalMethodName.g:1483:3: (enumLiteral_18= 'RB' )
                    {
                    // InternalMethodName.g:1483:3: (enumLiteral_18= 'RB' )
                    // InternalMethodName.g:1484:4: enumLiteral_18= 'RB'
                    {
                    enumLiteral_18=(Token)match(input,67,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_RBEnumLiteralDeclaration_18().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_18, grammarAccess.getPOSAccess().getPOS_RBEnumLiteralDeclaration_18());
                    			

                    }


                    }
                    break;
                case 20 :
                    // InternalMethodName.g:1491:3: (enumLiteral_19= 'RBR' )
                    {
                    // InternalMethodName.g:1491:3: (enumLiteral_19= 'RBR' )
                    // InternalMethodName.g:1492:4: enumLiteral_19= 'RBR'
                    {
                    enumLiteral_19=(Token)match(input,68,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_RBREnumLiteralDeclaration_19().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_19, grammarAccess.getPOSAccess().getPOS_RBREnumLiteralDeclaration_19());
                    			

                    }


                    }
                    break;
                case 21 :
                    // InternalMethodName.g:1499:3: (enumLiteral_20= 'RBS' )
                    {
                    // InternalMethodName.g:1499:3: (enumLiteral_20= 'RBS' )
                    // InternalMethodName.g:1500:4: enumLiteral_20= 'RBS'
                    {
                    enumLiteral_20=(Token)match(input,69,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_RBSEnumLiteralDeclaration_20().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_20, grammarAccess.getPOSAccess().getPOS_RBSEnumLiteralDeclaration_20());
                    			

                    }


                    }
                    break;
                case 22 :
                    // InternalMethodName.g:1507:3: (enumLiteral_21= 'RP' )
                    {
                    // InternalMethodName.g:1507:3: (enumLiteral_21= 'RP' )
                    // InternalMethodName.g:1508:4: enumLiteral_21= 'RP'
                    {
                    enumLiteral_21=(Token)match(input,70,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_RPEnumLiteralDeclaration_21().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_21, grammarAccess.getPOSAccess().getPOS_RPEnumLiteralDeclaration_21());
                    			

                    }


                    }
                    break;
                case 23 :
                    // InternalMethodName.g:1515:3: (enumLiteral_22= 'SYM' )
                    {
                    // InternalMethodName.g:1515:3: (enumLiteral_22= 'SYM' )
                    // InternalMethodName.g:1516:4: enumLiteral_22= 'SYM'
                    {
                    enumLiteral_22=(Token)match(input,71,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_SYMEnumLiteralDeclaration_22().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_22, grammarAccess.getPOSAccess().getPOS_SYMEnumLiteralDeclaration_22());
                    			

                    }


                    }
                    break;
                case 24 :
                    // InternalMethodName.g:1523:3: (enumLiteral_23= 'TO' )
                    {
                    // InternalMethodName.g:1523:3: (enumLiteral_23= 'TO' )
                    // InternalMethodName.g:1524:4: enumLiteral_23= 'TO'
                    {
                    enumLiteral_23=(Token)match(input,72,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_TOEnumLiteralDeclaration_23().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_23, grammarAccess.getPOSAccess().getPOS_TOEnumLiteralDeclaration_23());
                    			

                    }


                    }
                    break;
                case 25 :
                    // InternalMethodName.g:1531:3: (enumLiteral_24= 'UH' )
                    {
                    // InternalMethodName.g:1531:3: (enumLiteral_24= 'UH' )
                    // InternalMethodName.g:1532:4: enumLiteral_24= 'UH'
                    {
                    enumLiteral_24=(Token)match(input,73,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_UHEnumLiteralDeclaration_24().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_24, grammarAccess.getPOSAccess().getPOS_UHEnumLiteralDeclaration_24());
                    			

                    }


                    }
                    break;
                case 26 :
                    // InternalMethodName.g:1539:3: (enumLiteral_25= 'VB' )
                    {
                    // InternalMethodName.g:1539:3: (enumLiteral_25= 'VB' )
                    // InternalMethodName.g:1540:4: enumLiteral_25= 'VB'
                    {
                    enumLiteral_25=(Token)match(input,74,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_VBEnumLiteralDeclaration_25().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_25, grammarAccess.getPOSAccess().getPOS_VBEnumLiteralDeclaration_25());
                    			

                    }


                    }
                    break;
                case 27 :
                    // InternalMethodName.g:1547:3: (enumLiteral_26= 'VBD' )
                    {
                    // InternalMethodName.g:1547:3: (enumLiteral_26= 'VBD' )
                    // InternalMethodName.g:1548:4: enumLiteral_26= 'VBD'
                    {
                    enumLiteral_26=(Token)match(input,75,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_VBDEnumLiteralDeclaration_26().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_26, grammarAccess.getPOSAccess().getPOS_VBDEnumLiteralDeclaration_26());
                    			

                    }


                    }
                    break;
                case 28 :
                    // InternalMethodName.g:1555:3: (enumLiteral_27= 'VBG' )
                    {
                    // InternalMethodName.g:1555:3: (enumLiteral_27= 'VBG' )
                    // InternalMethodName.g:1556:4: enumLiteral_27= 'VBG'
                    {
                    enumLiteral_27=(Token)match(input,76,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_VBGEnumLiteralDeclaration_27().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_27, grammarAccess.getPOSAccess().getPOS_VBGEnumLiteralDeclaration_27());
                    			

                    }


                    }
                    break;
                case 29 :
                    // InternalMethodName.g:1563:3: (enumLiteral_28= 'VBN' )
                    {
                    // InternalMethodName.g:1563:3: (enumLiteral_28= 'VBN' )
                    // InternalMethodName.g:1564:4: enumLiteral_28= 'VBN'
                    {
                    enumLiteral_28=(Token)match(input,77,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_VBNEnumLiteralDeclaration_28().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_28, grammarAccess.getPOSAccess().getPOS_VBNEnumLiteralDeclaration_28());
                    			

                    }


                    }
                    break;
                case 30 :
                    // InternalMethodName.g:1571:3: (enumLiteral_29= 'VBP' )
                    {
                    // InternalMethodName.g:1571:3: (enumLiteral_29= 'VBP' )
                    // InternalMethodName.g:1572:4: enumLiteral_29= 'VBP'
                    {
                    enumLiteral_29=(Token)match(input,78,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_VBPEnumLiteralDeclaration_29().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_29, grammarAccess.getPOSAccess().getPOS_VBPEnumLiteralDeclaration_29());
                    			

                    }


                    }
                    break;
                case 31 :
                    // InternalMethodName.g:1579:3: (enumLiteral_30= 'VBZ' )
                    {
                    // InternalMethodName.g:1579:3: (enumLiteral_30= 'VBZ' )
                    // InternalMethodName.g:1580:4: enumLiteral_30= 'VBZ'
                    {
                    enumLiteral_30=(Token)match(input,79,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_VBZEnumLiteralDeclaration_30().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_30, grammarAccess.getPOSAccess().getPOS_VBZEnumLiteralDeclaration_30());
                    			

                    }


                    }
                    break;
                case 32 :
                    // InternalMethodName.g:1587:3: (enumLiteral_31= 'WDT' )
                    {
                    // InternalMethodName.g:1587:3: (enumLiteral_31= 'WDT' )
                    // InternalMethodName.g:1588:4: enumLiteral_31= 'WDT'
                    {
                    enumLiteral_31=(Token)match(input,80,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_WDTEnumLiteralDeclaration_31().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_31, grammarAccess.getPOSAccess().getPOS_WDTEnumLiteralDeclaration_31());
                    			

                    }


                    }
                    break;
                case 33 :
                    // InternalMethodName.g:1595:3: (enumLiteral_32= 'WP' )
                    {
                    // InternalMethodName.g:1595:3: (enumLiteral_32= 'WP' )
                    // InternalMethodName.g:1596:4: enumLiteral_32= 'WP'
                    {
                    enumLiteral_32=(Token)match(input,81,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_WPEnumLiteralDeclaration_32().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_32, grammarAccess.getPOSAccess().getPOS_WPEnumLiteralDeclaration_32());
                    			

                    }


                    }
                    break;
                case 34 :
                    // InternalMethodName.g:1603:3: (enumLiteral_33= 'WRB' )
                    {
                    // InternalMethodName.g:1603:3: (enumLiteral_33= 'WRB' )
                    // InternalMethodName.g:1604:4: enumLiteral_33= 'WRB'
                    {
                    enumLiteral_33=(Token)match(input,82,FOLLOW_2); 

                    				current = grammarAccess.getPOSAccess().getPOS_WRBEnumLiteralDeclaration_33().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_33, grammarAccess.getPOSAccess().getPOS_WRBEnumLiteralDeclaration_33());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePOS"


    // $ANTLR start "ruleCardinalityModifier"
    // InternalMethodName.g:1614:1: ruleCardinalityModifier returns [Enumerator current=null] : ( (enumLiteral_0= 'CARD_1' ) | (enumLiteral_1= '*' ) | (enumLiteral_2= '+' ) | (enumLiteral_3= '?' ) ) ;
    public final Enumerator ruleCardinalityModifier() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalMethodName.g:1620:2: ( ( (enumLiteral_0= 'CARD_1' ) | (enumLiteral_1= '*' ) | (enumLiteral_2= '+' ) | (enumLiteral_3= '?' ) ) )
            // InternalMethodName.g:1621:2: ( (enumLiteral_0= 'CARD_1' ) | (enumLiteral_1= '*' ) | (enumLiteral_2= '+' ) | (enumLiteral_3= '?' ) )
            {
            // InternalMethodName.g:1621:2: ( (enumLiteral_0= 'CARD_1' ) | (enumLiteral_1= '*' ) | (enumLiteral_2= '+' ) | (enumLiteral_3= '?' ) )
            int alt29=4;
            switch ( input.LA(1) ) {
            case 83:
                {
                alt29=1;
                }
                break;
            case 84:
                {
                alt29=2;
                }
                break;
            case 85:
                {
                alt29=3;
                }
                break;
            case 86:
                {
                alt29=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }

            switch (alt29) {
                case 1 :
                    // InternalMethodName.g:1622:3: (enumLiteral_0= 'CARD_1' )
                    {
                    // InternalMethodName.g:1622:3: (enumLiteral_0= 'CARD_1' )
                    // InternalMethodName.g:1623:4: enumLiteral_0= 'CARD_1'
                    {
                    enumLiteral_0=(Token)match(input,83,FOLLOW_2); 

                    				current = grammarAccess.getCardinalityModifierAccess().getCARD_1EnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getCardinalityModifierAccess().getCARD_1EnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMethodName.g:1630:3: (enumLiteral_1= '*' )
                    {
                    // InternalMethodName.g:1630:3: (enumLiteral_1= '*' )
                    // InternalMethodName.g:1631:4: enumLiteral_1= '*'
                    {
                    enumLiteral_1=(Token)match(input,84,FOLLOW_2); 

                    				current = grammarAccess.getCardinalityModifierAccess().getCARD_0_NEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getCardinalityModifierAccess().getCARD_0_NEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalMethodName.g:1638:3: (enumLiteral_2= '+' )
                    {
                    // InternalMethodName.g:1638:3: (enumLiteral_2= '+' )
                    // InternalMethodName.g:1639:4: enumLiteral_2= '+'
                    {
                    enumLiteral_2=(Token)match(input,85,FOLLOW_2); 

                    				current = grammarAccess.getCardinalityModifierAccess().getCARD_1_NEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getCardinalityModifierAccess().getCARD_1_NEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalMethodName.g:1646:3: (enumLiteral_3= '?' )
                    {
                    // InternalMethodName.g:1646:3: (enumLiteral_3= '?' )
                    // InternalMethodName.g:1647:4: enumLiteral_3= '?'
                    {
                    enumLiteral_3=(Token)match(input,86,FOLLOW_2); 

                    				current = grammarAccess.getCardinalityModifierAccess().getCARD_0_1EnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getCardinalityModifierAccess().getCARD_0_1EnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCardinalityModifier"


    // $ANTLR start "ruleFrequency"
    // InternalMethodName.g:1657:1: ruleFrequency returns [Enumerator current=null] : ( (enumLiteral_0= 'always' ) | (enumLiteral_1= 'very-often' ) | (enumLiteral_2= 'often' ) | (enumLiteral_3= 'rarely' ) | (enumLiteral_4= 'very-seldom' ) | (enumLiteral_5= 'never' ) ) ;
    public final Enumerator ruleFrequency() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;


        	enterRule();

        try {
            // InternalMethodName.g:1663:2: ( ( (enumLiteral_0= 'always' ) | (enumLiteral_1= 'very-often' ) | (enumLiteral_2= 'often' ) | (enumLiteral_3= 'rarely' ) | (enumLiteral_4= 'very-seldom' ) | (enumLiteral_5= 'never' ) ) )
            // InternalMethodName.g:1664:2: ( (enumLiteral_0= 'always' ) | (enumLiteral_1= 'very-often' ) | (enumLiteral_2= 'often' ) | (enumLiteral_3= 'rarely' ) | (enumLiteral_4= 'very-seldom' ) | (enumLiteral_5= 'never' ) )
            {
            // InternalMethodName.g:1664:2: ( (enumLiteral_0= 'always' ) | (enumLiteral_1= 'very-often' ) | (enumLiteral_2= 'often' ) | (enumLiteral_3= 'rarely' ) | (enumLiteral_4= 'very-seldom' ) | (enumLiteral_5= 'never' ) )
            int alt30=6;
            switch ( input.LA(1) ) {
            case 87:
                {
                alt30=1;
                }
                break;
            case 88:
                {
                alt30=2;
                }
                break;
            case 89:
                {
                alt30=3;
                }
                break;
            case 90:
                {
                alt30=4;
                }
                break;
            case 91:
                {
                alt30=5;
                }
                break;
            case 92:
                {
                alt30=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }

            switch (alt30) {
                case 1 :
                    // InternalMethodName.g:1665:3: (enumLiteral_0= 'always' )
                    {
                    // InternalMethodName.g:1665:3: (enumLiteral_0= 'always' )
                    // InternalMethodName.g:1666:4: enumLiteral_0= 'always'
                    {
                    enumLiteral_0=(Token)match(input,87,FOLLOW_2); 

                    				current = grammarAccess.getFrequencyAccess().getALWAYSEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getFrequencyAccess().getALWAYSEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMethodName.g:1673:3: (enumLiteral_1= 'very-often' )
                    {
                    // InternalMethodName.g:1673:3: (enumLiteral_1= 'very-often' )
                    // InternalMethodName.g:1674:4: enumLiteral_1= 'very-often'
                    {
                    enumLiteral_1=(Token)match(input,88,FOLLOW_2); 

                    				current = grammarAccess.getFrequencyAccess().getVERYOFTENEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getFrequencyAccess().getVERYOFTENEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalMethodName.g:1681:3: (enumLiteral_2= 'often' )
                    {
                    // InternalMethodName.g:1681:3: (enumLiteral_2= 'often' )
                    // InternalMethodName.g:1682:4: enumLiteral_2= 'often'
                    {
                    enumLiteral_2=(Token)match(input,89,FOLLOW_2); 

                    				current = grammarAccess.getFrequencyAccess().getOFTEEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getFrequencyAccess().getOFTEEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalMethodName.g:1689:3: (enumLiteral_3= 'rarely' )
                    {
                    // InternalMethodName.g:1689:3: (enumLiteral_3= 'rarely' )
                    // InternalMethodName.g:1690:4: enumLiteral_3= 'rarely'
                    {
                    enumLiteral_3=(Token)match(input,90,FOLLOW_2); 

                    				current = grammarAccess.getFrequencyAccess().getRARELYEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getFrequencyAccess().getRARELYEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalMethodName.g:1697:3: (enumLiteral_4= 'very-seldom' )
                    {
                    // InternalMethodName.g:1697:3: (enumLiteral_4= 'very-seldom' )
                    // InternalMethodName.g:1698:4: enumLiteral_4= 'very-seldom'
                    {
                    enumLiteral_4=(Token)match(input,91,FOLLOW_2); 

                    				current = grammarAccess.getFrequencyAccess().getVERYSELDOMEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_4, grammarAccess.getFrequencyAccess().getVERYSELDOMEnumLiteralDeclaration_4());
                    			

                    }


                    }
                    break;
                case 6 :
                    // InternalMethodName.g:1705:3: (enumLiteral_5= 'never' )
                    {
                    // InternalMethodName.g:1705:3: (enumLiteral_5= 'never' )
                    // InternalMethodName.g:1706:4: enumLiteral_5= 'never'
                    {
                    enumLiteral_5=(Token)match(input,92,FOLLOW_2); 

                    				current = grammarAccess.getFrequencyAccess().getNEVEREnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_5, grammarAccess.getFrequencyAccess().getNEVEREnumLiteralDeclaration_5());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFrequency"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x00001F0000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x00001F0000001010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000000000000A000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000180000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x000000000F800000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0xFFFE008C00000020L,0x000000000007FFFFL});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000010000010L,0x000000001F800000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000080000002L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000300000002L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000000000002L,0x0000000000780000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000002000000002L,0x0000000000780000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0001E00000000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0xFFFE000400000000L,0x000000000007FFFFL});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000008000000000L});

}