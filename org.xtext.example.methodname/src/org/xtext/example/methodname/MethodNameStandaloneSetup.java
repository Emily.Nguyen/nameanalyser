/*
 * generated by Xtext 2.25.0
 */
package org.xtext.example.methodname;


/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
public class MethodNameStandaloneSetup extends MethodNameStandaloneSetupGenerated {

	public static void doSetup() {
		new MethodNameStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}
