grammar org.xtext.example.methodname.MethodName with org.eclipse.xtext.common.Terminals

generate methodName "http://www.xtext.org/example/methodname/MethodName"

Model:
    "declarations"
        declarations+=Declaration*
        fixedDeclarations+=FixedDeclaration+
    "rules"
        rules+=Rule*
    "cases"
        elements+=Case*
; 

Declaration:
    name=ID "<->" path=STRING
;

FixedDeclaration:
    kind=Kind "<->" path=STRING
;

enum Kind:
    KIND_INTERFACE="interface"|
    KIND_PACKAGE="package" |
    KIND_CLASS="class" |
    KIND_METHOD="method" |
    KIND_VAR="variable"
;

Rule:
    "def" name=ID "for" kind=Kind "{"
        "filter" decls+=[Declaration] ("||" decls+=[Declaration])*
        "such" "that" "size" compOp=(">"|"<"|"=="|"<="|">=") compValue=INT
    "}" |
    "def" "declare" name=ID
;
    
Case:
    "case" "for" kind=Kind condition=Expr "{"
        properties+=Property*
    "}"
;
    
Expr: Or;    
    
Or returns Expr: 
    And ({Or.left=current} "|" right=And)*;

And returns Expr: 
    Primary ({And.left=current} concatKind=("."|"...") right=Primary)*;
        
Primary returns Expr:
    GroupExpr |
    Atomic
;
    
GroupExpr:
    hasNot?="!"? "(" expr=Expr ")" card=CardinalityModifier?
;

Atomic returns Expr:
    {StringConst}
        hasNot?="!"? value=STRING card=CardinalityModifier? ("[" sentiment=Sentiment "]")? |
    {POSValue}
        hasNot?="!"? pos=POS card=CardinalityModifier? ("[" sentiment=Sentiment "]")? |
    {SynonymConst}
    	hasNot?="!"? "#" value=STRING card=CardinalityModifier? ("[" sentiment=Sentiment "]")?
;    

enum Sentiment:
	SENT_UNSPECIFIED |
    SENT_NEUTRAL="neutral" |
    SENT_POSITIVE="positive" |
    SENT_NEGATIVE="negative"
;

enum POS: 
   	POS_CC="CC" | 
    POS_CD="CD" | 
    POS_DT="DT" |
    POS_EX="EX" |
    POS_FW="FW" |
    POS_IN="IN" |
    POS_JJ="JJ" |
    POS_JJR="JJR" |
    POS_JJS="JJS" |
    POS_LS="LS" | 
    POS_MD="MD" |
    POS_NN="NN" |
    POS_NNS="NNS" |
    POS_NNP="NNP" |
    POS_NNPS="NNPS" |
    POS_PDT="PDT" |
    POS_POS="POS" |
    POS_PRP="PRP" | 
    POS_RB="RB" |
    POS_RBR="RBR" |
    POS_RBS="RBS" |
    POS_RP="RP" |
    POS_SYM="SYM" |
    POS_TO="TO" |
    POS_UH="UH" |
    POS_VB="VB" |
    POS_VBD="VBD" | 
    POS_VBG="VBG" |
    POS_VBN="VBN" |
    POS_VBP="VBP" |
    POS_VBZ="VBZ" |
    POS_WDT="WDT" |
    POS_WP="WP" |
    POS_WRB="WRB"
;
    
enum CardinalityModifier:
    CARD_1 |
    CARD_0_N="*" | 
    CARD_1_N="+" | 
    CARD_0_1="?" 
;
    
Property: 
    frequency=Frequency? criterion=[Rule];

enum Frequency: 
    ALWAYS="always" | 
    VERYOFTEN="very-often" | 
    OFTE="often" | 
    RARELY="rarely" | 
    VERYSELDOM="very-seldom" | 
    NEVER="never"
;
