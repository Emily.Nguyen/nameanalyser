package org.xtext.example.methodname.generator;

import net.sf.extjwnl.JWNLException;
import net.sf.extjwnl.data.IndexWord;
import net.sf.extjwnl.data.POS;
import net.sf.extjwnl.data.Synset;
import net.sf.extjwnl.dictionary.Dictionary;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Synonym {
	
	public static String generateAllSynonyms(String word, String sentiment) throws JWNLException, CloneNotSupportedException {
	    Dictionary dictionary = Dictionary.getDefaultResourceInstance();
	    Set<String> allSynonyms =  new HashSet<>();
	    String synonymsAndSentiment = "";
	    if (null != dictionary) {
	        allSynonyms.addAll(findSynonym(dictionary.lookupIndexWord(POS.VERB, word)));
	        allSynonyms.addAll(findSynonym(dictionary.lookupIndexWord(POS.NOUN, word)));
	        allSynonyms.addAll(findSynonym(dictionary.lookupIndexWord(POS.ADJECTIVE, word)));
	        allSynonyms.addAll(findSynonym(dictionary.lookupIndexWord(POS.ADVERB, word)));
	        allSynonyms.add(word);
	    }
	    
	    if (sentiment.equals("unspecified")) {
	    	Set<String> synonymWithUnspecifiedSentiment =  new HashSet<>();
	    	for (String synonym : allSynonyms) {
	    		synonymWithUnspecifiedSentiment.add(synonym + "@positive | " + synonym + "@negative | " + synonym + "@neutral");
	    	}
	    	synonymsAndSentiment = String.join(" | ", synonymWithUnspecifiedSentiment);
	    } else {
	    	synonymsAndSentiment = String.join("@" + sentiment + " | ", allSynonyms) + "@" + sentiment;
	    }
	    return synonymsAndSentiment; 
	    
	}

    private static Set<String> findSynonym(IndexWord word) throws JWNLException, CloneNotSupportedException {
        Set<String> resultList = new HashSet<>(); 
        List<String> subList = new ArrayList<>();
        List<Synset> wordSynsetList = new ArrayList<>();
        try {
            wordSynsetList = word.getSenses();
        } catch (Exception e){
            System.out.println("No synonym for this word as this POS type");
        } finally {
            if (!wordSynsetList.isEmpty()) {
                for (Synset wordSynset : wordSynsetList) {
                    subList = wordSynset.getWords().stream()
                                                .map(m -> m.getLemma())
                                                .collect(Collectors.toList());
                    resultList.addAll(subList);
                }
            }
        }
        return resultList;
    }
}
