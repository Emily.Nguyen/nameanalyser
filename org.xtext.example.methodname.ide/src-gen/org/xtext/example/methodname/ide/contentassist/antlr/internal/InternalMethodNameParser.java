package org.xtext.example.methodname.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.methodname.services.MethodNameGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMethodNameParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'>'", "'<'", "'=='", "'<='", "'>='", "'.'", "'...'", "'interface'", "'package'", "'class'", "'method'", "'variable'", "'SENT_UNSPECIFIED'", "'neutral'", "'positive'", "'negative'", "'CC'", "'CD'", "'DT'", "'EX'", "'FW'", "'IN'", "'JJ'", "'JJR'", "'JJS'", "'LS'", "'MD'", "'NN'", "'NNS'", "'NNP'", "'NNPS'", "'PDT'", "'POS'", "'PRP'", "'RB'", "'RBR'", "'RBS'", "'RP'", "'SYM'", "'TO'", "'UH'", "'VB'", "'VBD'", "'VBG'", "'VBN'", "'VBP'", "'VBZ'", "'WDT'", "'WP'", "'WRB'", "'CARD_1'", "'*'", "'+'", "'?'", "'always'", "'very-often'", "'often'", "'rarely'", "'very-seldom'", "'never'", "'declarations'", "'rules'", "'cases'", "'<->'", "'def'", "'for'", "'{'", "'filter'", "'such'", "'that'", "'size'", "'}'", "'||'", "'declare'", "'case'", "'|'", "'('", "')'", "'['", "']'", "'#'", "'!'"
    };
    public static final int T__50=50;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=4;
    public static final int RULE_INT=6;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__67=67;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__91=91;
    public static final int T__92=92;
    public static final int T__90=90;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__74=74;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__88=88;
    public static final int T__89=89;
    public static final int T__84=84;
    public static final int T__85=85;
    public static final int T__86=86;
    public static final int T__87=87;

    // delegates
    // delegators


        public InternalMethodNameParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMethodNameParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMethodNameParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMethodName.g"; }


    	private MethodNameGrammarAccess grammarAccess;

    	public void setGrammarAccess(MethodNameGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleModel"
    // InternalMethodName.g:53:1: entryRuleModel : ruleModel EOF ;
    public final void entryRuleModel() throws RecognitionException {
        try {
            // InternalMethodName.g:54:1: ( ruleModel EOF )
            // InternalMethodName.g:55:1: ruleModel EOF
            {
             before(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            ruleModel();

            state._fsp--;

             after(grammarAccess.getModelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalMethodName.g:62:1: ruleModel : ( ( rule__Model__Group__0 ) ) ;
    public final void ruleModel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:66:2: ( ( ( rule__Model__Group__0 ) ) )
            // InternalMethodName.g:67:2: ( ( rule__Model__Group__0 ) )
            {
            // InternalMethodName.g:67:2: ( ( rule__Model__Group__0 ) )
            // InternalMethodName.g:68:3: ( rule__Model__Group__0 )
            {
             before(grammarAccess.getModelAccess().getGroup()); 
            // InternalMethodName.g:69:3: ( rule__Model__Group__0 )
            // InternalMethodName.g:69:4: rule__Model__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Model__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleDeclaration"
    // InternalMethodName.g:78:1: entryRuleDeclaration : ruleDeclaration EOF ;
    public final void entryRuleDeclaration() throws RecognitionException {
        try {
            // InternalMethodName.g:79:1: ( ruleDeclaration EOF )
            // InternalMethodName.g:80:1: ruleDeclaration EOF
            {
             before(grammarAccess.getDeclarationRule()); 
            pushFollow(FOLLOW_1);
            ruleDeclaration();

            state._fsp--;

             after(grammarAccess.getDeclarationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDeclaration"


    // $ANTLR start "ruleDeclaration"
    // InternalMethodName.g:87:1: ruleDeclaration : ( ( rule__Declaration__Group__0 ) ) ;
    public final void ruleDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:91:2: ( ( ( rule__Declaration__Group__0 ) ) )
            // InternalMethodName.g:92:2: ( ( rule__Declaration__Group__0 ) )
            {
            // InternalMethodName.g:92:2: ( ( rule__Declaration__Group__0 ) )
            // InternalMethodName.g:93:3: ( rule__Declaration__Group__0 )
            {
             before(grammarAccess.getDeclarationAccess().getGroup()); 
            // InternalMethodName.g:94:3: ( rule__Declaration__Group__0 )
            // InternalMethodName.g:94:4: rule__Declaration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Declaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDeclarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDeclaration"


    // $ANTLR start "entryRuleFixedDeclaration"
    // InternalMethodName.g:103:1: entryRuleFixedDeclaration : ruleFixedDeclaration EOF ;
    public final void entryRuleFixedDeclaration() throws RecognitionException {
        try {
            // InternalMethodName.g:104:1: ( ruleFixedDeclaration EOF )
            // InternalMethodName.g:105:1: ruleFixedDeclaration EOF
            {
             before(grammarAccess.getFixedDeclarationRule()); 
            pushFollow(FOLLOW_1);
            ruleFixedDeclaration();

            state._fsp--;

             after(grammarAccess.getFixedDeclarationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFixedDeclaration"


    // $ANTLR start "ruleFixedDeclaration"
    // InternalMethodName.g:112:1: ruleFixedDeclaration : ( ( rule__FixedDeclaration__Group__0 ) ) ;
    public final void ruleFixedDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:116:2: ( ( ( rule__FixedDeclaration__Group__0 ) ) )
            // InternalMethodName.g:117:2: ( ( rule__FixedDeclaration__Group__0 ) )
            {
            // InternalMethodName.g:117:2: ( ( rule__FixedDeclaration__Group__0 ) )
            // InternalMethodName.g:118:3: ( rule__FixedDeclaration__Group__0 )
            {
             before(grammarAccess.getFixedDeclarationAccess().getGroup()); 
            // InternalMethodName.g:119:3: ( rule__FixedDeclaration__Group__0 )
            // InternalMethodName.g:119:4: rule__FixedDeclaration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__FixedDeclaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFixedDeclarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFixedDeclaration"


    // $ANTLR start "entryRuleRule"
    // InternalMethodName.g:128:1: entryRuleRule : ruleRule EOF ;
    public final void entryRuleRule() throws RecognitionException {
        try {
            // InternalMethodName.g:129:1: ( ruleRule EOF )
            // InternalMethodName.g:130:1: ruleRule EOF
            {
             before(grammarAccess.getRuleRule()); 
            pushFollow(FOLLOW_1);
            ruleRule();

            state._fsp--;

             after(grammarAccess.getRuleRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRule"


    // $ANTLR start "ruleRule"
    // InternalMethodName.g:137:1: ruleRule : ( ( rule__Rule__Alternatives ) ) ;
    public final void ruleRule() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:141:2: ( ( ( rule__Rule__Alternatives ) ) )
            // InternalMethodName.g:142:2: ( ( rule__Rule__Alternatives ) )
            {
            // InternalMethodName.g:142:2: ( ( rule__Rule__Alternatives ) )
            // InternalMethodName.g:143:3: ( rule__Rule__Alternatives )
            {
             before(grammarAccess.getRuleAccess().getAlternatives()); 
            // InternalMethodName.g:144:3: ( rule__Rule__Alternatives )
            // InternalMethodName.g:144:4: rule__Rule__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Rule__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getRuleAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRule"


    // $ANTLR start "entryRuleCase"
    // InternalMethodName.g:153:1: entryRuleCase : ruleCase EOF ;
    public final void entryRuleCase() throws RecognitionException {
        try {
            // InternalMethodName.g:154:1: ( ruleCase EOF )
            // InternalMethodName.g:155:1: ruleCase EOF
            {
             before(grammarAccess.getCaseRule()); 
            pushFollow(FOLLOW_1);
            ruleCase();

            state._fsp--;

             after(grammarAccess.getCaseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCase"


    // $ANTLR start "ruleCase"
    // InternalMethodName.g:162:1: ruleCase : ( ( rule__Case__Group__0 ) ) ;
    public final void ruleCase() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:166:2: ( ( ( rule__Case__Group__0 ) ) )
            // InternalMethodName.g:167:2: ( ( rule__Case__Group__0 ) )
            {
            // InternalMethodName.g:167:2: ( ( rule__Case__Group__0 ) )
            // InternalMethodName.g:168:3: ( rule__Case__Group__0 )
            {
             before(grammarAccess.getCaseAccess().getGroup()); 
            // InternalMethodName.g:169:3: ( rule__Case__Group__0 )
            // InternalMethodName.g:169:4: rule__Case__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Case__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCaseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCase"


    // $ANTLR start "entryRuleExpr"
    // InternalMethodName.g:178:1: entryRuleExpr : ruleExpr EOF ;
    public final void entryRuleExpr() throws RecognitionException {
        try {
            // InternalMethodName.g:179:1: ( ruleExpr EOF )
            // InternalMethodName.g:180:1: ruleExpr EOF
            {
             before(grammarAccess.getExprRule()); 
            pushFollow(FOLLOW_1);
            ruleExpr();

            state._fsp--;

             after(grammarAccess.getExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpr"


    // $ANTLR start "ruleExpr"
    // InternalMethodName.g:187:1: ruleExpr : ( ruleOr ) ;
    public final void ruleExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:191:2: ( ( ruleOr ) )
            // InternalMethodName.g:192:2: ( ruleOr )
            {
            // InternalMethodName.g:192:2: ( ruleOr )
            // InternalMethodName.g:193:3: ruleOr
            {
             before(grammarAccess.getExprAccess().getOrParserRuleCall()); 
            pushFollow(FOLLOW_2);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getExprAccess().getOrParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpr"


    // $ANTLR start "entryRuleOr"
    // InternalMethodName.g:203:1: entryRuleOr : ruleOr EOF ;
    public final void entryRuleOr() throws RecognitionException {
        try {
            // InternalMethodName.g:204:1: ( ruleOr EOF )
            // InternalMethodName.g:205:1: ruleOr EOF
            {
             before(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_1);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getOrRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalMethodName.g:212:1: ruleOr : ( ( rule__Or__Group__0 ) ) ;
    public final void ruleOr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:216:2: ( ( ( rule__Or__Group__0 ) ) )
            // InternalMethodName.g:217:2: ( ( rule__Or__Group__0 ) )
            {
            // InternalMethodName.g:217:2: ( ( rule__Or__Group__0 ) )
            // InternalMethodName.g:218:3: ( rule__Or__Group__0 )
            {
             before(grammarAccess.getOrAccess().getGroup()); 
            // InternalMethodName.g:219:3: ( rule__Or__Group__0 )
            // InternalMethodName.g:219:4: rule__Or__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleAnd"
    // InternalMethodName.g:228:1: entryRuleAnd : ruleAnd EOF ;
    public final void entryRuleAnd() throws RecognitionException {
        try {
            // InternalMethodName.g:229:1: ( ruleAnd EOF )
            // InternalMethodName.g:230:1: ruleAnd EOF
            {
             before(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_1);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getAndRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalMethodName.g:237:1: ruleAnd : ( ( rule__And__Group__0 ) ) ;
    public final void ruleAnd() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:241:2: ( ( ( rule__And__Group__0 ) ) )
            // InternalMethodName.g:242:2: ( ( rule__And__Group__0 ) )
            {
            // InternalMethodName.g:242:2: ( ( rule__And__Group__0 ) )
            // InternalMethodName.g:243:3: ( rule__And__Group__0 )
            {
             before(grammarAccess.getAndAccess().getGroup()); 
            // InternalMethodName.g:244:3: ( rule__And__Group__0 )
            // InternalMethodName.g:244:4: rule__And__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__And__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRulePrimary"
    // InternalMethodName.g:253:1: entryRulePrimary : rulePrimary EOF ;
    public final void entryRulePrimary() throws RecognitionException {
        try {
            // InternalMethodName.g:254:1: ( rulePrimary EOF )
            // InternalMethodName.g:255:1: rulePrimary EOF
            {
             before(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalMethodName.g:262:1: rulePrimary : ( ( rule__Primary__Alternatives ) ) ;
    public final void rulePrimary() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:266:2: ( ( ( rule__Primary__Alternatives ) ) )
            // InternalMethodName.g:267:2: ( ( rule__Primary__Alternatives ) )
            {
            // InternalMethodName.g:267:2: ( ( rule__Primary__Alternatives ) )
            // InternalMethodName.g:268:3: ( rule__Primary__Alternatives )
            {
             before(grammarAccess.getPrimaryAccess().getAlternatives()); 
            // InternalMethodName.g:269:3: ( rule__Primary__Alternatives )
            // InternalMethodName.g:269:4: rule__Primary__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleGroupExpr"
    // InternalMethodName.g:278:1: entryRuleGroupExpr : ruleGroupExpr EOF ;
    public final void entryRuleGroupExpr() throws RecognitionException {
        try {
            // InternalMethodName.g:279:1: ( ruleGroupExpr EOF )
            // InternalMethodName.g:280:1: ruleGroupExpr EOF
            {
             before(grammarAccess.getGroupExprRule()); 
            pushFollow(FOLLOW_1);
            ruleGroupExpr();

            state._fsp--;

             after(grammarAccess.getGroupExprRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGroupExpr"


    // $ANTLR start "ruleGroupExpr"
    // InternalMethodName.g:287:1: ruleGroupExpr : ( ( rule__GroupExpr__Group__0 ) ) ;
    public final void ruleGroupExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:291:2: ( ( ( rule__GroupExpr__Group__0 ) ) )
            // InternalMethodName.g:292:2: ( ( rule__GroupExpr__Group__0 ) )
            {
            // InternalMethodName.g:292:2: ( ( rule__GroupExpr__Group__0 ) )
            // InternalMethodName.g:293:3: ( rule__GroupExpr__Group__0 )
            {
             before(grammarAccess.getGroupExprAccess().getGroup()); 
            // InternalMethodName.g:294:3: ( rule__GroupExpr__Group__0 )
            // InternalMethodName.g:294:4: rule__GroupExpr__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GroupExpr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getGroupExprAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGroupExpr"


    // $ANTLR start "entryRuleAtomic"
    // InternalMethodName.g:303:1: entryRuleAtomic : ruleAtomic EOF ;
    public final void entryRuleAtomic() throws RecognitionException {
        try {
            // InternalMethodName.g:304:1: ( ruleAtomic EOF )
            // InternalMethodName.g:305:1: ruleAtomic EOF
            {
             before(grammarAccess.getAtomicRule()); 
            pushFollow(FOLLOW_1);
            ruleAtomic();

            state._fsp--;

             after(grammarAccess.getAtomicRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAtomic"


    // $ANTLR start "ruleAtomic"
    // InternalMethodName.g:312:1: ruleAtomic : ( ( rule__Atomic__Alternatives ) ) ;
    public final void ruleAtomic() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:316:2: ( ( ( rule__Atomic__Alternatives ) ) )
            // InternalMethodName.g:317:2: ( ( rule__Atomic__Alternatives ) )
            {
            // InternalMethodName.g:317:2: ( ( rule__Atomic__Alternatives ) )
            // InternalMethodName.g:318:3: ( rule__Atomic__Alternatives )
            {
             before(grammarAccess.getAtomicAccess().getAlternatives()); 
            // InternalMethodName.g:319:3: ( rule__Atomic__Alternatives )
            // InternalMethodName.g:319:4: rule__Atomic__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAtomic"


    // $ANTLR start "entryRuleProperty"
    // InternalMethodName.g:328:1: entryRuleProperty : ruleProperty EOF ;
    public final void entryRuleProperty() throws RecognitionException {
        try {
            // InternalMethodName.g:329:1: ( ruleProperty EOF )
            // InternalMethodName.g:330:1: ruleProperty EOF
            {
             before(grammarAccess.getPropertyRule()); 
            pushFollow(FOLLOW_1);
            ruleProperty();

            state._fsp--;

             after(grammarAccess.getPropertyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProperty"


    // $ANTLR start "ruleProperty"
    // InternalMethodName.g:337:1: ruleProperty : ( ( rule__Property__Group__0 ) ) ;
    public final void ruleProperty() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:341:2: ( ( ( rule__Property__Group__0 ) ) )
            // InternalMethodName.g:342:2: ( ( rule__Property__Group__0 ) )
            {
            // InternalMethodName.g:342:2: ( ( rule__Property__Group__0 ) )
            // InternalMethodName.g:343:3: ( rule__Property__Group__0 )
            {
             before(grammarAccess.getPropertyAccess().getGroup()); 
            // InternalMethodName.g:344:3: ( rule__Property__Group__0 )
            // InternalMethodName.g:344:4: rule__Property__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Property__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPropertyAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProperty"


    // $ANTLR start "ruleKind"
    // InternalMethodName.g:353:1: ruleKind : ( ( rule__Kind__Alternatives ) ) ;
    public final void ruleKind() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:357:1: ( ( ( rule__Kind__Alternatives ) ) )
            // InternalMethodName.g:358:2: ( ( rule__Kind__Alternatives ) )
            {
            // InternalMethodName.g:358:2: ( ( rule__Kind__Alternatives ) )
            // InternalMethodName.g:359:3: ( rule__Kind__Alternatives )
            {
             before(grammarAccess.getKindAccess().getAlternatives()); 
            // InternalMethodName.g:360:3: ( rule__Kind__Alternatives )
            // InternalMethodName.g:360:4: rule__Kind__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Kind__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getKindAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleKind"


    // $ANTLR start "ruleSentiment"
    // InternalMethodName.g:369:1: ruleSentiment : ( ( rule__Sentiment__Alternatives ) ) ;
    public final void ruleSentiment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:373:1: ( ( ( rule__Sentiment__Alternatives ) ) )
            // InternalMethodName.g:374:2: ( ( rule__Sentiment__Alternatives ) )
            {
            // InternalMethodName.g:374:2: ( ( rule__Sentiment__Alternatives ) )
            // InternalMethodName.g:375:3: ( rule__Sentiment__Alternatives )
            {
             before(grammarAccess.getSentimentAccess().getAlternatives()); 
            // InternalMethodName.g:376:3: ( rule__Sentiment__Alternatives )
            // InternalMethodName.g:376:4: rule__Sentiment__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Sentiment__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSentimentAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSentiment"


    // $ANTLR start "rulePOS"
    // InternalMethodName.g:385:1: rulePOS : ( ( rule__POS__Alternatives ) ) ;
    public final void rulePOS() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:389:1: ( ( ( rule__POS__Alternatives ) ) )
            // InternalMethodName.g:390:2: ( ( rule__POS__Alternatives ) )
            {
            // InternalMethodName.g:390:2: ( ( rule__POS__Alternatives ) )
            // InternalMethodName.g:391:3: ( rule__POS__Alternatives )
            {
             before(grammarAccess.getPOSAccess().getAlternatives()); 
            // InternalMethodName.g:392:3: ( rule__POS__Alternatives )
            // InternalMethodName.g:392:4: rule__POS__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__POS__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPOSAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePOS"


    // $ANTLR start "ruleCardinalityModifier"
    // InternalMethodName.g:401:1: ruleCardinalityModifier : ( ( rule__CardinalityModifier__Alternatives ) ) ;
    public final void ruleCardinalityModifier() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:405:1: ( ( ( rule__CardinalityModifier__Alternatives ) ) )
            // InternalMethodName.g:406:2: ( ( rule__CardinalityModifier__Alternatives ) )
            {
            // InternalMethodName.g:406:2: ( ( rule__CardinalityModifier__Alternatives ) )
            // InternalMethodName.g:407:3: ( rule__CardinalityModifier__Alternatives )
            {
             before(grammarAccess.getCardinalityModifierAccess().getAlternatives()); 
            // InternalMethodName.g:408:3: ( rule__CardinalityModifier__Alternatives )
            // InternalMethodName.g:408:4: rule__CardinalityModifier__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__CardinalityModifier__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getCardinalityModifierAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCardinalityModifier"


    // $ANTLR start "ruleFrequency"
    // InternalMethodName.g:417:1: ruleFrequency : ( ( rule__Frequency__Alternatives ) ) ;
    public final void ruleFrequency() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:421:1: ( ( ( rule__Frequency__Alternatives ) ) )
            // InternalMethodName.g:422:2: ( ( rule__Frequency__Alternatives ) )
            {
            // InternalMethodName.g:422:2: ( ( rule__Frequency__Alternatives ) )
            // InternalMethodName.g:423:3: ( rule__Frequency__Alternatives )
            {
             before(grammarAccess.getFrequencyAccess().getAlternatives()); 
            // InternalMethodName.g:424:3: ( rule__Frequency__Alternatives )
            // InternalMethodName.g:424:4: rule__Frequency__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Frequency__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getFrequencyAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFrequency"


    // $ANTLR start "rule__Rule__Alternatives"
    // InternalMethodName.g:432:1: rule__Rule__Alternatives : ( ( ( rule__Rule__Group_0__0 ) ) | ( ( rule__Rule__Group_1__0 ) ) );
    public final void rule__Rule__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:436:1: ( ( ( rule__Rule__Group_0__0 ) ) | ( ( rule__Rule__Group_1__0 ) ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==75) ) {
                int LA1_1 = input.LA(2);

                if ( (LA1_1==RULE_ID) ) {
                    alt1=1;
                }
                else if ( (LA1_1==84) ) {
                    alt1=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalMethodName.g:437:2: ( ( rule__Rule__Group_0__0 ) )
                    {
                    // InternalMethodName.g:437:2: ( ( rule__Rule__Group_0__0 ) )
                    // InternalMethodName.g:438:3: ( rule__Rule__Group_0__0 )
                    {
                     before(grammarAccess.getRuleAccess().getGroup_0()); 
                    // InternalMethodName.g:439:3: ( rule__Rule__Group_0__0 )
                    // InternalMethodName.g:439:4: rule__Rule__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Rule__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getRuleAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMethodName.g:443:2: ( ( rule__Rule__Group_1__0 ) )
                    {
                    // InternalMethodName.g:443:2: ( ( rule__Rule__Group_1__0 ) )
                    // InternalMethodName.g:444:3: ( rule__Rule__Group_1__0 )
                    {
                     before(grammarAccess.getRuleAccess().getGroup_1()); 
                    // InternalMethodName.g:445:3: ( rule__Rule__Group_1__0 )
                    // InternalMethodName.g:445:4: rule__Rule__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Rule__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getRuleAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Alternatives"


    // $ANTLR start "rule__Rule__CompOpAlternatives_0_11_0"
    // InternalMethodName.g:453:1: rule__Rule__CompOpAlternatives_0_11_0 : ( ( '>' ) | ( '<' ) | ( '==' ) | ( '<=' ) | ( '>=' ) );
    public final void rule__Rule__CompOpAlternatives_0_11_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:457:1: ( ( '>' ) | ( '<' ) | ( '==' ) | ( '<=' ) | ( '>=' ) )
            int alt2=5;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt2=1;
                }
                break;
            case 12:
                {
                alt2=2;
                }
                break;
            case 13:
                {
                alt2=3;
                }
                break;
            case 14:
                {
                alt2=4;
                }
                break;
            case 15:
                {
                alt2=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalMethodName.g:458:2: ( '>' )
                    {
                    // InternalMethodName.g:458:2: ( '>' )
                    // InternalMethodName.g:459:3: '>'
                    {
                     before(grammarAccess.getRuleAccess().getCompOpGreaterThanSignKeyword_0_11_0_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getRuleAccess().getCompOpGreaterThanSignKeyword_0_11_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMethodName.g:464:2: ( '<' )
                    {
                    // InternalMethodName.g:464:2: ( '<' )
                    // InternalMethodName.g:465:3: '<'
                    {
                     before(grammarAccess.getRuleAccess().getCompOpLessThanSignKeyword_0_11_0_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getRuleAccess().getCompOpLessThanSignKeyword_0_11_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMethodName.g:470:2: ( '==' )
                    {
                    // InternalMethodName.g:470:2: ( '==' )
                    // InternalMethodName.g:471:3: '=='
                    {
                     before(grammarAccess.getRuleAccess().getCompOpEqualsSignEqualsSignKeyword_0_11_0_2()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getRuleAccess().getCompOpEqualsSignEqualsSignKeyword_0_11_0_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMethodName.g:476:2: ( '<=' )
                    {
                    // InternalMethodName.g:476:2: ( '<=' )
                    // InternalMethodName.g:477:3: '<='
                    {
                     before(grammarAccess.getRuleAccess().getCompOpLessThanSignEqualsSignKeyword_0_11_0_3()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getRuleAccess().getCompOpLessThanSignEqualsSignKeyword_0_11_0_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalMethodName.g:482:2: ( '>=' )
                    {
                    // InternalMethodName.g:482:2: ( '>=' )
                    // InternalMethodName.g:483:3: '>='
                    {
                     before(grammarAccess.getRuleAccess().getCompOpGreaterThanSignEqualsSignKeyword_0_11_0_4()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getRuleAccess().getCompOpGreaterThanSignEqualsSignKeyword_0_11_0_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__CompOpAlternatives_0_11_0"


    // $ANTLR start "rule__And__ConcatKindAlternatives_1_1_0"
    // InternalMethodName.g:492:1: rule__And__ConcatKindAlternatives_1_1_0 : ( ( '.' ) | ( '...' ) );
    public final void rule__And__ConcatKindAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:496:1: ( ( '.' ) | ( '...' ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==16) ) {
                alt3=1;
            }
            else if ( (LA3_0==17) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalMethodName.g:497:2: ( '.' )
                    {
                    // InternalMethodName.g:497:2: ( '.' )
                    // InternalMethodName.g:498:3: '.'
                    {
                     before(grammarAccess.getAndAccess().getConcatKindFullStopKeyword_1_1_0_0()); 
                    match(input,16,FOLLOW_2); 
                     after(grammarAccess.getAndAccess().getConcatKindFullStopKeyword_1_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMethodName.g:503:2: ( '...' )
                    {
                    // InternalMethodName.g:503:2: ( '...' )
                    // InternalMethodName.g:504:3: '...'
                    {
                     before(grammarAccess.getAndAccess().getConcatKindFullStopFullStopFullStopKeyword_1_1_0_1()); 
                    match(input,17,FOLLOW_2); 
                     after(grammarAccess.getAndAccess().getConcatKindFullStopFullStopFullStopKeyword_1_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__ConcatKindAlternatives_1_1_0"


    // $ANTLR start "rule__Primary__Alternatives"
    // InternalMethodName.g:513:1: rule__Primary__Alternatives : ( ( ruleGroupExpr ) | ( ruleAtomic ) );
    public final void rule__Primary__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:517:1: ( ( ruleGroupExpr ) | ( ruleAtomic ) )
            int alt4=2;
            switch ( input.LA(1) ) {
            case 92:
                {
                int LA4_1 = input.LA(2);

                if ( (LA4_1==RULE_STRING||(LA4_1>=27 && LA4_1<=60)||LA4_1==91) ) {
                    alt4=2;
                }
                else if ( (LA4_1==87) ) {
                    alt4=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 4, 1, input);

                    throw nvae;
                }
                }
                break;
            case 87:
                {
                alt4=1;
                }
                break;
            case RULE_STRING:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 91:
                {
                alt4=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalMethodName.g:518:2: ( ruleGroupExpr )
                    {
                    // InternalMethodName.g:518:2: ( ruleGroupExpr )
                    // InternalMethodName.g:519:3: ruleGroupExpr
                    {
                     before(grammarAccess.getPrimaryAccess().getGroupExprParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleGroupExpr();

                    state._fsp--;

                     after(grammarAccess.getPrimaryAccess().getGroupExprParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMethodName.g:524:2: ( ruleAtomic )
                    {
                    // InternalMethodName.g:524:2: ( ruleAtomic )
                    // InternalMethodName.g:525:3: ruleAtomic
                    {
                     before(grammarAccess.getPrimaryAccess().getAtomicParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleAtomic();

                    state._fsp--;

                     after(grammarAccess.getPrimaryAccess().getAtomicParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Alternatives"


    // $ANTLR start "rule__Atomic__Alternatives"
    // InternalMethodName.g:534:1: rule__Atomic__Alternatives : ( ( ( rule__Atomic__Group_0__0 ) ) | ( ( rule__Atomic__Group_1__0 ) ) | ( ( rule__Atomic__Group_2__0 ) ) );
    public final void rule__Atomic__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:538:1: ( ( ( rule__Atomic__Group_0__0 ) ) | ( ( rule__Atomic__Group_1__0 ) ) | ( ( rule__Atomic__Group_2__0 ) ) )
            int alt5=3;
            switch ( input.LA(1) ) {
            case 92:
                {
                switch ( input.LA(2) ) {
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                case 50:
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 59:
                case 60:
                    {
                    alt5=2;
                    }
                    break;
                case 91:
                    {
                    alt5=3;
                    }
                    break;
                case RULE_STRING:
                    {
                    alt5=1;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 5, 1, input);

                    throw nvae;
                }

                }
                break;
            case RULE_STRING:
                {
                alt5=1;
                }
                break;
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
                {
                alt5=2;
                }
                break;
            case 91:
                {
                alt5=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalMethodName.g:539:2: ( ( rule__Atomic__Group_0__0 ) )
                    {
                    // InternalMethodName.g:539:2: ( ( rule__Atomic__Group_0__0 ) )
                    // InternalMethodName.g:540:3: ( rule__Atomic__Group_0__0 )
                    {
                     before(grammarAccess.getAtomicAccess().getGroup_0()); 
                    // InternalMethodName.g:541:3: ( rule__Atomic__Group_0__0 )
                    // InternalMethodName.g:541:4: rule__Atomic__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMethodName.g:545:2: ( ( rule__Atomic__Group_1__0 ) )
                    {
                    // InternalMethodName.g:545:2: ( ( rule__Atomic__Group_1__0 ) )
                    // InternalMethodName.g:546:3: ( rule__Atomic__Group_1__0 )
                    {
                     before(grammarAccess.getAtomicAccess().getGroup_1()); 
                    // InternalMethodName.g:547:3: ( rule__Atomic__Group_1__0 )
                    // InternalMethodName.g:547:4: rule__Atomic__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMethodName.g:551:2: ( ( rule__Atomic__Group_2__0 ) )
                    {
                    // InternalMethodName.g:551:2: ( ( rule__Atomic__Group_2__0 ) )
                    // InternalMethodName.g:552:3: ( rule__Atomic__Group_2__0 )
                    {
                     before(grammarAccess.getAtomicAccess().getGroup_2()); 
                    // InternalMethodName.g:553:3: ( rule__Atomic__Group_2__0 )
                    // InternalMethodName.g:553:4: rule__Atomic__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAtomicAccess().getGroup_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Alternatives"


    // $ANTLR start "rule__Kind__Alternatives"
    // InternalMethodName.g:561:1: rule__Kind__Alternatives : ( ( ( 'interface' ) ) | ( ( 'package' ) ) | ( ( 'class' ) ) | ( ( 'method' ) ) | ( ( 'variable' ) ) );
    public final void rule__Kind__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:565:1: ( ( ( 'interface' ) ) | ( ( 'package' ) ) | ( ( 'class' ) ) | ( ( 'method' ) ) | ( ( 'variable' ) ) )
            int alt6=5;
            switch ( input.LA(1) ) {
            case 18:
                {
                alt6=1;
                }
                break;
            case 19:
                {
                alt6=2;
                }
                break;
            case 20:
                {
                alt6=3;
                }
                break;
            case 21:
                {
                alt6=4;
                }
                break;
            case 22:
                {
                alt6=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalMethodName.g:566:2: ( ( 'interface' ) )
                    {
                    // InternalMethodName.g:566:2: ( ( 'interface' ) )
                    // InternalMethodName.g:567:3: ( 'interface' )
                    {
                     before(grammarAccess.getKindAccess().getKIND_INTERFACEEnumLiteralDeclaration_0()); 
                    // InternalMethodName.g:568:3: ( 'interface' )
                    // InternalMethodName.g:568:4: 'interface'
                    {
                    match(input,18,FOLLOW_2); 

                    }

                     after(grammarAccess.getKindAccess().getKIND_INTERFACEEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMethodName.g:572:2: ( ( 'package' ) )
                    {
                    // InternalMethodName.g:572:2: ( ( 'package' ) )
                    // InternalMethodName.g:573:3: ( 'package' )
                    {
                     before(grammarAccess.getKindAccess().getKIND_PACKAGEEnumLiteralDeclaration_1()); 
                    // InternalMethodName.g:574:3: ( 'package' )
                    // InternalMethodName.g:574:4: 'package'
                    {
                    match(input,19,FOLLOW_2); 

                    }

                     after(grammarAccess.getKindAccess().getKIND_PACKAGEEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMethodName.g:578:2: ( ( 'class' ) )
                    {
                    // InternalMethodName.g:578:2: ( ( 'class' ) )
                    // InternalMethodName.g:579:3: ( 'class' )
                    {
                     before(grammarAccess.getKindAccess().getKIND_CLASSEnumLiteralDeclaration_2()); 
                    // InternalMethodName.g:580:3: ( 'class' )
                    // InternalMethodName.g:580:4: 'class'
                    {
                    match(input,20,FOLLOW_2); 

                    }

                     after(grammarAccess.getKindAccess().getKIND_CLASSEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMethodName.g:584:2: ( ( 'method' ) )
                    {
                    // InternalMethodName.g:584:2: ( ( 'method' ) )
                    // InternalMethodName.g:585:3: ( 'method' )
                    {
                     before(grammarAccess.getKindAccess().getKIND_METHODEnumLiteralDeclaration_3()); 
                    // InternalMethodName.g:586:3: ( 'method' )
                    // InternalMethodName.g:586:4: 'method'
                    {
                    match(input,21,FOLLOW_2); 

                    }

                     after(grammarAccess.getKindAccess().getKIND_METHODEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalMethodName.g:590:2: ( ( 'variable' ) )
                    {
                    // InternalMethodName.g:590:2: ( ( 'variable' ) )
                    // InternalMethodName.g:591:3: ( 'variable' )
                    {
                     before(grammarAccess.getKindAccess().getKIND_VAREnumLiteralDeclaration_4()); 
                    // InternalMethodName.g:592:3: ( 'variable' )
                    // InternalMethodName.g:592:4: 'variable'
                    {
                    match(input,22,FOLLOW_2); 

                    }

                     after(grammarAccess.getKindAccess().getKIND_VAREnumLiteralDeclaration_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Kind__Alternatives"


    // $ANTLR start "rule__Sentiment__Alternatives"
    // InternalMethodName.g:600:1: rule__Sentiment__Alternatives : ( ( ( 'SENT_UNSPECIFIED' ) ) | ( ( 'neutral' ) ) | ( ( 'positive' ) ) | ( ( 'negative' ) ) );
    public final void rule__Sentiment__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:604:1: ( ( ( 'SENT_UNSPECIFIED' ) ) | ( ( 'neutral' ) ) | ( ( 'positive' ) ) | ( ( 'negative' ) ) )
            int alt7=4;
            switch ( input.LA(1) ) {
            case 23:
                {
                alt7=1;
                }
                break;
            case 24:
                {
                alt7=2;
                }
                break;
            case 25:
                {
                alt7=3;
                }
                break;
            case 26:
                {
                alt7=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // InternalMethodName.g:605:2: ( ( 'SENT_UNSPECIFIED' ) )
                    {
                    // InternalMethodName.g:605:2: ( ( 'SENT_UNSPECIFIED' ) )
                    // InternalMethodName.g:606:3: ( 'SENT_UNSPECIFIED' )
                    {
                     before(grammarAccess.getSentimentAccess().getSENT_UNSPECIFIEDEnumLiteralDeclaration_0()); 
                    // InternalMethodName.g:607:3: ( 'SENT_UNSPECIFIED' )
                    // InternalMethodName.g:607:4: 'SENT_UNSPECIFIED'
                    {
                    match(input,23,FOLLOW_2); 

                    }

                     after(grammarAccess.getSentimentAccess().getSENT_UNSPECIFIEDEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMethodName.g:611:2: ( ( 'neutral' ) )
                    {
                    // InternalMethodName.g:611:2: ( ( 'neutral' ) )
                    // InternalMethodName.g:612:3: ( 'neutral' )
                    {
                     before(grammarAccess.getSentimentAccess().getSENT_NEUTRALEnumLiteralDeclaration_1()); 
                    // InternalMethodName.g:613:3: ( 'neutral' )
                    // InternalMethodName.g:613:4: 'neutral'
                    {
                    match(input,24,FOLLOW_2); 

                    }

                     after(grammarAccess.getSentimentAccess().getSENT_NEUTRALEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMethodName.g:617:2: ( ( 'positive' ) )
                    {
                    // InternalMethodName.g:617:2: ( ( 'positive' ) )
                    // InternalMethodName.g:618:3: ( 'positive' )
                    {
                     before(grammarAccess.getSentimentAccess().getSENT_POSITIVEEnumLiteralDeclaration_2()); 
                    // InternalMethodName.g:619:3: ( 'positive' )
                    // InternalMethodName.g:619:4: 'positive'
                    {
                    match(input,25,FOLLOW_2); 

                    }

                     after(grammarAccess.getSentimentAccess().getSENT_POSITIVEEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMethodName.g:623:2: ( ( 'negative' ) )
                    {
                    // InternalMethodName.g:623:2: ( ( 'negative' ) )
                    // InternalMethodName.g:624:3: ( 'negative' )
                    {
                     before(grammarAccess.getSentimentAccess().getSENT_NEGATIVEEnumLiteralDeclaration_3()); 
                    // InternalMethodName.g:625:3: ( 'negative' )
                    // InternalMethodName.g:625:4: 'negative'
                    {
                    match(input,26,FOLLOW_2); 

                    }

                     after(grammarAccess.getSentimentAccess().getSENT_NEGATIVEEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Sentiment__Alternatives"


    // $ANTLR start "rule__POS__Alternatives"
    // InternalMethodName.g:633:1: rule__POS__Alternatives : ( ( ( 'CC' ) ) | ( ( 'CD' ) ) | ( ( 'DT' ) ) | ( ( 'EX' ) ) | ( ( 'FW' ) ) | ( ( 'IN' ) ) | ( ( 'JJ' ) ) | ( ( 'JJR' ) ) | ( ( 'JJS' ) ) | ( ( 'LS' ) ) | ( ( 'MD' ) ) | ( ( 'NN' ) ) | ( ( 'NNS' ) ) | ( ( 'NNP' ) ) | ( ( 'NNPS' ) ) | ( ( 'PDT' ) ) | ( ( 'POS' ) ) | ( ( 'PRP' ) ) | ( ( 'RB' ) ) | ( ( 'RBR' ) ) | ( ( 'RBS' ) ) | ( ( 'RP' ) ) | ( ( 'SYM' ) ) | ( ( 'TO' ) ) | ( ( 'UH' ) ) | ( ( 'VB' ) ) | ( ( 'VBD' ) ) | ( ( 'VBG' ) ) | ( ( 'VBN' ) ) | ( ( 'VBP' ) ) | ( ( 'VBZ' ) ) | ( ( 'WDT' ) ) | ( ( 'WP' ) ) | ( ( 'WRB' ) ) );
    public final void rule__POS__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:637:1: ( ( ( 'CC' ) ) | ( ( 'CD' ) ) | ( ( 'DT' ) ) | ( ( 'EX' ) ) | ( ( 'FW' ) ) | ( ( 'IN' ) ) | ( ( 'JJ' ) ) | ( ( 'JJR' ) ) | ( ( 'JJS' ) ) | ( ( 'LS' ) ) | ( ( 'MD' ) ) | ( ( 'NN' ) ) | ( ( 'NNS' ) ) | ( ( 'NNP' ) ) | ( ( 'NNPS' ) ) | ( ( 'PDT' ) ) | ( ( 'POS' ) ) | ( ( 'PRP' ) ) | ( ( 'RB' ) ) | ( ( 'RBR' ) ) | ( ( 'RBS' ) ) | ( ( 'RP' ) ) | ( ( 'SYM' ) ) | ( ( 'TO' ) ) | ( ( 'UH' ) ) | ( ( 'VB' ) ) | ( ( 'VBD' ) ) | ( ( 'VBG' ) ) | ( ( 'VBN' ) ) | ( ( 'VBP' ) ) | ( ( 'VBZ' ) ) | ( ( 'WDT' ) ) | ( ( 'WP' ) ) | ( ( 'WRB' ) ) )
            int alt8=34;
            switch ( input.LA(1) ) {
            case 27:
                {
                alt8=1;
                }
                break;
            case 28:
                {
                alt8=2;
                }
                break;
            case 29:
                {
                alt8=3;
                }
                break;
            case 30:
                {
                alt8=4;
                }
                break;
            case 31:
                {
                alt8=5;
                }
                break;
            case 32:
                {
                alt8=6;
                }
                break;
            case 33:
                {
                alt8=7;
                }
                break;
            case 34:
                {
                alt8=8;
                }
                break;
            case 35:
                {
                alt8=9;
                }
                break;
            case 36:
                {
                alt8=10;
                }
                break;
            case 37:
                {
                alt8=11;
                }
                break;
            case 38:
                {
                alt8=12;
                }
                break;
            case 39:
                {
                alt8=13;
                }
                break;
            case 40:
                {
                alt8=14;
                }
                break;
            case 41:
                {
                alt8=15;
                }
                break;
            case 42:
                {
                alt8=16;
                }
                break;
            case 43:
                {
                alt8=17;
                }
                break;
            case 44:
                {
                alt8=18;
                }
                break;
            case 45:
                {
                alt8=19;
                }
                break;
            case 46:
                {
                alt8=20;
                }
                break;
            case 47:
                {
                alt8=21;
                }
                break;
            case 48:
                {
                alt8=22;
                }
                break;
            case 49:
                {
                alt8=23;
                }
                break;
            case 50:
                {
                alt8=24;
                }
                break;
            case 51:
                {
                alt8=25;
                }
                break;
            case 52:
                {
                alt8=26;
                }
                break;
            case 53:
                {
                alt8=27;
                }
                break;
            case 54:
                {
                alt8=28;
                }
                break;
            case 55:
                {
                alt8=29;
                }
                break;
            case 56:
                {
                alt8=30;
                }
                break;
            case 57:
                {
                alt8=31;
                }
                break;
            case 58:
                {
                alt8=32;
                }
                break;
            case 59:
                {
                alt8=33;
                }
                break;
            case 60:
                {
                alt8=34;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalMethodName.g:638:2: ( ( 'CC' ) )
                    {
                    // InternalMethodName.g:638:2: ( ( 'CC' ) )
                    // InternalMethodName.g:639:3: ( 'CC' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_CCEnumLiteralDeclaration_0()); 
                    // InternalMethodName.g:640:3: ( 'CC' )
                    // InternalMethodName.g:640:4: 'CC'
                    {
                    match(input,27,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_CCEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMethodName.g:644:2: ( ( 'CD' ) )
                    {
                    // InternalMethodName.g:644:2: ( ( 'CD' ) )
                    // InternalMethodName.g:645:3: ( 'CD' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_CDEnumLiteralDeclaration_1()); 
                    // InternalMethodName.g:646:3: ( 'CD' )
                    // InternalMethodName.g:646:4: 'CD'
                    {
                    match(input,28,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_CDEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMethodName.g:650:2: ( ( 'DT' ) )
                    {
                    // InternalMethodName.g:650:2: ( ( 'DT' ) )
                    // InternalMethodName.g:651:3: ( 'DT' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_DTEnumLiteralDeclaration_2()); 
                    // InternalMethodName.g:652:3: ( 'DT' )
                    // InternalMethodName.g:652:4: 'DT'
                    {
                    match(input,29,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_DTEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMethodName.g:656:2: ( ( 'EX' ) )
                    {
                    // InternalMethodName.g:656:2: ( ( 'EX' ) )
                    // InternalMethodName.g:657:3: ( 'EX' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_EXEnumLiteralDeclaration_3()); 
                    // InternalMethodName.g:658:3: ( 'EX' )
                    // InternalMethodName.g:658:4: 'EX'
                    {
                    match(input,30,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_EXEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalMethodName.g:662:2: ( ( 'FW' ) )
                    {
                    // InternalMethodName.g:662:2: ( ( 'FW' ) )
                    // InternalMethodName.g:663:3: ( 'FW' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_FWEnumLiteralDeclaration_4()); 
                    // InternalMethodName.g:664:3: ( 'FW' )
                    // InternalMethodName.g:664:4: 'FW'
                    {
                    match(input,31,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_FWEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalMethodName.g:668:2: ( ( 'IN' ) )
                    {
                    // InternalMethodName.g:668:2: ( ( 'IN' ) )
                    // InternalMethodName.g:669:3: ( 'IN' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_INEnumLiteralDeclaration_5()); 
                    // InternalMethodName.g:670:3: ( 'IN' )
                    // InternalMethodName.g:670:4: 'IN'
                    {
                    match(input,32,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_INEnumLiteralDeclaration_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalMethodName.g:674:2: ( ( 'JJ' ) )
                    {
                    // InternalMethodName.g:674:2: ( ( 'JJ' ) )
                    // InternalMethodName.g:675:3: ( 'JJ' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_JJEnumLiteralDeclaration_6()); 
                    // InternalMethodName.g:676:3: ( 'JJ' )
                    // InternalMethodName.g:676:4: 'JJ'
                    {
                    match(input,33,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_JJEnumLiteralDeclaration_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalMethodName.g:680:2: ( ( 'JJR' ) )
                    {
                    // InternalMethodName.g:680:2: ( ( 'JJR' ) )
                    // InternalMethodName.g:681:3: ( 'JJR' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_JJREnumLiteralDeclaration_7()); 
                    // InternalMethodName.g:682:3: ( 'JJR' )
                    // InternalMethodName.g:682:4: 'JJR'
                    {
                    match(input,34,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_JJREnumLiteralDeclaration_7()); 

                    }


                    }
                    break;
                case 9 :
                    // InternalMethodName.g:686:2: ( ( 'JJS' ) )
                    {
                    // InternalMethodName.g:686:2: ( ( 'JJS' ) )
                    // InternalMethodName.g:687:3: ( 'JJS' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_JJSEnumLiteralDeclaration_8()); 
                    // InternalMethodName.g:688:3: ( 'JJS' )
                    // InternalMethodName.g:688:4: 'JJS'
                    {
                    match(input,35,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_JJSEnumLiteralDeclaration_8()); 

                    }


                    }
                    break;
                case 10 :
                    // InternalMethodName.g:692:2: ( ( 'LS' ) )
                    {
                    // InternalMethodName.g:692:2: ( ( 'LS' ) )
                    // InternalMethodName.g:693:3: ( 'LS' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_LSEnumLiteralDeclaration_9()); 
                    // InternalMethodName.g:694:3: ( 'LS' )
                    // InternalMethodName.g:694:4: 'LS'
                    {
                    match(input,36,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_LSEnumLiteralDeclaration_9()); 

                    }


                    }
                    break;
                case 11 :
                    // InternalMethodName.g:698:2: ( ( 'MD' ) )
                    {
                    // InternalMethodName.g:698:2: ( ( 'MD' ) )
                    // InternalMethodName.g:699:3: ( 'MD' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_MDEnumLiteralDeclaration_10()); 
                    // InternalMethodName.g:700:3: ( 'MD' )
                    // InternalMethodName.g:700:4: 'MD'
                    {
                    match(input,37,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_MDEnumLiteralDeclaration_10()); 

                    }


                    }
                    break;
                case 12 :
                    // InternalMethodName.g:704:2: ( ( 'NN' ) )
                    {
                    // InternalMethodName.g:704:2: ( ( 'NN' ) )
                    // InternalMethodName.g:705:3: ( 'NN' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_NNEnumLiteralDeclaration_11()); 
                    // InternalMethodName.g:706:3: ( 'NN' )
                    // InternalMethodName.g:706:4: 'NN'
                    {
                    match(input,38,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_NNEnumLiteralDeclaration_11()); 

                    }


                    }
                    break;
                case 13 :
                    // InternalMethodName.g:710:2: ( ( 'NNS' ) )
                    {
                    // InternalMethodName.g:710:2: ( ( 'NNS' ) )
                    // InternalMethodName.g:711:3: ( 'NNS' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_NNSEnumLiteralDeclaration_12()); 
                    // InternalMethodName.g:712:3: ( 'NNS' )
                    // InternalMethodName.g:712:4: 'NNS'
                    {
                    match(input,39,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_NNSEnumLiteralDeclaration_12()); 

                    }


                    }
                    break;
                case 14 :
                    // InternalMethodName.g:716:2: ( ( 'NNP' ) )
                    {
                    // InternalMethodName.g:716:2: ( ( 'NNP' ) )
                    // InternalMethodName.g:717:3: ( 'NNP' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_NNPEnumLiteralDeclaration_13()); 
                    // InternalMethodName.g:718:3: ( 'NNP' )
                    // InternalMethodName.g:718:4: 'NNP'
                    {
                    match(input,40,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_NNPEnumLiteralDeclaration_13()); 

                    }


                    }
                    break;
                case 15 :
                    // InternalMethodName.g:722:2: ( ( 'NNPS' ) )
                    {
                    // InternalMethodName.g:722:2: ( ( 'NNPS' ) )
                    // InternalMethodName.g:723:3: ( 'NNPS' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_NNPSEnumLiteralDeclaration_14()); 
                    // InternalMethodName.g:724:3: ( 'NNPS' )
                    // InternalMethodName.g:724:4: 'NNPS'
                    {
                    match(input,41,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_NNPSEnumLiteralDeclaration_14()); 

                    }


                    }
                    break;
                case 16 :
                    // InternalMethodName.g:728:2: ( ( 'PDT' ) )
                    {
                    // InternalMethodName.g:728:2: ( ( 'PDT' ) )
                    // InternalMethodName.g:729:3: ( 'PDT' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_PDTEnumLiteralDeclaration_15()); 
                    // InternalMethodName.g:730:3: ( 'PDT' )
                    // InternalMethodName.g:730:4: 'PDT'
                    {
                    match(input,42,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_PDTEnumLiteralDeclaration_15()); 

                    }


                    }
                    break;
                case 17 :
                    // InternalMethodName.g:734:2: ( ( 'POS' ) )
                    {
                    // InternalMethodName.g:734:2: ( ( 'POS' ) )
                    // InternalMethodName.g:735:3: ( 'POS' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_POSEnumLiteralDeclaration_16()); 
                    // InternalMethodName.g:736:3: ( 'POS' )
                    // InternalMethodName.g:736:4: 'POS'
                    {
                    match(input,43,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_POSEnumLiteralDeclaration_16()); 

                    }


                    }
                    break;
                case 18 :
                    // InternalMethodName.g:740:2: ( ( 'PRP' ) )
                    {
                    // InternalMethodName.g:740:2: ( ( 'PRP' ) )
                    // InternalMethodName.g:741:3: ( 'PRP' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_PRPEnumLiteralDeclaration_17()); 
                    // InternalMethodName.g:742:3: ( 'PRP' )
                    // InternalMethodName.g:742:4: 'PRP'
                    {
                    match(input,44,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_PRPEnumLiteralDeclaration_17()); 

                    }


                    }
                    break;
                case 19 :
                    // InternalMethodName.g:746:2: ( ( 'RB' ) )
                    {
                    // InternalMethodName.g:746:2: ( ( 'RB' ) )
                    // InternalMethodName.g:747:3: ( 'RB' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_RBEnumLiteralDeclaration_18()); 
                    // InternalMethodName.g:748:3: ( 'RB' )
                    // InternalMethodName.g:748:4: 'RB'
                    {
                    match(input,45,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_RBEnumLiteralDeclaration_18()); 

                    }


                    }
                    break;
                case 20 :
                    // InternalMethodName.g:752:2: ( ( 'RBR' ) )
                    {
                    // InternalMethodName.g:752:2: ( ( 'RBR' ) )
                    // InternalMethodName.g:753:3: ( 'RBR' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_RBREnumLiteralDeclaration_19()); 
                    // InternalMethodName.g:754:3: ( 'RBR' )
                    // InternalMethodName.g:754:4: 'RBR'
                    {
                    match(input,46,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_RBREnumLiteralDeclaration_19()); 

                    }


                    }
                    break;
                case 21 :
                    // InternalMethodName.g:758:2: ( ( 'RBS' ) )
                    {
                    // InternalMethodName.g:758:2: ( ( 'RBS' ) )
                    // InternalMethodName.g:759:3: ( 'RBS' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_RBSEnumLiteralDeclaration_20()); 
                    // InternalMethodName.g:760:3: ( 'RBS' )
                    // InternalMethodName.g:760:4: 'RBS'
                    {
                    match(input,47,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_RBSEnumLiteralDeclaration_20()); 

                    }


                    }
                    break;
                case 22 :
                    // InternalMethodName.g:764:2: ( ( 'RP' ) )
                    {
                    // InternalMethodName.g:764:2: ( ( 'RP' ) )
                    // InternalMethodName.g:765:3: ( 'RP' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_RPEnumLiteralDeclaration_21()); 
                    // InternalMethodName.g:766:3: ( 'RP' )
                    // InternalMethodName.g:766:4: 'RP'
                    {
                    match(input,48,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_RPEnumLiteralDeclaration_21()); 

                    }


                    }
                    break;
                case 23 :
                    // InternalMethodName.g:770:2: ( ( 'SYM' ) )
                    {
                    // InternalMethodName.g:770:2: ( ( 'SYM' ) )
                    // InternalMethodName.g:771:3: ( 'SYM' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_SYMEnumLiteralDeclaration_22()); 
                    // InternalMethodName.g:772:3: ( 'SYM' )
                    // InternalMethodName.g:772:4: 'SYM'
                    {
                    match(input,49,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_SYMEnumLiteralDeclaration_22()); 

                    }


                    }
                    break;
                case 24 :
                    // InternalMethodName.g:776:2: ( ( 'TO' ) )
                    {
                    // InternalMethodName.g:776:2: ( ( 'TO' ) )
                    // InternalMethodName.g:777:3: ( 'TO' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_TOEnumLiteralDeclaration_23()); 
                    // InternalMethodName.g:778:3: ( 'TO' )
                    // InternalMethodName.g:778:4: 'TO'
                    {
                    match(input,50,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_TOEnumLiteralDeclaration_23()); 

                    }


                    }
                    break;
                case 25 :
                    // InternalMethodName.g:782:2: ( ( 'UH' ) )
                    {
                    // InternalMethodName.g:782:2: ( ( 'UH' ) )
                    // InternalMethodName.g:783:3: ( 'UH' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_UHEnumLiteralDeclaration_24()); 
                    // InternalMethodName.g:784:3: ( 'UH' )
                    // InternalMethodName.g:784:4: 'UH'
                    {
                    match(input,51,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_UHEnumLiteralDeclaration_24()); 

                    }


                    }
                    break;
                case 26 :
                    // InternalMethodName.g:788:2: ( ( 'VB' ) )
                    {
                    // InternalMethodName.g:788:2: ( ( 'VB' ) )
                    // InternalMethodName.g:789:3: ( 'VB' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_VBEnumLiteralDeclaration_25()); 
                    // InternalMethodName.g:790:3: ( 'VB' )
                    // InternalMethodName.g:790:4: 'VB'
                    {
                    match(input,52,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_VBEnumLiteralDeclaration_25()); 

                    }


                    }
                    break;
                case 27 :
                    // InternalMethodName.g:794:2: ( ( 'VBD' ) )
                    {
                    // InternalMethodName.g:794:2: ( ( 'VBD' ) )
                    // InternalMethodName.g:795:3: ( 'VBD' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_VBDEnumLiteralDeclaration_26()); 
                    // InternalMethodName.g:796:3: ( 'VBD' )
                    // InternalMethodName.g:796:4: 'VBD'
                    {
                    match(input,53,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_VBDEnumLiteralDeclaration_26()); 

                    }


                    }
                    break;
                case 28 :
                    // InternalMethodName.g:800:2: ( ( 'VBG' ) )
                    {
                    // InternalMethodName.g:800:2: ( ( 'VBG' ) )
                    // InternalMethodName.g:801:3: ( 'VBG' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_VBGEnumLiteralDeclaration_27()); 
                    // InternalMethodName.g:802:3: ( 'VBG' )
                    // InternalMethodName.g:802:4: 'VBG'
                    {
                    match(input,54,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_VBGEnumLiteralDeclaration_27()); 

                    }


                    }
                    break;
                case 29 :
                    // InternalMethodName.g:806:2: ( ( 'VBN' ) )
                    {
                    // InternalMethodName.g:806:2: ( ( 'VBN' ) )
                    // InternalMethodName.g:807:3: ( 'VBN' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_VBNEnumLiteralDeclaration_28()); 
                    // InternalMethodName.g:808:3: ( 'VBN' )
                    // InternalMethodName.g:808:4: 'VBN'
                    {
                    match(input,55,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_VBNEnumLiteralDeclaration_28()); 

                    }


                    }
                    break;
                case 30 :
                    // InternalMethodName.g:812:2: ( ( 'VBP' ) )
                    {
                    // InternalMethodName.g:812:2: ( ( 'VBP' ) )
                    // InternalMethodName.g:813:3: ( 'VBP' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_VBPEnumLiteralDeclaration_29()); 
                    // InternalMethodName.g:814:3: ( 'VBP' )
                    // InternalMethodName.g:814:4: 'VBP'
                    {
                    match(input,56,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_VBPEnumLiteralDeclaration_29()); 

                    }


                    }
                    break;
                case 31 :
                    // InternalMethodName.g:818:2: ( ( 'VBZ' ) )
                    {
                    // InternalMethodName.g:818:2: ( ( 'VBZ' ) )
                    // InternalMethodName.g:819:3: ( 'VBZ' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_VBZEnumLiteralDeclaration_30()); 
                    // InternalMethodName.g:820:3: ( 'VBZ' )
                    // InternalMethodName.g:820:4: 'VBZ'
                    {
                    match(input,57,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_VBZEnumLiteralDeclaration_30()); 

                    }


                    }
                    break;
                case 32 :
                    // InternalMethodName.g:824:2: ( ( 'WDT' ) )
                    {
                    // InternalMethodName.g:824:2: ( ( 'WDT' ) )
                    // InternalMethodName.g:825:3: ( 'WDT' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_WDTEnumLiteralDeclaration_31()); 
                    // InternalMethodName.g:826:3: ( 'WDT' )
                    // InternalMethodName.g:826:4: 'WDT'
                    {
                    match(input,58,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_WDTEnumLiteralDeclaration_31()); 

                    }


                    }
                    break;
                case 33 :
                    // InternalMethodName.g:830:2: ( ( 'WP' ) )
                    {
                    // InternalMethodName.g:830:2: ( ( 'WP' ) )
                    // InternalMethodName.g:831:3: ( 'WP' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_WPEnumLiteralDeclaration_32()); 
                    // InternalMethodName.g:832:3: ( 'WP' )
                    // InternalMethodName.g:832:4: 'WP'
                    {
                    match(input,59,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_WPEnumLiteralDeclaration_32()); 

                    }


                    }
                    break;
                case 34 :
                    // InternalMethodName.g:836:2: ( ( 'WRB' ) )
                    {
                    // InternalMethodName.g:836:2: ( ( 'WRB' ) )
                    // InternalMethodName.g:837:3: ( 'WRB' )
                    {
                     before(grammarAccess.getPOSAccess().getPOS_WRBEnumLiteralDeclaration_33()); 
                    // InternalMethodName.g:838:3: ( 'WRB' )
                    // InternalMethodName.g:838:4: 'WRB'
                    {
                    match(input,60,FOLLOW_2); 

                    }

                     after(grammarAccess.getPOSAccess().getPOS_WRBEnumLiteralDeclaration_33()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__POS__Alternatives"


    // $ANTLR start "rule__CardinalityModifier__Alternatives"
    // InternalMethodName.g:846:1: rule__CardinalityModifier__Alternatives : ( ( ( 'CARD_1' ) ) | ( ( '*' ) ) | ( ( '+' ) ) | ( ( '?' ) ) );
    public final void rule__CardinalityModifier__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:850:1: ( ( ( 'CARD_1' ) ) | ( ( '*' ) ) | ( ( '+' ) ) | ( ( '?' ) ) )
            int alt9=4;
            switch ( input.LA(1) ) {
            case 61:
                {
                alt9=1;
                }
                break;
            case 62:
                {
                alt9=2;
                }
                break;
            case 63:
                {
                alt9=3;
                }
                break;
            case 64:
                {
                alt9=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalMethodName.g:851:2: ( ( 'CARD_1' ) )
                    {
                    // InternalMethodName.g:851:2: ( ( 'CARD_1' ) )
                    // InternalMethodName.g:852:3: ( 'CARD_1' )
                    {
                     before(grammarAccess.getCardinalityModifierAccess().getCARD_1EnumLiteralDeclaration_0()); 
                    // InternalMethodName.g:853:3: ( 'CARD_1' )
                    // InternalMethodName.g:853:4: 'CARD_1'
                    {
                    match(input,61,FOLLOW_2); 

                    }

                     after(grammarAccess.getCardinalityModifierAccess().getCARD_1EnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMethodName.g:857:2: ( ( '*' ) )
                    {
                    // InternalMethodName.g:857:2: ( ( '*' ) )
                    // InternalMethodName.g:858:3: ( '*' )
                    {
                     before(grammarAccess.getCardinalityModifierAccess().getCARD_0_NEnumLiteralDeclaration_1()); 
                    // InternalMethodName.g:859:3: ( '*' )
                    // InternalMethodName.g:859:4: '*'
                    {
                    match(input,62,FOLLOW_2); 

                    }

                     after(grammarAccess.getCardinalityModifierAccess().getCARD_0_NEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMethodName.g:863:2: ( ( '+' ) )
                    {
                    // InternalMethodName.g:863:2: ( ( '+' ) )
                    // InternalMethodName.g:864:3: ( '+' )
                    {
                     before(grammarAccess.getCardinalityModifierAccess().getCARD_1_NEnumLiteralDeclaration_2()); 
                    // InternalMethodName.g:865:3: ( '+' )
                    // InternalMethodName.g:865:4: '+'
                    {
                    match(input,63,FOLLOW_2); 

                    }

                     after(grammarAccess.getCardinalityModifierAccess().getCARD_1_NEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMethodName.g:869:2: ( ( '?' ) )
                    {
                    // InternalMethodName.g:869:2: ( ( '?' ) )
                    // InternalMethodName.g:870:3: ( '?' )
                    {
                     before(grammarAccess.getCardinalityModifierAccess().getCARD_0_1EnumLiteralDeclaration_3()); 
                    // InternalMethodName.g:871:3: ( '?' )
                    // InternalMethodName.g:871:4: '?'
                    {
                    match(input,64,FOLLOW_2); 

                    }

                     after(grammarAccess.getCardinalityModifierAccess().getCARD_0_1EnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CardinalityModifier__Alternatives"


    // $ANTLR start "rule__Frequency__Alternatives"
    // InternalMethodName.g:879:1: rule__Frequency__Alternatives : ( ( ( 'always' ) ) | ( ( 'very-often' ) ) | ( ( 'often' ) ) | ( ( 'rarely' ) ) | ( ( 'very-seldom' ) ) | ( ( 'never' ) ) );
    public final void rule__Frequency__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:883:1: ( ( ( 'always' ) ) | ( ( 'very-often' ) ) | ( ( 'often' ) ) | ( ( 'rarely' ) ) | ( ( 'very-seldom' ) ) | ( ( 'never' ) ) )
            int alt10=6;
            switch ( input.LA(1) ) {
            case 65:
                {
                alt10=1;
                }
                break;
            case 66:
                {
                alt10=2;
                }
                break;
            case 67:
                {
                alt10=3;
                }
                break;
            case 68:
                {
                alt10=4;
                }
                break;
            case 69:
                {
                alt10=5;
                }
                break;
            case 70:
                {
                alt10=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalMethodName.g:884:2: ( ( 'always' ) )
                    {
                    // InternalMethodName.g:884:2: ( ( 'always' ) )
                    // InternalMethodName.g:885:3: ( 'always' )
                    {
                     before(grammarAccess.getFrequencyAccess().getALWAYSEnumLiteralDeclaration_0()); 
                    // InternalMethodName.g:886:3: ( 'always' )
                    // InternalMethodName.g:886:4: 'always'
                    {
                    match(input,65,FOLLOW_2); 

                    }

                     after(grammarAccess.getFrequencyAccess().getALWAYSEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMethodName.g:890:2: ( ( 'very-often' ) )
                    {
                    // InternalMethodName.g:890:2: ( ( 'very-often' ) )
                    // InternalMethodName.g:891:3: ( 'very-often' )
                    {
                     before(grammarAccess.getFrequencyAccess().getVERYOFTENEnumLiteralDeclaration_1()); 
                    // InternalMethodName.g:892:3: ( 'very-often' )
                    // InternalMethodName.g:892:4: 'very-often'
                    {
                    match(input,66,FOLLOW_2); 

                    }

                     after(grammarAccess.getFrequencyAccess().getVERYOFTENEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMethodName.g:896:2: ( ( 'often' ) )
                    {
                    // InternalMethodName.g:896:2: ( ( 'often' ) )
                    // InternalMethodName.g:897:3: ( 'often' )
                    {
                     before(grammarAccess.getFrequencyAccess().getOFTEEnumLiteralDeclaration_2()); 
                    // InternalMethodName.g:898:3: ( 'often' )
                    // InternalMethodName.g:898:4: 'often'
                    {
                    match(input,67,FOLLOW_2); 

                    }

                     after(grammarAccess.getFrequencyAccess().getOFTEEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMethodName.g:902:2: ( ( 'rarely' ) )
                    {
                    // InternalMethodName.g:902:2: ( ( 'rarely' ) )
                    // InternalMethodName.g:903:3: ( 'rarely' )
                    {
                     before(grammarAccess.getFrequencyAccess().getRARELYEnumLiteralDeclaration_3()); 
                    // InternalMethodName.g:904:3: ( 'rarely' )
                    // InternalMethodName.g:904:4: 'rarely'
                    {
                    match(input,68,FOLLOW_2); 

                    }

                     after(grammarAccess.getFrequencyAccess().getRARELYEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalMethodName.g:908:2: ( ( 'very-seldom' ) )
                    {
                    // InternalMethodName.g:908:2: ( ( 'very-seldom' ) )
                    // InternalMethodName.g:909:3: ( 'very-seldom' )
                    {
                     before(grammarAccess.getFrequencyAccess().getVERYSELDOMEnumLiteralDeclaration_4()); 
                    // InternalMethodName.g:910:3: ( 'very-seldom' )
                    // InternalMethodName.g:910:4: 'very-seldom'
                    {
                    match(input,69,FOLLOW_2); 

                    }

                     after(grammarAccess.getFrequencyAccess().getVERYSELDOMEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalMethodName.g:914:2: ( ( 'never' ) )
                    {
                    // InternalMethodName.g:914:2: ( ( 'never' ) )
                    // InternalMethodName.g:915:3: ( 'never' )
                    {
                     before(grammarAccess.getFrequencyAccess().getNEVEREnumLiteralDeclaration_5()); 
                    // InternalMethodName.g:916:3: ( 'never' )
                    // InternalMethodName.g:916:4: 'never'
                    {
                    match(input,70,FOLLOW_2); 

                    }

                     after(grammarAccess.getFrequencyAccess().getNEVEREnumLiteralDeclaration_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Frequency__Alternatives"


    // $ANTLR start "rule__Model__Group__0"
    // InternalMethodName.g:924:1: rule__Model__Group__0 : rule__Model__Group__0__Impl rule__Model__Group__1 ;
    public final void rule__Model__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:928:1: ( rule__Model__Group__0__Impl rule__Model__Group__1 )
            // InternalMethodName.g:929:2: rule__Model__Group__0__Impl rule__Model__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Model__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0"


    // $ANTLR start "rule__Model__Group__0__Impl"
    // InternalMethodName.g:936:1: rule__Model__Group__0__Impl : ( 'declarations' ) ;
    public final void rule__Model__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:940:1: ( ( 'declarations' ) )
            // InternalMethodName.g:941:1: ( 'declarations' )
            {
            // InternalMethodName.g:941:1: ( 'declarations' )
            // InternalMethodName.g:942:2: 'declarations'
            {
             before(grammarAccess.getModelAccess().getDeclarationsKeyword_0()); 
            match(input,71,FOLLOW_2); 
             after(grammarAccess.getModelAccess().getDeclarationsKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0__Impl"


    // $ANTLR start "rule__Model__Group__1"
    // InternalMethodName.g:951:1: rule__Model__Group__1 : rule__Model__Group__1__Impl rule__Model__Group__2 ;
    public final void rule__Model__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:955:1: ( rule__Model__Group__1__Impl rule__Model__Group__2 )
            // InternalMethodName.g:956:2: rule__Model__Group__1__Impl rule__Model__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__Model__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1"


    // $ANTLR start "rule__Model__Group__1__Impl"
    // InternalMethodName.g:963:1: rule__Model__Group__1__Impl : ( ( rule__Model__DeclarationsAssignment_1 )* ) ;
    public final void rule__Model__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:967:1: ( ( ( rule__Model__DeclarationsAssignment_1 )* ) )
            // InternalMethodName.g:968:1: ( ( rule__Model__DeclarationsAssignment_1 )* )
            {
            // InternalMethodName.g:968:1: ( ( rule__Model__DeclarationsAssignment_1 )* )
            // InternalMethodName.g:969:2: ( rule__Model__DeclarationsAssignment_1 )*
            {
             before(grammarAccess.getModelAccess().getDeclarationsAssignment_1()); 
            // InternalMethodName.g:970:2: ( rule__Model__DeclarationsAssignment_1 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==RULE_ID) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalMethodName.g:970:3: rule__Model__DeclarationsAssignment_1
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__Model__DeclarationsAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getDeclarationsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1__Impl"


    // $ANTLR start "rule__Model__Group__2"
    // InternalMethodName.g:978:1: rule__Model__Group__2 : rule__Model__Group__2__Impl rule__Model__Group__3 ;
    public final void rule__Model__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:982:1: ( rule__Model__Group__2__Impl rule__Model__Group__3 )
            // InternalMethodName.g:983:2: rule__Model__Group__2__Impl rule__Model__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Model__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__2"


    // $ANTLR start "rule__Model__Group__2__Impl"
    // InternalMethodName.g:990:1: rule__Model__Group__2__Impl : ( ( ( rule__Model__FixedDeclarationsAssignment_2 ) ) ( ( rule__Model__FixedDeclarationsAssignment_2 )* ) ) ;
    public final void rule__Model__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:994:1: ( ( ( ( rule__Model__FixedDeclarationsAssignment_2 ) ) ( ( rule__Model__FixedDeclarationsAssignment_2 )* ) ) )
            // InternalMethodName.g:995:1: ( ( ( rule__Model__FixedDeclarationsAssignment_2 ) ) ( ( rule__Model__FixedDeclarationsAssignment_2 )* ) )
            {
            // InternalMethodName.g:995:1: ( ( ( rule__Model__FixedDeclarationsAssignment_2 ) ) ( ( rule__Model__FixedDeclarationsAssignment_2 )* ) )
            // InternalMethodName.g:996:2: ( ( rule__Model__FixedDeclarationsAssignment_2 ) ) ( ( rule__Model__FixedDeclarationsAssignment_2 )* )
            {
            // InternalMethodName.g:996:2: ( ( rule__Model__FixedDeclarationsAssignment_2 ) )
            // InternalMethodName.g:997:3: ( rule__Model__FixedDeclarationsAssignment_2 )
            {
             before(grammarAccess.getModelAccess().getFixedDeclarationsAssignment_2()); 
            // InternalMethodName.g:998:3: ( rule__Model__FixedDeclarationsAssignment_2 )
            // InternalMethodName.g:998:4: rule__Model__FixedDeclarationsAssignment_2
            {
            pushFollow(FOLLOW_6);
            rule__Model__FixedDeclarationsAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getFixedDeclarationsAssignment_2()); 

            }

            // InternalMethodName.g:1001:2: ( ( rule__Model__FixedDeclarationsAssignment_2 )* )
            // InternalMethodName.g:1002:3: ( rule__Model__FixedDeclarationsAssignment_2 )*
            {
             before(grammarAccess.getModelAccess().getFixedDeclarationsAssignment_2()); 
            // InternalMethodName.g:1003:3: ( rule__Model__FixedDeclarationsAssignment_2 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0>=18 && LA12_0<=22)) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalMethodName.g:1003:4: rule__Model__FixedDeclarationsAssignment_2
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__Model__FixedDeclarationsAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getFixedDeclarationsAssignment_2()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__2__Impl"


    // $ANTLR start "rule__Model__Group__3"
    // InternalMethodName.g:1012:1: rule__Model__Group__3 : rule__Model__Group__3__Impl rule__Model__Group__4 ;
    public final void rule__Model__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1016:1: ( rule__Model__Group__3__Impl rule__Model__Group__4 )
            // InternalMethodName.g:1017:2: rule__Model__Group__3__Impl rule__Model__Group__4
            {
            pushFollow(FOLLOW_7);
            rule__Model__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__3"


    // $ANTLR start "rule__Model__Group__3__Impl"
    // InternalMethodName.g:1024:1: rule__Model__Group__3__Impl : ( 'rules' ) ;
    public final void rule__Model__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1028:1: ( ( 'rules' ) )
            // InternalMethodName.g:1029:1: ( 'rules' )
            {
            // InternalMethodName.g:1029:1: ( 'rules' )
            // InternalMethodName.g:1030:2: 'rules'
            {
             before(grammarAccess.getModelAccess().getRulesKeyword_3()); 
            match(input,72,FOLLOW_2); 
             after(grammarAccess.getModelAccess().getRulesKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__3__Impl"


    // $ANTLR start "rule__Model__Group__4"
    // InternalMethodName.g:1039:1: rule__Model__Group__4 : rule__Model__Group__4__Impl rule__Model__Group__5 ;
    public final void rule__Model__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1043:1: ( rule__Model__Group__4__Impl rule__Model__Group__5 )
            // InternalMethodName.g:1044:2: rule__Model__Group__4__Impl rule__Model__Group__5
            {
            pushFollow(FOLLOW_7);
            rule__Model__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__4"


    // $ANTLR start "rule__Model__Group__4__Impl"
    // InternalMethodName.g:1051:1: rule__Model__Group__4__Impl : ( ( rule__Model__RulesAssignment_4 )* ) ;
    public final void rule__Model__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1055:1: ( ( ( rule__Model__RulesAssignment_4 )* ) )
            // InternalMethodName.g:1056:1: ( ( rule__Model__RulesAssignment_4 )* )
            {
            // InternalMethodName.g:1056:1: ( ( rule__Model__RulesAssignment_4 )* )
            // InternalMethodName.g:1057:2: ( rule__Model__RulesAssignment_4 )*
            {
             before(grammarAccess.getModelAccess().getRulesAssignment_4()); 
            // InternalMethodName.g:1058:2: ( rule__Model__RulesAssignment_4 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==75) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalMethodName.g:1058:3: rule__Model__RulesAssignment_4
            	    {
            	    pushFollow(FOLLOW_8);
            	    rule__Model__RulesAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getRulesAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__4__Impl"


    // $ANTLR start "rule__Model__Group__5"
    // InternalMethodName.g:1066:1: rule__Model__Group__5 : rule__Model__Group__5__Impl rule__Model__Group__6 ;
    public final void rule__Model__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1070:1: ( rule__Model__Group__5__Impl rule__Model__Group__6 )
            // InternalMethodName.g:1071:2: rule__Model__Group__5__Impl rule__Model__Group__6
            {
            pushFollow(FOLLOW_9);
            rule__Model__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__5"


    // $ANTLR start "rule__Model__Group__5__Impl"
    // InternalMethodName.g:1078:1: rule__Model__Group__5__Impl : ( 'cases' ) ;
    public final void rule__Model__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1082:1: ( ( 'cases' ) )
            // InternalMethodName.g:1083:1: ( 'cases' )
            {
            // InternalMethodName.g:1083:1: ( 'cases' )
            // InternalMethodName.g:1084:2: 'cases'
            {
             before(grammarAccess.getModelAccess().getCasesKeyword_5()); 
            match(input,73,FOLLOW_2); 
             after(grammarAccess.getModelAccess().getCasesKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__5__Impl"


    // $ANTLR start "rule__Model__Group__6"
    // InternalMethodName.g:1093:1: rule__Model__Group__6 : rule__Model__Group__6__Impl ;
    public final void rule__Model__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1097:1: ( rule__Model__Group__6__Impl )
            // InternalMethodName.g:1098:2: rule__Model__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Model__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__6"


    // $ANTLR start "rule__Model__Group__6__Impl"
    // InternalMethodName.g:1104:1: rule__Model__Group__6__Impl : ( ( rule__Model__ElementsAssignment_6 )* ) ;
    public final void rule__Model__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1108:1: ( ( ( rule__Model__ElementsAssignment_6 )* ) )
            // InternalMethodName.g:1109:1: ( ( rule__Model__ElementsAssignment_6 )* )
            {
            // InternalMethodName.g:1109:1: ( ( rule__Model__ElementsAssignment_6 )* )
            // InternalMethodName.g:1110:2: ( rule__Model__ElementsAssignment_6 )*
            {
             before(grammarAccess.getModelAccess().getElementsAssignment_6()); 
            // InternalMethodName.g:1111:2: ( rule__Model__ElementsAssignment_6 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==85) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalMethodName.g:1111:3: rule__Model__ElementsAssignment_6
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__Model__ElementsAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getElementsAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__6__Impl"


    // $ANTLR start "rule__Declaration__Group__0"
    // InternalMethodName.g:1120:1: rule__Declaration__Group__0 : rule__Declaration__Group__0__Impl rule__Declaration__Group__1 ;
    public final void rule__Declaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1124:1: ( rule__Declaration__Group__0__Impl rule__Declaration__Group__1 )
            // InternalMethodName.g:1125:2: rule__Declaration__Group__0__Impl rule__Declaration__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__Declaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__0"


    // $ANTLR start "rule__Declaration__Group__0__Impl"
    // InternalMethodName.g:1132:1: rule__Declaration__Group__0__Impl : ( ( rule__Declaration__NameAssignment_0 ) ) ;
    public final void rule__Declaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1136:1: ( ( ( rule__Declaration__NameAssignment_0 ) ) )
            // InternalMethodName.g:1137:1: ( ( rule__Declaration__NameAssignment_0 ) )
            {
            // InternalMethodName.g:1137:1: ( ( rule__Declaration__NameAssignment_0 ) )
            // InternalMethodName.g:1138:2: ( rule__Declaration__NameAssignment_0 )
            {
             before(grammarAccess.getDeclarationAccess().getNameAssignment_0()); 
            // InternalMethodName.g:1139:2: ( rule__Declaration__NameAssignment_0 )
            // InternalMethodName.g:1139:3: rule__Declaration__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Declaration__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getDeclarationAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__0__Impl"


    // $ANTLR start "rule__Declaration__Group__1"
    // InternalMethodName.g:1147:1: rule__Declaration__Group__1 : rule__Declaration__Group__1__Impl rule__Declaration__Group__2 ;
    public final void rule__Declaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1151:1: ( rule__Declaration__Group__1__Impl rule__Declaration__Group__2 )
            // InternalMethodName.g:1152:2: rule__Declaration__Group__1__Impl rule__Declaration__Group__2
            {
            pushFollow(FOLLOW_12);
            rule__Declaration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__1"


    // $ANTLR start "rule__Declaration__Group__1__Impl"
    // InternalMethodName.g:1159:1: rule__Declaration__Group__1__Impl : ( '<->' ) ;
    public final void rule__Declaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1163:1: ( ( '<->' ) )
            // InternalMethodName.g:1164:1: ( '<->' )
            {
            // InternalMethodName.g:1164:1: ( '<->' )
            // InternalMethodName.g:1165:2: '<->'
            {
             before(grammarAccess.getDeclarationAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_1()); 
            match(input,74,FOLLOW_2); 
             after(grammarAccess.getDeclarationAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__1__Impl"


    // $ANTLR start "rule__Declaration__Group__2"
    // InternalMethodName.g:1174:1: rule__Declaration__Group__2 : rule__Declaration__Group__2__Impl ;
    public final void rule__Declaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1178:1: ( rule__Declaration__Group__2__Impl )
            // InternalMethodName.g:1179:2: rule__Declaration__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Declaration__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__2"


    // $ANTLR start "rule__Declaration__Group__2__Impl"
    // InternalMethodName.g:1185:1: rule__Declaration__Group__2__Impl : ( ( rule__Declaration__PathAssignment_2 ) ) ;
    public final void rule__Declaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1189:1: ( ( ( rule__Declaration__PathAssignment_2 ) ) )
            // InternalMethodName.g:1190:1: ( ( rule__Declaration__PathAssignment_2 ) )
            {
            // InternalMethodName.g:1190:1: ( ( rule__Declaration__PathAssignment_2 ) )
            // InternalMethodName.g:1191:2: ( rule__Declaration__PathAssignment_2 )
            {
             before(grammarAccess.getDeclarationAccess().getPathAssignment_2()); 
            // InternalMethodName.g:1192:2: ( rule__Declaration__PathAssignment_2 )
            // InternalMethodName.g:1192:3: rule__Declaration__PathAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Declaration__PathAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getDeclarationAccess().getPathAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__2__Impl"


    // $ANTLR start "rule__FixedDeclaration__Group__0"
    // InternalMethodName.g:1201:1: rule__FixedDeclaration__Group__0 : rule__FixedDeclaration__Group__0__Impl rule__FixedDeclaration__Group__1 ;
    public final void rule__FixedDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1205:1: ( rule__FixedDeclaration__Group__0__Impl rule__FixedDeclaration__Group__1 )
            // InternalMethodName.g:1206:2: rule__FixedDeclaration__Group__0__Impl rule__FixedDeclaration__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__FixedDeclaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FixedDeclaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedDeclaration__Group__0"


    // $ANTLR start "rule__FixedDeclaration__Group__0__Impl"
    // InternalMethodName.g:1213:1: rule__FixedDeclaration__Group__0__Impl : ( ( rule__FixedDeclaration__KindAssignment_0 ) ) ;
    public final void rule__FixedDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1217:1: ( ( ( rule__FixedDeclaration__KindAssignment_0 ) ) )
            // InternalMethodName.g:1218:1: ( ( rule__FixedDeclaration__KindAssignment_0 ) )
            {
            // InternalMethodName.g:1218:1: ( ( rule__FixedDeclaration__KindAssignment_0 ) )
            // InternalMethodName.g:1219:2: ( rule__FixedDeclaration__KindAssignment_0 )
            {
             before(grammarAccess.getFixedDeclarationAccess().getKindAssignment_0()); 
            // InternalMethodName.g:1220:2: ( rule__FixedDeclaration__KindAssignment_0 )
            // InternalMethodName.g:1220:3: rule__FixedDeclaration__KindAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__FixedDeclaration__KindAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getFixedDeclarationAccess().getKindAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedDeclaration__Group__0__Impl"


    // $ANTLR start "rule__FixedDeclaration__Group__1"
    // InternalMethodName.g:1228:1: rule__FixedDeclaration__Group__1 : rule__FixedDeclaration__Group__1__Impl rule__FixedDeclaration__Group__2 ;
    public final void rule__FixedDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1232:1: ( rule__FixedDeclaration__Group__1__Impl rule__FixedDeclaration__Group__2 )
            // InternalMethodName.g:1233:2: rule__FixedDeclaration__Group__1__Impl rule__FixedDeclaration__Group__2
            {
            pushFollow(FOLLOW_12);
            rule__FixedDeclaration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FixedDeclaration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedDeclaration__Group__1"


    // $ANTLR start "rule__FixedDeclaration__Group__1__Impl"
    // InternalMethodName.g:1240:1: rule__FixedDeclaration__Group__1__Impl : ( '<->' ) ;
    public final void rule__FixedDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1244:1: ( ( '<->' ) )
            // InternalMethodName.g:1245:1: ( '<->' )
            {
            // InternalMethodName.g:1245:1: ( '<->' )
            // InternalMethodName.g:1246:2: '<->'
            {
             before(grammarAccess.getFixedDeclarationAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_1()); 
            match(input,74,FOLLOW_2); 
             after(grammarAccess.getFixedDeclarationAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedDeclaration__Group__1__Impl"


    // $ANTLR start "rule__FixedDeclaration__Group__2"
    // InternalMethodName.g:1255:1: rule__FixedDeclaration__Group__2 : rule__FixedDeclaration__Group__2__Impl ;
    public final void rule__FixedDeclaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1259:1: ( rule__FixedDeclaration__Group__2__Impl )
            // InternalMethodName.g:1260:2: rule__FixedDeclaration__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FixedDeclaration__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedDeclaration__Group__2"


    // $ANTLR start "rule__FixedDeclaration__Group__2__Impl"
    // InternalMethodName.g:1266:1: rule__FixedDeclaration__Group__2__Impl : ( ( rule__FixedDeclaration__PathAssignment_2 ) ) ;
    public final void rule__FixedDeclaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1270:1: ( ( ( rule__FixedDeclaration__PathAssignment_2 ) ) )
            // InternalMethodName.g:1271:1: ( ( rule__FixedDeclaration__PathAssignment_2 ) )
            {
            // InternalMethodName.g:1271:1: ( ( rule__FixedDeclaration__PathAssignment_2 ) )
            // InternalMethodName.g:1272:2: ( rule__FixedDeclaration__PathAssignment_2 )
            {
             before(grammarAccess.getFixedDeclarationAccess().getPathAssignment_2()); 
            // InternalMethodName.g:1273:2: ( rule__FixedDeclaration__PathAssignment_2 )
            // InternalMethodName.g:1273:3: rule__FixedDeclaration__PathAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__FixedDeclaration__PathAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getFixedDeclarationAccess().getPathAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedDeclaration__Group__2__Impl"


    // $ANTLR start "rule__Rule__Group_0__0"
    // InternalMethodName.g:1282:1: rule__Rule__Group_0__0 : rule__Rule__Group_0__0__Impl rule__Rule__Group_0__1 ;
    public final void rule__Rule__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1286:1: ( rule__Rule__Group_0__0__Impl rule__Rule__Group_0__1 )
            // InternalMethodName.g:1287:2: rule__Rule__Group_0__0__Impl rule__Rule__Group_0__1
            {
            pushFollow(FOLLOW_13);
            rule__Rule__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__0"


    // $ANTLR start "rule__Rule__Group_0__0__Impl"
    // InternalMethodName.g:1294:1: rule__Rule__Group_0__0__Impl : ( 'def' ) ;
    public final void rule__Rule__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1298:1: ( ( 'def' ) )
            // InternalMethodName.g:1299:1: ( 'def' )
            {
            // InternalMethodName.g:1299:1: ( 'def' )
            // InternalMethodName.g:1300:2: 'def'
            {
             before(grammarAccess.getRuleAccess().getDefKeyword_0_0()); 
            match(input,75,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getDefKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__0__Impl"


    // $ANTLR start "rule__Rule__Group_0__1"
    // InternalMethodName.g:1309:1: rule__Rule__Group_0__1 : rule__Rule__Group_0__1__Impl rule__Rule__Group_0__2 ;
    public final void rule__Rule__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1313:1: ( rule__Rule__Group_0__1__Impl rule__Rule__Group_0__2 )
            // InternalMethodName.g:1314:2: rule__Rule__Group_0__1__Impl rule__Rule__Group_0__2
            {
            pushFollow(FOLLOW_14);
            rule__Rule__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__1"


    // $ANTLR start "rule__Rule__Group_0__1__Impl"
    // InternalMethodName.g:1321:1: rule__Rule__Group_0__1__Impl : ( ( rule__Rule__NameAssignment_0_1 ) ) ;
    public final void rule__Rule__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1325:1: ( ( ( rule__Rule__NameAssignment_0_1 ) ) )
            // InternalMethodName.g:1326:1: ( ( rule__Rule__NameAssignment_0_1 ) )
            {
            // InternalMethodName.g:1326:1: ( ( rule__Rule__NameAssignment_0_1 ) )
            // InternalMethodName.g:1327:2: ( rule__Rule__NameAssignment_0_1 )
            {
             before(grammarAccess.getRuleAccess().getNameAssignment_0_1()); 
            // InternalMethodName.g:1328:2: ( rule__Rule__NameAssignment_0_1 )
            // InternalMethodName.g:1328:3: rule__Rule__NameAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Rule__NameAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getRuleAccess().getNameAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__1__Impl"


    // $ANTLR start "rule__Rule__Group_0__2"
    // InternalMethodName.g:1336:1: rule__Rule__Group_0__2 : rule__Rule__Group_0__2__Impl rule__Rule__Group_0__3 ;
    public final void rule__Rule__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1340:1: ( rule__Rule__Group_0__2__Impl rule__Rule__Group_0__3 )
            // InternalMethodName.g:1341:2: rule__Rule__Group_0__2__Impl rule__Rule__Group_0__3
            {
            pushFollow(FOLLOW_3);
            rule__Rule__Group_0__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group_0__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__2"


    // $ANTLR start "rule__Rule__Group_0__2__Impl"
    // InternalMethodName.g:1348:1: rule__Rule__Group_0__2__Impl : ( 'for' ) ;
    public final void rule__Rule__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1352:1: ( ( 'for' ) )
            // InternalMethodName.g:1353:1: ( 'for' )
            {
            // InternalMethodName.g:1353:1: ( 'for' )
            // InternalMethodName.g:1354:2: 'for'
            {
             before(grammarAccess.getRuleAccess().getForKeyword_0_2()); 
            match(input,76,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getForKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__2__Impl"


    // $ANTLR start "rule__Rule__Group_0__3"
    // InternalMethodName.g:1363:1: rule__Rule__Group_0__3 : rule__Rule__Group_0__3__Impl rule__Rule__Group_0__4 ;
    public final void rule__Rule__Group_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1367:1: ( rule__Rule__Group_0__3__Impl rule__Rule__Group_0__4 )
            // InternalMethodName.g:1368:2: rule__Rule__Group_0__3__Impl rule__Rule__Group_0__4
            {
            pushFollow(FOLLOW_15);
            rule__Rule__Group_0__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group_0__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__3"


    // $ANTLR start "rule__Rule__Group_0__3__Impl"
    // InternalMethodName.g:1375:1: rule__Rule__Group_0__3__Impl : ( ( rule__Rule__KindAssignment_0_3 ) ) ;
    public final void rule__Rule__Group_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1379:1: ( ( ( rule__Rule__KindAssignment_0_3 ) ) )
            // InternalMethodName.g:1380:1: ( ( rule__Rule__KindAssignment_0_3 ) )
            {
            // InternalMethodName.g:1380:1: ( ( rule__Rule__KindAssignment_0_3 ) )
            // InternalMethodName.g:1381:2: ( rule__Rule__KindAssignment_0_3 )
            {
             before(grammarAccess.getRuleAccess().getKindAssignment_0_3()); 
            // InternalMethodName.g:1382:2: ( rule__Rule__KindAssignment_0_3 )
            // InternalMethodName.g:1382:3: rule__Rule__KindAssignment_0_3
            {
            pushFollow(FOLLOW_2);
            rule__Rule__KindAssignment_0_3();

            state._fsp--;


            }

             after(grammarAccess.getRuleAccess().getKindAssignment_0_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__3__Impl"


    // $ANTLR start "rule__Rule__Group_0__4"
    // InternalMethodName.g:1390:1: rule__Rule__Group_0__4 : rule__Rule__Group_0__4__Impl rule__Rule__Group_0__5 ;
    public final void rule__Rule__Group_0__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1394:1: ( rule__Rule__Group_0__4__Impl rule__Rule__Group_0__5 )
            // InternalMethodName.g:1395:2: rule__Rule__Group_0__4__Impl rule__Rule__Group_0__5
            {
            pushFollow(FOLLOW_16);
            rule__Rule__Group_0__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group_0__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__4"


    // $ANTLR start "rule__Rule__Group_0__4__Impl"
    // InternalMethodName.g:1402:1: rule__Rule__Group_0__4__Impl : ( '{' ) ;
    public final void rule__Rule__Group_0__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1406:1: ( ( '{' ) )
            // InternalMethodName.g:1407:1: ( '{' )
            {
            // InternalMethodName.g:1407:1: ( '{' )
            // InternalMethodName.g:1408:2: '{'
            {
             before(grammarAccess.getRuleAccess().getLeftCurlyBracketKeyword_0_4()); 
            match(input,77,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getLeftCurlyBracketKeyword_0_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__4__Impl"


    // $ANTLR start "rule__Rule__Group_0__5"
    // InternalMethodName.g:1417:1: rule__Rule__Group_0__5 : rule__Rule__Group_0__5__Impl rule__Rule__Group_0__6 ;
    public final void rule__Rule__Group_0__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1421:1: ( rule__Rule__Group_0__5__Impl rule__Rule__Group_0__6 )
            // InternalMethodName.g:1422:2: rule__Rule__Group_0__5__Impl rule__Rule__Group_0__6
            {
            pushFollow(FOLLOW_13);
            rule__Rule__Group_0__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group_0__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__5"


    // $ANTLR start "rule__Rule__Group_0__5__Impl"
    // InternalMethodName.g:1429:1: rule__Rule__Group_0__5__Impl : ( 'filter' ) ;
    public final void rule__Rule__Group_0__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1433:1: ( ( 'filter' ) )
            // InternalMethodName.g:1434:1: ( 'filter' )
            {
            // InternalMethodName.g:1434:1: ( 'filter' )
            // InternalMethodName.g:1435:2: 'filter'
            {
             before(grammarAccess.getRuleAccess().getFilterKeyword_0_5()); 
            match(input,78,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getFilterKeyword_0_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__5__Impl"


    // $ANTLR start "rule__Rule__Group_0__6"
    // InternalMethodName.g:1444:1: rule__Rule__Group_0__6 : rule__Rule__Group_0__6__Impl rule__Rule__Group_0__7 ;
    public final void rule__Rule__Group_0__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1448:1: ( rule__Rule__Group_0__6__Impl rule__Rule__Group_0__7 )
            // InternalMethodName.g:1449:2: rule__Rule__Group_0__6__Impl rule__Rule__Group_0__7
            {
            pushFollow(FOLLOW_17);
            rule__Rule__Group_0__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group_0__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__6"


    // $ANTLR start "rule__Rule__Group_0__6__Impl"
    // InternalMethodName.g:1456:1: rule__Rule__Group_0__6__Impl : ( ( rule__Rule__DeclsAssignment_0_6 ) ) ;
    public final void rule__Rule__Group_0__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1460:1: ( ( ( rule__Rule__DeclsAssignment_0_6 ) ) )
            // InternalMethodName.g:1461:1: ( ( rule__Rule__DeclsAssignment_0_6 ) )
            {
            // InternalMethodName.g:1461:1: ( ( rule__Rule__DeclsAssignment_0_6 ) )
            // InternalMethodName.g:1462:2: ( rule__Rule__DeclsAssignment_0_6 )
            {
             before(grammarAccess.getRuleAccess().getDeclsAssignment_0_6()); 
            // InternalMethodName.g:1463:2: ( rule__Rule__DeclsAssignment_0_6 )
            // InternalMethodName.g:1463:3: rule__Rule__DeclsAssignment_0_6
            {
            pushFollow(FOLLOW_2);
            rule__Rule__DeclsAssignment_0_6();

            state._fsp--;


            }

             after(grammarAccess.getRuleAccess().getDeclsAssignment_0_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__6__Impl"


    // $ANTLR start "rule__Rule__Group_0__7"
    // InternalMethodName.g:1471:1: rule__Rule__Group_0__7 : rule__Rule__Group_0__7__Impl rule__Rule__Group_0__8 ;
    public final void rule__Rule__Group_0__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1475:1: ( rule__Rule__Group_0__7__Impl rule__Rule__Group_0__8 )
            // InternalMethodName.g:1476:2: rule__Rule__Group_0__7__Impl rule__Rule__Group_0__8
            {
            pushFollow(FOLLOW_17);
            rule__Rule__Group_0__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group_0__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__7"


    // $ANTLR start "rule__Rule__Group_0__7__Impl"
    // InternalMethodName.g:1483:1: rule__Rule__Group_0__7__Impl : ( ( rule__Rule__Group_0_7__0 )* ) ;
    public final void rule__Rule__Group_0__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1487:1: ( ( ( rule__Rule__Group_0_7__0 )* ) )
            // InternalMethodName.g:1488:1: ( ( rule__Rule__Group_0_7__0 )* )
            {
            // InternalMethodName.g:1488:1: ( ( rule__Rule__Group_0_7__0 )* )
            // InternalMethodName.g:1489:2: ( rule__Rule__Group_0_7__0 )*
            {
             before(grammarAccess.getRuleAccess().getGroup_0_7()); 
            // InternalMethodName.g:1490:2: ( rule__Rule__Group_0_7__0 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==83) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalMethodName.g:1490:3: rule__Rule__Group_0_7__0
            	    {
            	    pushFollow(FOLLOW_18);
            	    rule__Rule__Group_0_7__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getRuleAccess().getGroup_0_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__7__Impl"


    // $ANTLR start "rule__Rule__Group_0__8"
    // InternalMethodName.g:1498:1: rule__Rule__Group_0__8 : rule__Rule__Group_0__8__Impl rule__Rule__Group_0__9 ;
    public final void rule__Rule__Group_0__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1502:1: ( rule__Rule__Group_0__8__Impl rule__Rule__Group_0__9 )
            // InternalMethodName.g:1503:2: rule__Rule__Group_0__8__Impl rule__Rule__Group_0__9
            {
            pushFollow(FOLLOW_19);
            rule__Rule__Group_0__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group_0__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__8"


    // $ANTLR start "rule__Rule__Group_0__8__Impl"
    // InternalMethodName.g:1510:1: rule__Rule__Group_0__8__Impl : ( 'such' ) ;
    public final void rule__Rule__Group_0__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1514:1: ( ( 'such' ) )
            // InternalMethodName.g:1515:1: ( 'such' )
            {
            // InternalMethodName.g:1515:1: ( 'such' )
            // InternalMethodName.g:1516:2: 'such'
            {
             before(grammarAccess.getRuleAccess().getSuchKeyword_0_8()); 
            match(input,79,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getSuchKeyword_0_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__8__Impl"


    // $ANTLR start "rule__Rule__Group_0__9"
    // InternalMethodName.g:1525:1: rule__Rule__Group_0__9 : rule__Rule__Group_0__9__Impl rule__Rule__Group_0__10 ;
    public final void rule__Rule__Group_0__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1529:1: ( rule__Rule__Group_0__9__Impl rule__Rule__Group_0__10 )
            // InternalMethodName.g:1530:2: rule__Rule__Group_0__9__Impl rule__Rule__Group_0__10
            {
            pushFollow(FOLLOW_20);
            rule__Rule__Group_0__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group_0__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__9"


    // $ANTLR start "rule__Rule__Group_0__9__Impl"
    // InternalMethodName.g:1537:1: rule__Rule__Group_0__9__Impl : ( 'that' ) ;
    public final void rule__Rule__Group_0__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1541:1: ( ( 'that' ) )
            // InternalMethodName.g:1542:1: ( 'that' )
            {
            // InternalMethodName.g:1542:1: ( 'that' )
            // InternalMethodName.g:1543:2: 'that'
            {
             before(grammarAccess.getRuleAccess().getThatKeyword_0_9()); 
            match(input,80,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getThatKeyword_0_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__9__Impl"


    // $ANTLR start "rule__Rule__Group_0__10"
    // InternalMethodName.g:1552:1: rule__Rule__Group_0__10 : rule__Rule__Group_0__10__Impl rule__Rule__Group_0__11 ;
    public final void rule__Rule__Group_0__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1556:1: ( rule__Rule__Group_0__10__Impl rule__Rule__Group_0__11 )
            // InternalMethodName.g:1557:2: rule__Rule__Group_0__10__Impl rule__Rule__Group_0__11
            {
            pushFollow(FOLLOW_21);
            rule__Rule__Group_0__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group_0__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__10"


    // $ANTLR start "rule__Rule__Group_0__10__Impl"
    // InternalMethodName.g:1564:1: rule__Rule__Group_0__10__Impl : ( 'size' ) ;
    public final void rule__Rule__Group_0__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1568:1: ( ( 'size' ) )
            // InternalMethodName.g:1569:1: ( 'size' )
            {
            // InternalMethodName.g:1569:1: ( 'size' )
            // InternalMethodName.g:1570:2: 'size'
            {
             before(grammarAccess.getRuleAccess().getSizeKeyword_0_10()); 
            match(input,81,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getSizeKeyword_0_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__10__Impl"


    // $ANTLR start "rule__Rule__Group_0__11"
    // InternalMethodName.g:1579:1: rule__Rule__Group_0__11 : rule__Rule__Group_0__11__Impl rule__Rule__Group_0__12 ;
    public final void rule__Rule__Group_0__11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1583:1: ( rule__Rule__Group_0__11__Impl rule__Rule__Group_0__12 )
            // InternalMethodName.g:1584:2: rule__Rule__Group_0__11__Impl rule__Rule__Group_0__12
            {
            pushFollow(FOLLOW_22);
            rule__Rule__Group_0__11__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group_0__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__11"


    // $ANTLR start "rule__Rule__Group_0__11__Impl"
    // InternalMethodName.g:1591:1: rule__Rule__Group_0__11__Impl : ( ( rule__Rule__CompOpAssignment_0_11 ) ) ;
    public final void rule__Rule__Group_0__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1595:1: ( ( ( rule__Rule__CompOpAssignment_0_11 ) ) )
            // InternalMethodName.g:1596:1: ( ( rule__Rule__CompOpAssignment_0_11 ) )
            {
            // InternalMethodName.g:1596:1: ( ( rule__Rule__CompOpAssignment_0_11 ) )
            // InternalMethodName.g:1597:2: ( rule__Rule__CompOpAssignment_0_11 )
            {
             before(grammarAccess.getRuleAccess().getCompOpAssignment_0_11()); 
            // InternalMethodName.g:1598:2: ( rule__Rule__CompOpAssignment_0_11 )
            // InternalMethodName.g:1598:3: rule__Rule__CompOpAssignment_0_11
            {
            pushFollow(FOLLOW_2);
            rule__Rule__CompOpAssignment_0_11();

            state._fsp--;


            }

             after(grammarAccess.getRuleAccess().getCompOpAssignment_0_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__11__Impl"


    // $ANTLR start "rule__Rule__Group_0__12"
    // InternalMethodName.g:1606:1: rule__Rule__Group_0__12 : rule__Rule__Group_0__12__Impl rule__Rule__Group_0__13 ;
    public final void rule__Rule__Group_0__12() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1610:1: ( rule__Rule__Group_0__12__Impl rule__Rule__Group_0__13 )
            // InternalMethodName.g:1611:2: rule__Rule__Group_0__12__Impl rule__Rule__Group_0__13
            {
            pushFollow(FOLLOW_23);
            rule__Rule__Group_0__12__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group_0__13();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__12"


    // $ANTLR start "rule__Rule__Group_0__12__Impl"
    // InternalMethodName.g:1618:1: rule__Rule__Group_0__12__Impl : ( ( rule__Rule__CompValueAssignment_0_12 ) ) ;
    public final void rule__Rule__Group_0__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1622:1: ( ( ( rule__Rule__CompValueAssignment_0_12 ) ) )
            // InternalMethodName.g:1623:1: ( ( rule__Rule__CompValueAssignment_0_12 ) )
            {
            // InternalMethodName.g:1623:1: ( ( rule__Rule__CompValueAssignment_0_12 ) )
            // InternalMethodName.g:1624:2: ( rule__Rule__CompValueAssignment_0_12 )
            {
             before(grammarAccess.getRuleAccess().getCompValueAssignment_0_12()); 
            // InternalMethodName.g:1625:2: ( rule__Rule__CompValueAssignment_0_12 )
            // InternalMethodName.g:1625:3: rule__Rule__CompValueAssignment_0_12
            {
            pushFollow(FOLLOW_2);
            rule__Rule__CompValueAssignment_0_12();

            state._fsp--;


            }

             after(grammarAccess.getRuleAccess().getCompValueAssignment_0_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__12__Impl"


    // $ANTLR start "rule__Rule__Group_0__13"
    // InternalMethodName.g:1633:1: rule__Rule__Group_0__13 : rule__Rule__Group_0__13__Impl ;
    public final void rule__Rule__Group_0__13() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1637:1: ( rule__Rule__Group_0__13__Impl )
            // InternalMethodName.g:1638:2: rule__Rule__Group_0__13__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Rule__Group_0__13__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__13"


    // $ANTLR start "rule__Rule__Group_0__13__Impl"
    // InternalMethodName.g:1644:1: rule__Rule__Group_0__13__Impl : ( '}' ) ;
    public final void rule__Rule__Group_0__13__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1648:1: ( ( '}' ) )
            // InternalMethodName.g:1649:1: ( '}' )
            {
            // InternalMethodName.g:1649:1: ( '}' )
            // InternalMethodName.g:1650:2: '}'
            {
             before(grammarAccess.getRuleAccess().getRightCurlyBracketKeyword_0_13()); 
            match(input,82,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getRightCurlyBracketKeyword_0_13()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0__13__Impl"


    // $ANTLR start "rule__Rule__Group_0_7__0"
    // InternalMethodName.g:1660:1: rule__Rule__Group_0_7__0 : rule__Rule__Group_0_7__0__Impl rule__Rule__Group_0_7__1 ;
    public final void rule__Rule__Group_0_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1664:1: ( rule__Rule__Group_0_7__0__Impl rule__Rule__Group_0_7__1 )
            // InternalMethodName.g:1665:2: rule__Rule__Group_0_7__0__Impl rule__Rule__Group_0_7__1
            {
            pushFollow(FOLLOW_13);
            rule__Rule__Group_0_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group_0_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0_7__0"


    // $ANTLR start "rule__Rule__Group_0_7__0__Impl"
    // InternalMethodName.g:1672:1: rule__Rule__Group_0_7__0__Impl : ( '||' ) ;
    public final void rule__Rule__Group_0_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1676:1: ( ( '||' ) )
            // InternalMethodName.g:1677:1: ( '||' )
            {
            // InternalMethodName.g:1677:1: ( '||' )
            // InternalMethodName.g:1678:2: '||'
            {
             before(grammarAccess.getRuleAccess().getVerticalLineVerticalLineKeyword_0_7_0()); 
            match(input,83,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getVerticalLineVerticalLineKeyword_0_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0_7__0__Impl"


    // $ANTLR start "rule__Rule__Group_0_7__1"
    // InternalMethodName.g:1687:1: rule__Rule__Group_0_7__1 : rule__Rule__Group_0_7__1__Impl ;
    public final void rule__Rule__Group_0_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1691:1: ( rule__Rule__Group_0_7__1__Impl )
            // InternalMethodName.g:1692:2: rule__Rule__Group_0_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Rule__Group_0_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0_7__1"


    // $ANTLR start "rule__Rule__Group_0_7__1__Impl"
    // InternalMethodName.g:1698:1: rule__Rule__Group_0_7__1__Impl : ( ( rule__Rule__DeclsAssignment_0_7_1 ) ) ;
    public final void rule__Rule__Group_0_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1702:1: ( ( ( rule__Rule__DeclsAssignment_0_7_1 ) ) )
            // InternalMethodName.g:1703:1: ( ( rule__Rule__DeclsAssignment_0_7_1 ) )
            {
            // InternalMethodName.g:1703:1: ( ( rule__Rule__DeclsAssignment_0_7_1 ) )
            // InternalMethodName.g:1704:2: ( rule__Rule__DeclsAssignment_0_7_1 )
            {
             before(grammarAccess.getRuleAccess().getDeclsAssignment_0_7_1()); 
            // InternalMethodName.g:1705:2: ( rule__Rule__DeclsAssignment_0_7_1 )
            // InternalMethodName.g:1705:3: rule__Rule__DeclsAssignment_0_7_1
            {
            pushFollow(FOLLOW_2);
            rule__Rule__DeclsAssignment_0_7_1();

            state._fsp--;


            }

             after(grammarAccess.getRuleAccess().getDeclsAssignment_0_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_0_7__1__Impl"


    // $ANTLR start "rule__Rule__Group_1__0"
    // InternalMethodName.g:1714:1: rule__Rule__Group_1__0 : rule__Rule__Group_1__0__Impl rule__Rule__Group_1__1 ;
    public final void rule__Rule__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1718:1: ( rule__Rule__Group_1__0__Impl rule__Rule__Group_1__1 )
            // InternalMethodName.g:1719:2: rule__Rule__Group_1__0__Impl rule__Rule__Group_1__1
            {
            pushFollow(FOLLOW_24);
            rule__Rule__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_1__0"


    // $ANTLR start "rule__Rule__Group_1__0__Impl"
    // InternalMethodName.g:1726:1: rule__Rule__Group_1__0__Impl : ( 'def' ) ;
    public final void rule__Rule__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1730:1: ( ( 'def' ) )
            // InternalMethodName.g:1731:1: ( 'def' )
            {
            // InternalMethodName.g:1731:1: ( 'def' )
            // InternalMethodName.g:1732:2: 'def'
            {
             before(grammarAccess.getRuleAccess().getDefKeyword_1_0()); 
            match(input,75,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getDefKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_1__0__Impl"


    // $ANTLR start "rule__Rule__Group_1__1"
    // InternalMethodName.g:1741:1: rule__Rule__Group_1__1 : rule__Rule__Group_1__1__Impl rule__Rule__Group_1__2 ;
    public final void rule__Rule__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1745:1: ( rule__Rule__Group_1__1__Impl rule__Rule__Group_1__2 )
            // InternalMethodName.g:1746:2: rule__Rule__Group_1__1__Impl rule__Rule__Group_1__2
            {
            pushFollow(FOLLOW_13);
            rule__Rule__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_1__1"


    // $ANTLR start "rule__Rule__Group_1__1__Impl"
    // InternalMethodName.g:1753:1: rule__Rule__Group_1__1__Impl : ( 'declare' ) ;
    public final void rule__Rule__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1757:1: ( ( 'declare' ) )
            // InternalMethodName.g:1758:1: ( 'declare' )
            {
            // InternalMethodName.g:1758:1: ( 'declare' )
            // InternalMethodName.g:1759:2: 'declare'
            {
             before(grammarAccess.getRuleAccess().getDeclareKeyword_1_1()); 
            match(input,84,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getDeclareKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_1__1__Impl"


    // $ANTLR start "rule__Rule__Group_1__2"
    // InternalMethodName.g:1768:1: rule__Rule__Group_1__2 : rule__Rule__Group_1__2__Impl ;
    public final void rule__Rule__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1772:1: ( rule__Rule__Group_1__2__Impl )
            // InternalMethodName.g:1773:2: rule__Rule__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Rule__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_1__2"


    // $ANTLR start "rule__Rule__Group_1__2__Impl"
    // InternalMethodName.g:1779:1: rule__Rule__Group_1__2__Impl : ( ( rule__Rule__NameAssignment_1_2 ) ) ;
    public final void rule__Rule__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1783:1: ( ( ( rule__Rule__NameAssignment_1_2 ) ) )
            // InternalMethodName.g:1784:1: ( ( rule__Rule__NameAssignment_1_2 ) )
            {
            // InternalMethodName.g:1784:1: ( ( rule__Rule__NameAssignment_1_2 ) )
            // InternalMethodName.g:1785:2: ( rule__Rule__NameAssignment_1_2 )
            {
             before(grammarAccess.getRuleAccess().getNameAssignment_1_2()); 
            // InternalMethodName.g:1786:2: ( rule__Rule__NameAssignment_1_2 )
            // InternalMethodName.g:1786:3: rule__Rule__NameAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Rule__NameAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getRuleAccess().getNameAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group_1__2__Impl"


    // $ANTLR start "rule__Case__Group__0"
    // InternalMethodName.g:1795:1: rule__Case__Group__0 : rule__Case__Group__0__Impl rule__Case__Group__1 ;
    public final void rule__Case__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1799:1: ( rule__Case__Group__0__Impl rule__Case__Group__1 )
            // InternalMethodName.g:1800:2: rule__Case__Group__0__Impl rule__Case__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__Case__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Case__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__0"


    // $ANTLR start "rule__Case__Group__0__Impl"
    // InternalMethodName.g:1807:1: rule__Case__Group__0__Impl : ( 'case' ) ;
    public final void rule__Case__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1811:1: ( ( 'case' ) )
            // InternalMethodName.g:1812:1: ( 'case' )
            {
            // InternalMethodName.g:1812:1: ( 'case' )
            // InternalMethodName.g:1813:2: 'case'
            {
             before(grammarAccess.getCaseAccess().getCaseKeyword_0()); 
            match(input,85,FOLLOW_2); 
             after(grammarAccess.getCaseAccess().getCaseKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__0__Impl"


    // $ANTLR start "rule__Case__Group__1"
    // InternalMethodName.g:1822:1: rule__Case__Group__1 : rule__Case__Group__1__Impl rule__Case__Group__2 ;
    public final void rule__Case__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1826:1: ( rule__Case__Group__1__Impl rule__Case__Group__2 )
            // InternalMethodName.g:1827:2: rule__Case__Group__1__Impl rule__Case__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__Case__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Case__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__1"


    // $ANTLR start "rule__Case__Group__1__Impl"
    // InternalMethodName.g:1834:1: rule__Case__Group__1__Impl : ( 'for' ) ;
    public final void rule__Case__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1838:1: ( ( 'for' ) )
            // InternalMethodName.g:1839:1: ( 'for' )
            {
            // InternalMethodName.g:1839:1: ( 'for' )
            // InternalMethodName.g:1840:2: 'for'
            {
             before(grammarAccess.getCaseAccess().getForKeyword_1()); 
            match(input,76,FOLLOW_2); 
             after(grammarAccess.getCaseAccess().getForKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__1__Impl"


    // $ANTLR start "rule__Case__Group__2"
    // InternalMethodName.g:1849:1: rule__Case__Group__2 : rule__Case__Group__2__Impl rule__Case__Group__3 ;
    public final void rule__Case__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1853:1: ( rule__Case__Group__2__Impl rule__Case__Group__3 )
            // InternalMethodName.g:1854:2: rule__Case__Group__2__Impl rule__Case__Group__3
            {
            pushFollow(FOLLOW_25);
            rule__Case__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Case__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__2"


    // $ANTLR start "rule__Case__Group__2__Impl"
    // InternalMethodName.g:1861:1: rule__Case__Group__2__Impl : ( ( rule__Case__KindAssignment_2 ) ) ;
    public final void rule__Case__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1865:1: ( ( ( rule__Case__KindAssignment_2 ) ) )
            // InternalMethodName.g:1866:1: ( ( rule__Case__KindAssignment_2 ) )
            {
            // InternalMethodName.g:1866:1: ( ( rule__Case__KindAssignment_2 ) )
            // InternalMethodName.g:1867:2: ( rule__Case__KindAssignment_2 )
            {
             before(grammarAccess.getCaseAccess().getKindAssignment_2()); 
            // InternalMethodName.g:1868:2: ( rule__Case__KindAssignment_2 )
            // InternalMethodName.g:1868:3: rule__Case__KindAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Case__KindAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCaseAccess().getKindAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__2__Impl"


    // $ANTLR start "rule__Case__Group__3"
    // InternalMethodName.g:1876:1: rule__Case__Group__3 : rule__Case__Group__3__Impl rule__Case__Group__4 ;
    public final void rule__Case__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1880:1: ( rule__Case__Group__3__Impl rule__Case__Group__4 )
            // InternalMethodName.g:1881:2: rule__Case__Group__3__Impl rule__Case__Group__4
            {
            pushFollow(FOLLOW_15);
            rule__Case__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Case__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__3"


    // $ANTLR start "rule__Case__Group__3__Impl"
    // InternalMethodName.g:1888:1: rule__Case__Group__3__Impl : ( ( rule__Case__ConditionAssignment_3 ) ) ;
    public final void rule__Case__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1892:1: ( ( ( rule__Case__ConditionAssignment_3 ) ) )
            // InternalMethodName.g:1893:1: ( ( rule__Case__ConditionAssignment_3 ) )
            {
            // InternalMethodName.g:1893:1: ( ( rule__Case__ConditionAssignment_3 ) )
            // InternalMethodName.g:1894:2: ( rule__Case__ConditionAssignment_3 )
            {
             before(grammarAccess.getCaseAccess().getConditionAssignment_3()); 
            // InternalMethodName.g:1895:2: ( rule__Case__ConditionAssignment_3 )
            // InternalMethodName.g:1895:3: rule__Case__ConditionAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Case__ConditionAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getCaseAccess().getConditionAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__3__Impl"


    // $ANTLR start "rule__Case__Group__4"
    // InternalMethodName.g:1903:1: rule__Case__Group__4 : rule__Case__Group__4__Impl rule__Case__Group__5 ;
    public final void rule__Case__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1907:1: ( rule__Case__Group__4__Impl rule__Case__Group__5 )
            // InternalMethodName.g:1908:2: rule__Case__Group__4__Impl rule__Case__Group__5
            {
            pushFollow(FOLLOW_26);
            rule__Case__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Case__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__4"


    // $ANTLR start "rule__Case__Group__4__Impl"
    // InternalMethodName.g:1915:1: rule__Case__Group__4__Impl : ( '{' ) ;
    public final void rule__Case__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1919:1: ( ( '{' ) )
            // InternalMethodName.g:1920:1: ( '{' )
            {
            // InternalMethodName.g:1920:1: ( '{' )
            // InternalMethodName.g:1921:2: '{'
            {
             before(grammarAccess.getCaseAccess().getLeftCurlyBracketKeyword_4()); 
            match(input,77,FOLLOW_2); 
             after(grammarAccess.getCaseAccess().getLeftCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__4__Impl"


    // $ANTLR start "rule__Case__Group__5"
    // InternalMethodName.g:1930:1: rule__Case__Group__5 : rule__Case__Group__5__Impl rule__Case__Group__6 ;
    public final void rule__Case__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1934:1: ( rule__Case__Group__5__Impl rule__Case__Group__6 )
            // InternalMethodName.g:1935:2: rule__Case__Group__5__Impl rule__Case__Group__6
            {
            pushFollow(FOLLOW_26);
            rule__Case__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Case__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__5"


    // $ANTLR start "rule__Case__Group__5__Impl"
    // InternalMethodName.g:1942:1: rule__Case__Group__5__Impl : ( ( rule__Case__PropertiesAssignment_5 )* ) ;
    public final void rule__Case__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1946:1: ( ( ( rule__Case__PropertiesAssignment_5 )* ) )
            // InternalMethodName.g:1947:1: ( ( rule__Case__PropertiesAssignment_5 )* )
            {
            // InternalMethodName.g:1947:1: ( ( rule__Case__PropertiesAssignment_5 )* )
            // InternalMethodName.g:1948:2: ( rule__Case__PropertiesAssignment_5 )*
            {
             before(grammarAccess.getCaseAccess().getPropertiesAssignment_5()); 
            // InternalMethodName.g:1949:2: ( rule__Case__PropertiesAssignment_5 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==RULE_ID||(LA16_0>=65 && LA16_0<=70)) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalMethodName.g:1949:3: rule__Case__PropertiesAssignment_5
            	    {
            	    pushFollow(FOLLOW_27);
            	    rule__Case__PropertiesAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

             after(grammarAccess.getCaseAccess().getPropertiesAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__5__Impl"


    // $ANTLR start "rule__Case__Group__6"
    // InternalMethodName.g:1957:1: rule__Case__Group__6 : rule__Case__Group__6__Impl ;
    public final void rule__Case__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1961:1: ( rule__Case__Group__6__Impl )
            // InternalMethodName.g:1962:2: rule__Case__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Case__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__6"


    // $ANTLR start "rule__Case__Group__6__Impl"
    // InternalMethodName.g:1968:1: rule__Case__Group__6__Impl : ( '}' ) ;
    public final void rule__Case__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1972:1: ( ( '}' ) )
            // InternalMethodName.g:1973:1: ( '}' )
            {
            // InternalMethodName.g:1973:1: ( '}' )
            // InternalMethodName.g:1974:2: '}'
            {
             before(grammarAccess.getCaseAccess().getRightCurlyBracketKeyword_6()); 
            match(input,82,FOLLOW_2); 
             after(grammarAccess.getCaseAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__6__Impl"


    // $ANTLR start "rule__Or__Group__0"
    // InternalMethodName.g:1984:1: rule__Or__Group__0 : rule__Or__Group__0__Impl rule__Or__Group__1 ;
    public final void rule__Or__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:1988:1: ( rule__Or__Group__0__Impl rule__Or__Group__1 )
            // InternalMethodName.g:1989:2: rule__Or__Group__0__Impl rule__Or__Group__1
            {
            pushFollow(FOLLOW_28);
            rule__Or__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0"


    // $ANTLR start "rule__Or__Group__0__Impl"
    // InternalMethodName.g:1996:1: rule__Or__Group__0__Impl : ( ruleAnd ) ;
    public final void rule__Or__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2000:1: ( ( ruleAnd ) )
            // InternalMethodName.g:2001:1: ( ruleAnd )
            {
            // InternalMethodName.g:2001:1: ( ruleAnd )
            // InternalMethodName.g:2002:2: ruleAnd
            {
             before(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0__Impl"


    // $ANTLR start "rule__Or__Group__1"
    // InternalMethodName.g:2011:1: rule__Or__Group__1 : rule__Or__Group__1__Impl ;
    public final void rule__Or__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2015:1: ( rule__Or__Group__1__Impl )
            // InternalMethodName.g:2016:2: rule__Or__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1"


    // $ANTLR start "rule__Or__Group__1__Impl"
    // InternalMethodName.g:2022:1: rule__Or__Group__1__Impl : ( ( rule__Or__Group_1__0 )* ) ;
    public final void rule__Or__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2026:1: ( ( ( rule__Or__Group_1__0 )* ) )
            // InternalMethodName.g:2027:1: ( ( rule__Or__Group_1__0 )* )
            {
            // InternalMethodName.g:2027:1: ( ( rule__Or__Group_1__0 )* )
            // InternalMethodName.g:2028:2: ( rule__Or__Group_1__0 )*
            {
             before(grammarAccess.getOrAccess().getGroup_1()); 
            // InternalMethodName.g:2029:2: ( rule__Or__Group_1__0 )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==86) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalMethodName.g:2029:3: rule__Or__Group_1__0
            	    {
            	    pushFollow(FOLLOW_29);
            	    rule__Or__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

             after(grammarAccess.getOrAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1__Impl"


    // $ANTLR start "rule__Or__Group_1__0"
    // InternalMethodName.g:2038:1: rule__Or__Group_1__0 : rule__Or__Group_1__0__Impl rule__Or__Group_1__1 ;
    public final void rule__Or__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2042:1: ( rule__Or__Group_1__0__Impl rule__Or__Group_1__1 )
            // InternalMethodName.g:2043:2: rule__Or__Group_1__0__Impl rule__Or__Group_1__1
            {
            pushFollow(FOLLOW_28);
            rule__Or__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0"


    // $ANTLR start "rule__Or__Group_1__0__Impl"
    // InternalMethodName.g:2050:1: rule__Or__Group_1__0__Impl : ( () ) ;
    public final void rule__Or__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2054:1: ( ( () ) )
            // InternalMethodName.g:2055:1: ( () )
            {
            // InternalMethodName.g:2055:1: ( () )
            // InternalMethodName.g:2056:2: ()
            {
             before(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 
            // InternalMethodName.g:2057:2: ()
            // InternalMethodName.g:2057:3: 
            {
            }

             after(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0__Impl"


    // $ANTLR start "rule__Or__Group_1__1"
    // InternalMethodName.g:2065:1: rule__Or__Group_1__1 : rule__Or__Group_1__1__Impl rule__Or__Group_1__2 ;
    public final void rule__Or__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2069:1: ( rule__Or__Group_1__1__Impl rule__Or__Group_1__2 )
            // InternalMethodName.g:2070:2: rule__Or__Group_1__1__Impl rule__Or__Group_1__2
            {
            pushFollow(FOLLOW_25);
            rule__Or__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1"


    // $ANTLR start "rule__Or__Group_1__1__Impl"
    // InternalMethodName.g:2077:1: rule__Or__Group_1__1__Impl : ( '|' ) ;
    public final void rule__Or__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2081:1: ( ( '|' ) )
            // InternalMethodName.g:2082:1: ( '|' )
            {
            // InternalMethodName.g:2082:1: ( '|' )
            // InternalMethodName.g:2083:2: '|'
            {
             before(grammarAccess.getOrAccess().getVerticalLineKeyword_1_1()); 
            match(input,86,FOLLOW_2); 
             after(grammarAccess.getOrAccess().getVerticalLineKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1__Impl"


    // $ANTLR start "rule__Or__Group_1__2"
    // InternalMethodName.g:2092:1: rule__Or__Group_1__2 : rule__Or__Group_1__2__Impl ;
    public final void rule__Or__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2096:1: ( rule__Or__Group_1__2__Impl )
            // InternalMethodName.g:2097:2: rule__Or__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2"


    // $ANTLR start "rule__Or__Group_1__2__Impl"
    // InternalMethodName.g:2103:1: rule__Or__Group_1__2__Impl : ( ( rule__Or__RightAssignment_1_2 ) ) ;
    public final void rule__Or__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2107:1: ( ( ( rule__Or__RightAssignment_1_2 ) ) )
            // InternalMethodName.g:2108:1: ( ( rule__Or__RightAssignment_1_2 ) )
            {
            // InternalMethodName.g:2108:1: ( ( rule__Or__RightAssignment_1_2 ) )
            // InternalMethodName.g:2109:2: ( rule__Or__RightAssignment_1_2 )
            {
             before(grammarAccess.getOrAccess().getRightAssignment_1_2()); 
            // InternalMethodName.g:2110:2: ( rule__Or__RightAssignment_1_2 )
            // InternalMethodName.g:2110:3: rule__Or__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Or__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2__Impl"


    // $ANTLR start "rule__And__Group__0"
    // InternalMethodName.g:2119:1: rule__And__Group__0 : rule__And__Group__0__Impl rule__And__Group__1 ;
    public final void rule__And__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2123:1: ( rule__And__Group__0__Impl rule__And__Group__1 )
            // InternalMethodName.g:2124:2: rule__And__Group__0__Impl rule__And__Group__1
            {
            pushFollow(FOLLOW_30);
            rule__And__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0"


    // $ANTLR start "rule__And__Group__0__Impl"
    // InternalMethodName.g:2131:1: rule__And__Group__0__Impl : ( rulePrimary ) ;
    public final void rule__And__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2135:1: ( ( rulePrimary ) )
            // InternalMethodName.g:2136:1: ( rulePrimary )
            {
            // InternalMethodName.g:2136:1: ( rulePrimary )
            // InternalMethodName.g:2137:2: rulePrimary
            {
             before(grammarAccess.getAndAccess().getPrimaryParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getAndAccess().getPrimaryParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0__Impl"


    // $ANTLR start "rule__And__Group__1"
    // InternalMethodName.g:2146:1: rule__And__Group__1 : rule__And__Group__1__Impl ;
    public final void rule__And__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2150:1: ( rule__And__Group__1__Impl )
            // InternalMethodName.g:2151:2: rule__And__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1"


    // $ANTLR start "rule__And__Group__1__Impl"
    // InternalMethodName.g:2157:1: rule__And__Group__1__Impl : ( ( rule__And__Group_1__0 )* ) ;
    public final void rule__And__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2161:1: ( ( ( rule__And__Group_1__0 )* ) )
            // InternalMethodName.g:2162:1: ( ( rule__And__Group_1__0 )* )
            {
            // InternalMethodName.g:2162:1: ( ( rule__And__Group_1__0 )* )
            // InternalMethodName.g:2163:2: ( rule__And__Group_1__0 )*
            {
             before(grammarAccess.getAndAccess().getGroup_1()); 
            // InternalMethodName.g:2164:2: ( rule__And__Group_1__0 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( ((LA18_0>=16 && LA18_0<=17)) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalMethodName.g:2164:3: rule__And__Group_1__0
            	    {
            	    pushFollow(FOLLOW_31);
            	    rule__And__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

             after(grammarAccess.getAndAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1__Impl"


    // $ANTLR start "rule__And__Group_1__0"
    // InternalMethodName.g:2173:1: rule__And__Group_1__0 : rule__And__Group_1__0__Impl rule__And__Group_1__1 ;
    public final void rule__And__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2177:1: ( rule__And__Group_1__0__Impl rule__And__Group_1__1 )
            // InternalMethodName.g:2178:2: rule__And__Group_1__0__Impl rule__And__Group_1__1
            {
            pushFollow(FOLLOW_30);
            rule__And__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0"


    // $ANTLR start "rule__And__Group_1__0__Impl"
    // InternalMethodName.g:2185:1: rule__And__Group_1__0__Impl : ( () ) ;
    public final void rule__And__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2189:1: ( ( () ) )
            // InternalMethodName.g:2190:1: ( () )
            {
            // InternalMethodName.g:2190:1: ( () )
            // InternalMethodName.g:2191:2: ()
            {
             before(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 
            // InternalMethodName.g:2192:2: ()
            // InternalMethodName.g:2192:3: 
            {
            }

             after(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0__Impl"


    // $ANTLR start "rule__And__Group_1__1"
    // InternalMethodName.g:2200:1: rule__And__Group_1__1 : rule__And__Group_1__1__Impl rule__And__Group_1__2 ;
    public final void rule__And__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2204:1: ( rule__And__Group_1__1__Impl rule__And__Group_1__2 )
            // InternalMethodName.g:2205:2: rule__And__Group_1__1__Impl rule__And__Group_1__2
            {
            pushFollow(FOLLOW_25);
            rule__And__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1"


    // $ANTLR start "rule__And__Group_1__1__Impl"
    // InternalMethodName.g:2212:1: rule__And__Group_1__1__Impl : ( ( rule__And__ConcatKindAssignment_1_1 ) ) ;
    public final void rule__And__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2216:1: ( ( ( rule__And__ConcatKindAssignment_1_1 ) ) )
            // InternalMethodName.g:2217:1: ( ( rule__And__ConcatKindAssignment_1_1 ) )
            {
            // InternalMethodName.g:2217:1: ( ( rule__And__ConcatKindAssignment_1_1 ) )
            // InternalMethodName.g:2218:2: ( rule__And__ConcatKindAssignment_1_1 )
            {
             before(grammarAccess.getAndAccess().getConcatKindAssignment_1_1()); 
            // InternalMethodName.g:2219:2: ( rule__And__ConcatKindAssignment_1_1 )
            // InternalMethodName.g:2219:3: rule__And__ConcatKindAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__And__ConcatKindAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getConcatKindAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1__Impl"


    // $ANTLR start "rule__And__Group_1__2"
    // InternalMethodName.g:2227:1: rule__And__Group_1__2 : rule__And__Group_1__2__Impl ;
    public final void rule__And__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2231:1: ( rule__And__Group_1__2__Impl )
            // InternalMethodName.g:2232:2: rule__And__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2"


    // $ANTLR start "rule__And__Group_1__2__Impl"
    // InternalMethodName.g:2238:1: rule__And__Group_1__2__Impl : ( ( rule__And__RightAssignment_1_2 ) ) ;
    public final void rule__And__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2242:1: ( ( ( rule__And__RightAssignment_1_2 ) ) )
            // InternalMethodName.g:2243:1: ( ( rule__And__RightAssignment_1_2 ) )
            {
            // InternalMethodName.g:2243:1: ( ( rule__And__RightAssignment_1_2 ) )
            // InternalMethodName.g:2244:2: ( rule__And__RightAssignment_1_2 )
            {
             before(grammarAccess.getAndAccess().getRightAssignment_1_2()); 
            // InternalMethodName.g:2245:2: ( rule__And__RightAssignment_1_2 )
            // InternalMethodName.g:2245:3: rule__And__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__And__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2__Impl"


    // $ANTLR start "rule__GroupExpr__Group__0"
    // InternalMethodName.g:2254:1: rule__GroupExpr__Group__0 : rule__GroupExpr__Group__0__Impl rule__GroupExpr__Group__1 ;
    public final void rule__GroupExpr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2258:1: ( rule__GroupExpr__Group__0__Impl rule__GroupExpr__Group__1 )
            // InternalMethodName.g:2259:2: rule__GroupExpr__Group__0__Impl rule__GroupExpr__Group__1
            {
            pushFollow(FOLLOW_32);
            rule__GroupExpr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GroupExpr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupExpr__Group__0"


    // $ANTLR start "rule__GroupExpr__Group__0__Impl"
    // InternalMethodName.g:2266:1: rule__GroupExpr__Group__0__Impl : ( ( rule__GroupExpr__HasNotAssignment_0 )? ) ;
    public final void rule__GroupExpr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2270:1: ( ( ( rule__GroupExpr__HasNotAssignment_0 )? ) )
            // InternalMethodName.g:2271:1: ( ( rule__GroupExpr__HasNotAssignment_0 )? )
            {
            // InternalMethodName.g:2271:1: ( ( rule__GroupExpr__HasNotAssignment_0 )? )
            // InternalMethodName.g:2272:2: ( rule__GroupExpr__HasNotAssignment_0 )?
            {
             before(grammarAccess.getGroupExprAccess().getHasNotAssignment_0()); 
            // InternalMethodName.g:2273:2: ( rule__GroupExpr__HasNotAssignment_0 )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==92) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalMethodName.g:2273:3: rule__GroupExpr__HasNotAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__GroupExpr__HasNotAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getGroupExprAccess().getHasNotAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupExpr__Group__0__Impl"


    // $ANTLR start "rule__GroupExpr__Group__1"
    // InternalMethodName.g:2281:1: rule__GroupExpr__Group__1 : rule__GroupExpr__Group__1__Impl rule__GroupExpr__Group__2 ;
    public final void rule__GroupExpr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2285:1: ( rule__GroupExpr__Group__1__Impl rule__GroupExpr__Group__2 )
            // InternalMethodName.g:2286:2: rule__GroupExpr__Group__1__Impl rule__GroupExpr__Group__2
            {
            pushFollow(FOLLOW_25);
            rule__GroupExpr__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GroupExpr__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupExpr__Group__1"


    // $ANTLR start "rule__GroupExpr__Group__1__Impl"
    // InternalMethodName.g:2293:1: rule__GroupExpr__Group__1__Impl : ( '(' ) ;
    public final void rule__GroupExpr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2297:1: ( ( '(' ) )
            // InternalMethodName.g:2298:1: ( '(' )
            {
            // InternalMethodName.g:2298:1: ( '(' )
            // InternalMethodName.g:2299:2: '('
            {
             before(grammarAccess.getGroupExprAccess().getLeftParenthesisKeyword_1()); 
            match(input,87,FOLLOW_2); 
             after(grammarAccess.getGroupExprAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupExpr__Group__1__Impl"


    // $ANTLR start "rule__GroupExpr__Group__2"
    // InternalMethodName.g:2308:1: rule__GroupExpr__Group__2 : rule__GroupExpr__Group__2__Impl rule__GroupExpr__Group__3 ;
    public final void rule__GroupExpr__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2312:1: ( rule__GroupExpr__Group__2__Impl rule__GroupExpr__Group__3 )
            // InternalMethodName.g:2313:2: rule__GroupExpr__Group__2__Impl rule__GroupExpr__Group__3
            {
            pushFollow(FOLLOW_33);
            rule__GroupExpr__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GroupExpr__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupExpr__Group__2"


    // $ANTLR start "rule__GroupExpr__Group__2__Impl"
    // InternalMethodName.g:2320:1: rule__GroupExpr__Group__2__Impl : ( ( rule__GroupExpr__ExprAssignment_2 ) ) ;
    public final void rule__GroupExpr__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2324:1: ( ( ( rule__GroupExpr__ExprAssignment_2 ) ) )
            // InternalMethodName.g:2325:1: ( ( rule__GroupExpr__ExprAssignment_2 ) )
            {
            // InternalMethodName.g:2325:1: ( ( rule__GroupExpr__ExprAssignment_2 ) )
            // InternalMethodName.g:2326:2: ( rule__GroupExpr__ExprAssignment_2 )
            {
             before(grammarAccess.getGroupExprAccess().getExprAssignment_2()); 
            // InternalMethodName.g:2327:2: ( rule__GroupExpr__ExprAssignment_2 )
            // InternalMethodName.g:2327:3: rule__GroupExpr__ExprAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__GroupExpr__ExprAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getGroupExprAccess().getExprAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupExpr__Group__2__Impl"


    // $ANTLR start "rule__GroupExpr__Group__3"
    // InternalMethodName.g:2335:1: rule__GroupExpr__Group__3 : rule__GroupExpr__Group__3__Impl rule__GroupExpr__Group__4 ;
    public final void rule__GroupExpr__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2339:1: ( rule__GroupExpr__Group__3__Impl rule__GroupExpr__Group__4 )
            // InternalMethodName.g:2340:2: rule__GroupExpr__Group__3__Impl rule__GroupExpr__Group__4
            {
            pushFollow(FOLLOW_34);
            rule__GroupExpr__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GroupExpr__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupExpr__Group__3"


    // $ANTLR start "rule__GroupExpr__Group__3__Impl"
    // InternalMethodName.g:2347:1: rule__GroupExpr__Group__3__Impl : ( ')' ) ;
    public final void rule__GroupExpr__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2351:1: ( ( ')' ) )
            // InternalMethodName.g:2352:1: ( ')' )
            {
            // InternalMethodName.g:2352:1: ( ')' )
            // InternalMethodName.g:2353:2: ')'
            {
             before(grammarAccess.getGroupExprAccess().getRightParenthesisKeyword_3()); 
            match(input,88,FOLLOW_2); 
             after(grammarAccess.getGroupExprAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupExpr__Group__3__Impl"


    // $ANTLR start "rule__GroupExpr__Group__4"
    // InternalMethodName.g:2362:1: rule__GroupExpr__Group__4 : rule__GroupExpr__Group__4__Impl ;
    public final void rule__GroupExpr__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2366:1: ( rule__GroupExpr__Group__4__Impl )
            // InternalMethodName.g:2367:2: rule__GroupExpr__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GroupExpr__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupExpr__Group__4"


    // $ANTLR start "rule__GroupExpr__Group__4__Impl"
    // InternalMethodName.g:2373:1: rule__GroupExpr__Group__4__Impl : ( ( rule__GroupExpr__CardAssignment_4 )? ) ;
    public final void rule__GroupExpr__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2377:1: ( ( ( rule__GroupExpr__CardAssignment_4 )? ) )
            // InternalMethodName.g:2378:1: ( ( rule__GroupExpr__CardAssignment_4 )? )
            {
            // InternalMethodName.g:2378:1: ( ( rule__GroupExpr__CardAssignment_4 )? )
            // InternalMethodName.g:2379:2: ( rule__GroupExpr__CardAssignment_4 )?
            {
             before(grammarAccess.getGroupExprAccess().getCardAssignment_4()); 
            // InternalMethodName.g:2380:2: ( rule__GroupExpr__CardAssignment_4 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( ((LA20_0>=61 && LA20_0<=64)) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalMethodName.g:2380:3: rule__GroupExpr__CardAssignment_4
                    {
                    pushFollow(FOLLOW_2);
                    rule__GroupExpr__CardAssignment_4();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getGroupExprAccess().getCardAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupExpr__Group__4__Impl"


    // $ANTLR start "rule__Atomic__Group_0__0"
    // InternalMethodName.g:2389:1: rule__Atomic__Group_0__0 : rule__Atomic__Group_0__0__Impl rule__Atomic__Group_0__1 ;
    public final void rule__Atomic__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2393:1: ( rule__Atomic__Group_0__0__Impl rule__Atomic__Group_0__1 )
            // InternalMethodName.g:2394:2: rule__Atomic__Group_0__0__Impl rule__Atomic__Group_0__1
            {
            pushFollow(FOLLOW_35);
            rule__Atomic__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0__0"


    // $ANTLR start "rule__Atomic__Group_0__0__Impl"
    // InternalMethodName.g:2401:1: rule__Atomic__Group_0__0__Impl : ( () ) ;
    public final void rule__Atomic__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2405:1: ( ( () ) )
            // InternalMethodName.g:2406:1: ( () )
            {
            // InternalMethodName.g:2406:1: ( () )
            // InternalMethodName.g:2407:2: ()
            {
             before(grammarAccess.getAtomicAccess().getStringConstAction_0_0()); 
            // InternalMethodName.g:2408:2: ()
            // InternalMethodName.g:2408:3: 
            {
            }

             after(grammarAccess.getAtomicAccess().getStringConstAction_0_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0__0__Impl"


    // $ANTLR start "rule__Atomic__Group_0__1"
    // InternalMethodName.g:2416:1: rule__Atomic__Group_0__1 : rule__Atomic__Group_0__1__Impl rule__Atomic__Group_0__2 ;
    public final void rule__Atomic__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2420:1: ( rule__Atomic__Group_0__1__Impl rule__Atomic__Group_0__2 )
            // InternalMethodName.g:2421:2: rule__Atomic__Group_0__1__Impl rule__Atomic__Group_0__2
            {
            pushFollow(FOLLOW_35);
            rule__Atomic__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0__1"


    // $ANTLR start "rule__Atomic__Group_0__1__Impl"
    // InternalMethodName.g:2428:1: rule__Atomic__Group_0__1__Impl : ( ( rule__Atomic__HasNotAssignment_0_1 )? ) ;
    public final void rule__Atomic__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2432:1: ( ( ( rule__Atomic__HasNotAssignment_0_1 )? ) )
            // InternalMethodName.g:2433:1: ( ( rule__Atomic__HasNotAssignment_0_1 )? )
            {
            // InternalMethodName.g:2433:1: ( ( rule__Atomic__HasNotAssignment_0_1 )? )
            // InternalMethodName.g:2434:2: ( rule__Atomic__HasNotAssignment_0_1 )?
            {
             before(grammarAccess.getAtomicAccess().getHasNotAssignment_0_1()); 
            // InternalMethodName.g:2435:2: ( rule__Atomic__HasNotAssignment_0_1 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==92) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalMethodName.g:2435:3: rule__Atomic__HasNotAssignment_0_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__HasNotAssignment_0_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAtomicAccess().getHasNotAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0__1__Impl"


    // $ANTLR start "rule__Atomic__Group_0__2"
    // InternalMethodName.g:2443:1: rule__Atomic__Group_0__2 : rule__Atomic__Group_0__2__Impl rule__Atomic__Group_0__3 ;
    public final void rule__Atomic__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2447:1: ( rule__Atomic__Group_0__2__Impl rule__Atomic__Group_0__3 )
            // InternalMethodName.g:2448:2: rule__Atomic__Group_0__2__Impl rule__Atomic__Group_0__3
            {
            pushFollow(FOLLOW_36);
            rule__Atomic__Group_0__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_0__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0__2"


    // $ANTLR start "rule__Atomic__Group_0__2__Impl"
    // InternalMethodName.g:2455:1: rule__Atomic__Group_0__2__Impl : ( ( rule__Atomic__ValueAssignment_0_2 ) ) ;
    public final void rule__Atomic__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2459:1: ( ( ( rule__Atomic__ValueAssignment_0_2 ) ) )
            // InternalMethodName.g:2460:1: ( ( rule__Atomic__ValueAssignment_0_2 ) )
            {
            // InternalMethodName.g:2460:1: ( ( rule__Atomic__ValueAssignment_0_2 ) )
            // InternalMethodName.g:2461:2: ( rule__Atomic__ValueAssignment_0_2 )
            {
             before(grammarAccess.getAtomicAccess().getValueAssignment_0_2()); 
            // InternalMethodName.g:2462:2: ( rule__Atomic__ValueAssignment_0_2 )
            // InternalMethodName.g:2462:3: rule__Atomic__ValueAssignment_0_2
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__ValueAssignment_0_2();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getValueAssignment_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0__2__Impl"


    // $ANTLR start "rule__Atomic__Group_0__3"
    // InternalMethodName.g:2470:1: rule__Atomic__Group_0__3 : rule__Atomic__Group_0__3__Impl rule__Atomic__Group_0__4 ;
    public final void rule__Atomic__Group_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2474:1: ( rule__Atomic__Group_0__3__Impl rule__Atomic__Group_0__4 )
            // InternalMethodName.g:2475:2: rule__Atomic__Group_0__3__Impl rule__Atomic__Group_0__4
            {
            pushFollow(FOLLOW_36);
            rule__Atomic__Group_0__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_0__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0__3"


    // $ANTLR start "rule__Atomic__Group_0__3__Impl"
    // InternalMethodName.g:2482:1: rule__Atomic__Group_0__3__Impl : ( ( rule__Atomic__CardAssignment_0_3 )? ) ;
    public final void rule__Atomic__Group_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2486:1: ( ( ( rule__Atomic__CardAssignment_0_3 )? ) )
            // InternalMethodName.g:2487:1: ( ( rule__Atomic__CardAssignment_0_3 )? )
            {
            // InternalMethodName.g:2487:1: ( ( rule__Atomic__CardAssignment_0_3 )? )
            // InternalMethodName.g:2488:2: ( rule__Atomic__CardAssignment_0_3 )?
            {
             before(grammarAccess.getAtomicAccess().getCardAssignment_0_3()); 
            // InternalMethodName.g:2489:2: ( rule__Atomic__CardAssignment_0_3 )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( ((LA22_0>=61 && LA22_0<=64)) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalMethodName.g:2489:3: rule__Atomic__CardAssignment_0_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__CardAssignment_0_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAtomicAccess().getCardAssignment_0_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0__3__Impl"


    // $ANTLR start "rule__Atomic__Group_0__4"
    // InternalMethodName.g:2497:1: rule__Atomic__Group_0__4 : rule__Atomic__Group_0__4__Impl ;
    public final void rule__Atomic__Group_0__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2501:1: ( rule__Atomic__Group_0__4__Impl )
            // InternalMethodName.g:2502:2: rule__Atomic__Group_0__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Group_0__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0__4"


    // $ANTLR start "rule__Atomic__Group_0__4__Impl"
    // InternalMethodName.g:2508:1: rule__Atomic__Group_0__4__Impl : ( ( rule__Atomic__Group_0_4__0 )? ) ;
    public final void rule__Atomic__Group_0__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2512:1: ( ( ( rule__Atomic__Group_0_4__0 )? ) )
            // InternalMethodName.g:2513:1: ( ( rule__Atomic__Group_0_4__0 )? )
            {
            // InternalMethodName.g:2513:1: ( ( rule__Atomic__Group_0_4__0 )? )
            // InternalMethodName.g:2514:2: ( rule__Atomic__Group_0_4__0 )?
            {
             before(grammarAccess.getAtomicAccess().getGroup_0_4()); 
            // InternalMethodName.g:2515:2: ( rule__Atomic__Group_0_4__0 )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==89) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalMethodName.g:2515:3: rule__Atomic__Group_0_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__Group_0_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAtomicAccess().getGroup_0_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0__4__Impl"


    // $ANTLR start "rule__Atomic__Group_0_4__0"
    // InternalMethodName.g:2524:1: rule__Atomic__Group_0_4__0 : rule__Atomic__Group_0_4__0__Impl rule__Atomic__Group_0_4__1 ;
    public final void rule__Atomic__Group_0_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2528:1: ( rule__Atomic__Group_0_4__0__Impl rule__Atomic__Group_0_4__1 )
            // InternalMethodName.g:2529:2: rule__Atomic__Group_0_4__0__Impl rule__Atomic__Group_0_4__1
            {
            pushFollow(FOLLOW_37);
            rule__Atomic__Group_0_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_0_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0_4__0"


    // $ANTLR start "rule__Atomic__Group_0_4__0__Impl"
    // InternalMethodName.g:2536:1: rule__Atomic__Group_0_4__0__Impl : ( '[' ) ;
    public final void rule__Atomic__Group_0_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2540:1: ( ( '[' ) )
            // InternalMethodName.g:2541:1: ( '[' )
            {
            // InternalMethodName.g:2541:1: ( '[' )
            // InternalMethodName.g:2542:2: '['
            {
             before(grammarAccess.getAtomicAccess().getLeftSquareBracketKeyword_0_4_0()); 
            match(input,89,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getLeftSquareBracketKeyword_0_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0_4__0__Impl"


    // $ANTLR start "rule__Atomic__Group_0_4__1"
    // InternalMethodName.g:2551:1: rule__Atomic__Group_0_4__1 : rule__Atomic__Group_0_4__1__Impl rule__Atomic__Group_0_4__2 ;
    public final void rule__Atomic__Group_0_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2555:1: ( rule__Atomic__Group_0_4__1__Impl rule__Atomic__Group_0_4__2 )
            // InternalMethodName.g:2556:2: rule__Atomic__Group_0_4__1__Impl rule__Atomic__Group_0_4__2
            {
            pushFollow(FOLLOW_38);
            rule__Atomic__Group_0_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_0_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0_4__1"


    // $ANTLR start "rule__Atomic__Group_0_4__1__Impl"
    // InternalMethodName.g:2563:1: rule__Atomic__Group_0_4__1__Impl : ( ( rule__Atomic__SentimentAssignment_0_4_1 ) ) ;
    public final void rule__Atomic__Group_0_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2567:1: ( ( ( rule__Atomic__SentimentAssignment_0_4_1 ) ) )
            // InternalMethodName.g:2568:1: ( ( rule__Atomic__SentimentAssignment_0_4_1 ) )
            {
            // InternalMethodName.g:2568:1: ( ( rule__Atomic__SentimentAssignment_0_4_1 ) )
            // InternalMethodName.g:2569:2: ( rule__Atomic__SentimentAssignment_0_4_1 )
            {
             before(grammarAccess.getAtomicAccess().getSentimentAssignment_0_4_1()); 
            // InternalMethodName.g:2570:2: ( rule__Atomic__SentimentAssignment_0_4_1 )
            // InternalMethodName.g:2570:3: rule__Atomic__SentimentAssignment_0_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__SentimentAssignment_0_4_1();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getSentimentAssignment_0_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0_4__1__Impl"


    // $ANTLR start "rule__Atomic__Group_0_4__2"
    // InternalMethodName.g:2578:1: rule__Atomic__Group_0_4__2 : rule__Atomic__Group_0_4__2__Impl ;
    public final void rule__Atomic__Group_0_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2582:1: ( rule__Atomic__Group_0_4__2__Impl )
            // InternalMethodName.g:2583:2: rule__Atomic__Group_0_4__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Group_0_4__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0_4__2"


    // $ANTLR start "rule__Atomic__Group_0_4__2__Impl"
    // InternalMethodName.g:2589:1: rule__Atomic__Group_0_4__2__Impl : ( ']' ) ;
    public final void rule__Atomic__Group_0_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2593:1: ( ( ']' ) )
            // InternalMethodName.g:2594:1: ( ']' )
            {
            // InternalMethodName.g:2594:1: ( ']' )
            // InternalMethodName.g:2595:2: ']'
            {
             before(grammarAccess.getAtomicAccess().getRightSquareBracketKeyword_0_4_2()); 
            match(input,90,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getRightSquareBracketKeyword_0_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_0_4__2__Impl"


    // $ANTLR start "rule__Atomic__Group_1__0"
    // InternalMethodName.g:2605:1: rule__Atomic__Group_1__0 : rule__Atomic__Group_1__0__Impl rule__Atomic__Group_1__1 ;
    public final void rule__Atomic__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2609:1: ( rule__Atomic__Group_1__0__Impl rule__Atomic__Group_1__1 )
            // InternalMethodName.g:2610:2: rule__Atomic__Group_1__0__Impl rule__Atomic__Group_1__1
            {
            pushFollow(FOLLOW_39);
            rule__Atomic__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1__0"


    // $ANTLR start "rule__Atomic__Group_1__0__Impl"
    // InternalMethodName.g:2617:1: rule__Atomic__Group_1__0__Impl : ( () ) ;
    public final void rule__Atomic__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2621:1: ( ( () ) )
            // InternalMethodName.g:2622:1: ( () )
            {
            // InternalMethodName.g:2622:1: ( () )
            // InternalMethodName.g:2623:2: ()
            {
             before(grammarAccess.getAtomicAccess().getPOSValueAction_1_0()); 
            // InternalMethodName.g:2624:2: ()
            // InternalMethodName.g:2624:3: 
            {
            }

             after(grammarAccess.getAtomicAccess().getPOSValueAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1__0__Impl"


    // $ANTLR start "rule__Atomic__Group_1__1"
    // InternalMethodName.g:2632:1: rule__Atomic__Group_1__1 : rule__Atomic__Group_1__1__Impl rule__Atomic__Group_1__2 ;
    public final void rule__Atomic__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2636:1: ( rule__Atomic__Group_1__1__Impl rule__Atomic__Group_1__2 )
            // InternalMethodName.g:2637:2: rule__Atomic__Group_1__1__Impl rule__Atomic__Group_1__2
            {
            pushFollow(FOLLOW_39);
            rule__Atomic__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1__1"


    // $ANTLR start "rule__Atomic__Group_1__1__Impl"
    // InternalMethodName.g:2644:1: rule__Atomic__Group_1__1__Impl : ( ( rule__Atomic__HasNotAssignment_1_1 )? ) ;
    public final void rule__Atomic__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2648:1: ( ( ( rule__Atomic__HasNotAssignment_1_1 )? ) )
            // InternalMethodName.g:2649:1: ( ( rule__Atomic__HasNotAssignment_1_1 )? )
            {
            // InternalMethodName.g:2649:1: ( ( rule__Atomic__HasNotAssignment_1_1 )? )
            // InternalMethodName.g:2650:2: ( rule__Atomic__HasNotAssignment_1_1 )?
            {
             before(grammarAccess.getAtomicAccess().getHasNotAssignment_1_1()); 
            // InternalMethodName.g:2651:2: ( rule__Atomic__HasNotAssignment_1_1 )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==92) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalMethodName.g:2651:3: rule__Atomic__HasNotAssignment_1_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__HasNotAssignment_1_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAtomicAccess().getHasNotAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1__1__Impl"


    // $ANTLR start "rule__Atomic__Group_1__2"
    // InternalMethodName.g:2659:1: rule__Atomic__Group_1__2 : rule__Atomic__Group_1__2__Impl rule__Atomic__Group_1__3 ;
    public final void rule__Atomic__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2663:1: ( rule__Atomic__Group_1__2__Impl rule__Atomic__Group_1__3 )
            // InternalMethodName.g:2664:2: rule__Atomic__Group_1__2__Impl rule__Atomic__Group_1__3
            {
            pushFollow(FOLLOW_36);
            rule__Atomic__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1__2"


    // $ANTLR start "rule__Atomic__Group_1__2__Impl"
    // InternalMethodName.g:2671:1: rule__Atomic__Group_1__2__Impl : ( ( rule__Atomic__PosAssignment_1_2 ) ) ;
    public final void rule__Atomic__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2675:1: ( ( ( rule__Atomic__PosAssignment_1_2 ) ) )
            // InternalMethodName.g:2676:1: ( ( rule__Atomic__PosAssignment_1_2 ) )
            {
            // InternalMethodName.g:2676:1: ( ( rule__Atomic__PosAssignment_1_2 ) )
            // InternalMethodName.g:2677:2: ( rule__Atomic__PosAssignment_1_2 )
            {
             before(grammarAccess.getAtomicAccess().getPosAssignment_1_2()); 
            // InternalMethodName.g:2678:2: ( rule__Atomic__PosAssignment_1_2 )
            // InternalMethodName.g:2678:3: rule__Atomic__PosAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__PosAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getPosAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1__2__Impl"


    // $ANTLR start "rule__Atomic__Group_1__3"
    // InternalMethodName.g:2686:1: rule__Atomic__Group_1__3 : rule__Atomic__Group_1__3__Impl rule__Atomic__Group_1__4 ;
    public final void rule__Atomic__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2690:1: ( rule__Atomic__Group_1__3__Impl rule__Atomic__Group_1__4 )
            // InternalMethodName.g:2691:2: rule__Atomic__Group_1__3__Impl rule__Atomic__Group_1__4
            {
            pushFollow(FOLLOW_36);
            rule__Atomic__Group_1__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_1__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1__3"


    // $ANTLR start "rule__Atomic__Group_1__3__Impl"
    // InternalMethodName.g:2698:1: rule__Atomic__Group_1__3__Impl : ( ( rule__Atomic__CardAssignment_1_3 )? ) ;
    public final void rule__Atomic__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2702:1: ( ( ( rule__Atomic__CardAssignment_1_3 )? ) )
            // InternalMethodName.g:2703:1: ( ( rule__Atomic__CardAssignment_1_3 )? )
            {
            // InternalMethodName.g:2703:1: ( ( rule__Atomic__CardAssignment_1_3 )? )
            // InternalMethodName.g:2704:2: ( rule__Atomic__CardAssignment_1_3 )?
            {
             before(grammarAccess.getAtomicAccess().getCardAssignment_1_3()); 
            // InternalMethodName.g:2705:2: ( rule__Atomic__CardAssignment_1_3 )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( ((LA25_0>=61 && LA25_0<=64)) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalMethodName.g:2705:3: rule__Atomic__CardAssignment_1_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__CardAssignment_1_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAtomicAccess().getCardAssignment_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1__3__Impl"


    // $ANTLR start "rule__Atomic__Group_1__4"
    // InternalMethodName.g:2713:1: rule__Atomic__Group_1__4 : rule__Atomic__Group_1__4__Impl ;
    public final void rule__Atomic__Group_1__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2717:1: ( rule__Atomic__Group_1__4__Impl )
            // InternalMethodName.g:2718:2: rule__Atomic__Group_1__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Group_1__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1__4"


    // $ANTLR start "rule__Atomic__Group_1__4__Impl"
    // InternalMethodName.g:2724:1: rule__Atomic__Group_1__4__Impl : ( ( rule__Atomic__Group_1_4__0 )? ) ;
    public final void rule__Atomic__Group_1__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2728:1: ( ( ( rule__Atomic__Group_1_4__0 )? ) )
            // InternalMethodName.g:2729:1: ( ( rule__Atomic__Group_1_4__0 )? )
            {
            // InternalMethodName.g:2729:1: ( ( rule__Atomic__Group_1_4__0 )? )
            // InternalMethodName.g:2730:2: ( rule__Atomic__Group_1_4__0 )?
            {
             before(grammarAccess.getAtomicAccess().getGroup_1_4()); 
            // InternalMethodName.g:2731:2: ( rule__Atomic__Group_1_4__0 )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==89) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalMethodName.g:2731:3: rule__Atomic__Group_1_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__Group_1_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAtomicAccess().getGroup_1_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1__4__Impl"


    // $ANTLR start "rule__Atomic__Group_1_4__0"
    // InternalMethodName.g:2740:1: rule__Atomic__Group_1_4__0 : rule__Atomic__Group_1_4__0__Impl rule__Atomic__Group_1_4__1 ;
    public final void rule__Atomic__Group_1_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2744:1: ( rule__Atomic__Group_1_4__0__Impl rule__Atomic__Group_1_4__1 )
            // InternalMethodName.g:2745:2: rule__Atomic__Group_1_4__0__Impl rule__Atomic__Group_1_4__1
            {
            pushFollow(FOLLOW_37);
            rule__Atomic__Group_1_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_1_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1_4__0"


    // $ANTLR start "rule__Atomic__Group_1_4__0__Impl"
    // InternalMethodName.g:2752:1: rule__Atomic__Group_1_4__0__Impl : ( '[' ) ;
    public final void rule__Atomic__Group_1_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2756:1: ( ( '[' ) )
            // InternalMethodName.g:2757:1: ( '[' )
            {
            // InternalMethodName.g:2757:1: ( '[' )
            // InternalMethodName.g:2758:2: '['
            {
             before(grammarAccess.getAtomicAccess().getLeftSquareBracketKeyword_1_4_0()); 
            match(input,89,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getLeftSquareBracketKeyword_1_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1_4__0__Impl"


    // $ANTLR start "rule__Atomic__Group_1_4__1"
    // InternalMethodName.g:2767:1: rule__Atomic__Group_1_4__1 : rule__Atomic__Group_1_4__1__Impl rule__Atomic__Group_1_4__2 ;
    public final void rule__Atomic__Group_1_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2771:1: ( rule__Atomic__Group_1_4__1__Impl rule__Atomic__Group_1_4__2 )
            // InternalMethodName.g:2772:2: rule__Atomic__Group_1_4__1__Impl rule__Atomic__Group_1_4__2
            {
            pushFollow(FOLLOW_38);
            rule__Atomic__Group_1_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_1_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1_4__1"


    // $ANTLR start "rule__Atomic__Group_1_4__1__Impl"
    // InternalMethodName.g:2779:1: rule__Atomic__Group_1_4__1__Impl : ( ( rule__Atomic__SentimentAssignment_1_4_1 ) ) ;
    public final void rule__Atomic__Group_1_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2783:1: ( ( ( rule__Atomic__SentimentAssignment_1_4_1 ) ) )
            // InternalMethodName.g:2784:1: ( ( rule__Atomic__SentimentAssignment_1_4_1 ) )
            {
            // InternalMethodName.g:2784:1: ( ( rule__Atomic__SentimentAssignment_1_4_1 ) )
            // InternalMethodName.g:2785:2: ( rule__Atomic__SentimentAssignment_1_4_1 )
            {
             before(grammarAccess.getAtomicAccess().getSentimentAssignment_1_4_1()); 
            // InternalMethodName.g:2786:2: ( rule__Atomic__SentimentAssignment_1_4_1 )
            // InternalMethodName.g:2786:3: rule__Atomic__SentimentAssignment_1_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__SentimentAssignment_1_4_1();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getSentimentAssignment_1_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1_4__1__Impl"


    // $ANTLR start "rule__Atomic__Group_1_4__2"
    // InternalMethodName.g:2794:1: rule__Atomic__Group_1_4__2 : rule__Atomic__Group_1_4__2__Impl ;
    public final void rule__Atomic__Group_1_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2798:1: ( rule__Atomic__Group_1_4__2__Impl )
            // InternalMethodName.g:2799:2: rule__Atomic__Group_1_4__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Group_1_4__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1_4__2"


    // $ANTLR start "rule__Atomic__Group_1_4__2__Impl"
    // InternalMethodName.g:2805:1: rule__Atomic__Group_1_4__2__Impl : ( ']' ) ;
    public final void rule__Atomic__Group_1_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2809:1: ( ( ']' ) )
            // InternalMethodName.g:2810:1: ( ']' )
            {
            // InternalMethodName.g:2810:1: ( ']' )
            // InternalMethodName.g:2811:2: ']'
            {
             before(grammarAccess.getAtomicAccess().getRightSquareBracketKeyword_1_4_2()); 
            match(input,90,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getRightSquareBracketKeyword_1_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_1_4__2__Impl"


    // $ANTLR start "rule__Atomic__Group_2__0"
    // InternalMethodName.g:2821:1: rule__Atomic__Group_2__0 : rule__Atomic__Group_2__0__Impl rule__Atomic__Group_2__1 ;
    public final void rule__Atomic__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2825:1: ( rule__Atomic__Group_2__0__Impl rule__Atomic__Group_2__1 )
            // InternalMethodName.g:2826:2: rule__Atomic__Group_2__0__Impl rule__Atomic__Group_2__1
            {
            pushFollow(FOLLOW_25);
            rule__Atomic__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2__0"


    // $ANTLR start "rule__Atomic__Group_2__0__Impl"
    // InternalMethodName.g:2833:1: rule__Atomic__Group_2__0__Impl : ( () ) ;
    public final void rule__Atomic__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2837:1: ( ( () ) )
            // InternalMethodName.g:2838:1: ( () )
            {
            // InternalMethodName.g:2838:1: ( () )
            // InternalMethodName.g:2839:2: ()
            {
             before(grammarAccess.getAtomicAccess().getSynonymConstAction_2_0()); 
            // InternalMethodName.g:2840:2: ()
            // InternalMethodName.g:2840:3: 
            {
            }

             after(grammarAccess.getAtomicAccess().getSynonymConstAction_2_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2__0__Impl"


    // $ANTLR start "rule__Atomic__Group_2__1"
    // InternalMethodName.g:2848:1: rule__Atomic__Group_2__1 : rule__Atomic__Group_2__1__Impl rule__Atomic__Group_2__2 ;
    public final void rule__Atomic__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2852:1: ( rule__Atomic__Group_2__1__Impl rule__Atomic__Group_2__2 )
            // InternalMethodName.g:2853:2: rule__Atomic__Group_2__1__Impl rule__Atomic__Group_2__2
            {
            pushFollow(FOLLOW_25);
            rule__Atomic__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2__1"


    // $ANTLR start "rule__Atomic__Group_2__1__Impl"
    // InternalMethodName.g:2860:1: rule__Atomic__Group_2__1__Impl : ( ( rule__Atomic__HasNotAssignment_2_1 )? ) ;
    public final void rule__Atomic__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2864:1: ( ( ( rule__Atomic__HasNotAssignment_2_1 )? ) )
            // InternalMethodName.g:2865:1: ( ( rule__Atomic__HasNotAssignment_2_1 )? )
            {
            // InternalMethodName.g:2865:1: ( ( rule__Atomic__HasNotAssignment_2_1 )? )
            // InternalMethodName.g:2866:2: ( rule__Atomic__HasNotAssignment_2_1 )?
            {
             before(grammarAccess.getAtomicAccess().getHasNotAssignment_2_1()); 
            // InternalMethodName.g:2867:2: ( rule__Atomic__HasNotAssignment_2_1 )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==92) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalMethodName.g:2867:3: rule__Atomic__HasNotAssignment_2_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__HasNotAssignment_2_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAtomicAccess().getHasNotAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2__1__Impl"


    // $ANTLR start "rule__Atomic__Group_2__2"
    // InternalMethodName.g:2875:1: rule__Atomic__Group_2__2 : rule__Atomic__Group_2__2__Impl rule__Atomic__Group_2__3 ;
    public final void rule__Atomic__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2879:1: ( rule__Atomic__Group_2__2__Impl rule__Atomic__Group_2__3 )
            // InternalMethodName.g:2880:2: rule__Atomic__Group_2__2__Impl rule__Atomic__Group_2__3
            {
            pushFollow(FOLLOW_12);
            rule__Atomic__Group_2__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_2__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2__2"


    // $ANTLR start "rule__Atomic__Group_2__2__Impl"
    // InternalMethodName.g:2887:1: rule__Atomic__Group_2__2__Impl : ( '#' ) ;
    public final void rule__Atomic__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2891:1: ( ( '#' ) )
            // InternalMethodName.g:2892:1: ( '#' )
            {
            // InternalMethodName.g:2892:1: ( '#' )
            // InternalMethodName.g:2893:2: '#'
            {
             before(grammarAccess.getAtomicAccess().getNumberSignKeyword_2_2()); 
            match(input,91,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getNumberSignKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2__2__Impl"


    // $ANTLR start "rule__Atomic__Group_2__3"
    // InternalMethodName.g:2902:1: rule__Atomic__Group_2__3 : rule__Atomic__Group_2__3__Impl rule__Atomic__Group_2__4 ;
    public final void rule__Atomic__Group_2__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2906:1: ( rule__Atomic__Group_2__3__Impl rule__Atomic__Group_2__4 )
            // InternalMethodName.g:2907:2: rule__Atomic__Group_2__3__Impl rule__Atomic__Group_2__4
            {
            pushFollow(FOLLOW_36);
            rule__Atomic__Group_2__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_2__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2__3"


    // $ANTLR start "rule__Atomic__Group_2__3__Impl"
    // InternalMethodName.g:2914:1: rule__Atomic__Group_2__3__Impl : ( ( rule__Atomic__ValueAssignment_2_3 ) ) ;
    public final void rule__Atomic__Group_2__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2918:1: ( ( ( rule__Atomic__ValueAssignment_2_3 ) ) )
            // InternalMethodName.g:2919:1: ( ( rule__Atomic__ValueAssignment_2_3 ) )
            {
            // InternalMethodName.g:2919:1: ( ( rule__Atomic__ValueAssignment_2_3 ) )
            // InternalMethodName.g:2920:2: ( rule__Atomic__ValueAssignment_2_3 )
            {
             before(grammarAccess.getAtomicAccess().getValueAssignment_2_3()); 
            // InternalMethodName.g:2921:2: ( rule__Atomic__ValueAssignment_2_3 )
            // InternalMethodName.g:2921:3: rule__Atomic__ValueAssignment_2_3
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__ValueAssignment_2_3();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getValueAssignment_2_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2__3__Impl"


    // $ANTLR start "rule__Atomic__Group_2__4"
    // InternalMethodName.g:2929:1: rule__Atomic__Group_2__4 : rule__Atomic__Group_2__4__Impl rule__Atomic__Group_2__5 ;
    public final void rule__Atomic__Group_2__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2933:1: ( rule__Atomic__Group_2__4__Impl rule__Atomic__Group_2__5 )
            // InternalMethodName.g:2934:2: rule__Atomic__Group_2__4__Impl rule__Atomic__Group_2__5
            {
            pushFollow(FOLLOW_36);
            rule__Atomic__Group_2__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_2__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2__4"


    // $ANTLR start "rule__Atomic__Group_2__4__Impl"
    // InternalMethodName.g:2941:1: rule__Atomic__Group_2__4__Impl : ( ( rule__Atomic__CardAssignment_2_4 )? ) ;
    public final void rule__Atomic__Group_2__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2945:1: ( ( ( rule__Atomic__CardAssignment_2_4 )? ) )
            // InternalMethodName.g:2946:1: ( ( rule__Atomic__CardAssignment_2_4 )? )
            {
            // InternalMethodName.g:2946:1: ( ( rule__Atomic__CardAssignment_2_4 )? )
            // InternalMethodName.g:2947:2: ( rule__Atomic__CardAssignment_2_4 )?
            {
             before(grammarAccess.getAtomicAccess().getCardAssignment_2_4()); 
            // InternalMethodName.g:2948:2: ( rule__Atomic__CardAssignment_2_4 )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( ((LA28_0>=61 && LA28_0<=64)) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalMethodName.g:2948:3: rule__Atomic__CardAssignment_2_4
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__CardAssignment_2_4();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAtomicAccess().getCardAssignment_2_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2__4__Impl"


    // $ANTLR start "rule__Atomic__Group_2__5"
    // InternalMethodName.g:2956:1: rule__Atomic__Group_2__5 : rule__Atomic__Group_2__5__Impl ;
    public final void rule__Atomic__Group_2__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2960:1: ( rule__Atomic__Group_2__5__Impl )
            // InternalMethodName.g:2961:2: rule__Atomic__Group_2__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Group_2__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2__5"


    // $ANTLR start "rule__Atomic__Group_2__5__Impl"
    // InternalMethodName.g:2967:1: rule__Atomic__Group_2__5__Impl : ( ( rule__Atomic__Group_2_5__0 )? ) ;
    public final void rule__Atomic__Group_2__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2971:1: ( ( ( rule__Atomic__Group_2_5__0 )? ) )
            // InternalMethodName.g:2972:1: ( ( rule__Atomic__Group_2_5__0 )? )
            {
            // InternalMethodName.g:2972:1: ( ( rule__Atomic__Group_2_5__0 )? )
            // InternalMethodName.g:2973:2: ( rule__Atomic__Group_2_5__0 )?
            {
             before(grammarAccess.getAtomicAccess().getGroup_2_5()); 
            // InternalMethodName.g:2974:2: ( rule__Atomic__Group_2_5__0 )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==89) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalMethodName.g:2974:3: rule__Atomic__Group_2_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Atomic__Group_2_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAtomicAccess().getGroup_2_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2__5__Impl"


    // $ANTLR start "rule__Atomic__Group_2_5__0"
    // InternalMethodName.g:2983:1: rule__Atomic__Group_2_5__0 : rule__Atomic__Group_2_5__0__Impl rule__Atomic__Group_2_5__1 ;
    public final void rule__Atomic__Group_2_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2987:1: ( rule__Atomic__Group_2_5__0__Impl rule__Atomic__Group_2_5__1 )
            // InternalMethodName.g:2988:2: rule__Atomic__Group_2_5__0__Impl rule__Atomic__Group_2_5__1
            {
            pushFollow(FOLLOW_37);
            rule__Atomic__Group_2_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_2_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2_5__0"


    // $ANTLR start "rule__Atomic__Group_2_5__0__Impl"
    // InternalMethodName.g:2995:1: rule__Atomic__Group_2_5__0__Impl : ( '[' ) ;
    public final void rule__Atomic__Group_2_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:2999:1: ( ( '[' ) )
            // InternalMethodName.g:3000:1: ( '[' )
            {
            // InternalMethodName.g:3000:1: ( '[' )
            // InternalMethodName.g:3001:2: '['
            {
             before(grammarAccess.getAtomicAccess().getLeftSquareBracketKeyword_2_5_0()); 
            match(input,89,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getLeftSquareBracketKeyword_2_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2_5__0__Impl"


    // $ANTLR start "rule__Atomic__Group_2_5__1"
    // InternalMethodName.g:3010:1: rule__Atomic__Group_2_5__1 : rule__Atomic__Group_2_5__1__Impl rule__Atomic__Group_2_5__2 ;
    public final void rule__Atomic__Group_2_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3014:1: ( rule__Atomic__Group_2_5__1__Impl rule__Atomic__Group_2_5__2 )
            // InternalMethodName.g:3015:2: rule__Atomic__Group_2_5__1__Impl rule__Atomic__Group_2_5__2
            {
            pushFollow(FOLLOW_38);
            rule__Atomic__Group_2_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Atomic__Group_2_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2_5__1"


    // $ANTLR start "rule__Atomic__Group_2_5__1__Impl"
    // InternalMethodName.g:3022:1: rule__Atomic__Group_2_5__1__Impl : ( ( rule__Atomic__SentimentAssignment_2_5_1 ) ) ;
    public final void rule__Atomic__Group_2_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3026:1: ( ( ( rule__Atomic__SentimentAssignment_2_5_1 ) ) )
            // InternalMethodName.g:3027:1: ( ( rule__Atomic__SentimentAssignment_2_5_1 ) )
            {
            // InternalMethodName.g:3027:1: ( ( rule__Atomic__SentimentAssignment_2_5_1 ) )
            // InternalMethodName.g:3028:2: ( rule__Atomic__SentimentAssignment_2_5_1 )
            {
             before(grammarAccess.getAtomicAccess().getSentimentAssignment_2_5_1()); 
            // InternalMethodName.g:3029:2: ( rule__Atomic__SentimentAssignment_2_5_1 )
            // InternalMethodName.g:3029:3: rule__Atomic__SentimentAssignment_2_5_1
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__SentimentAssignment_2_5_1();

            state._fsp--;


            }

             after(grammarAccess.getAtomicAccess().getSentimentAssignment_2_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2_5__1__Impl"


    // $ANTLR start "rule__Atomic__Group_2_5__2"
    // InternalMethodName.g:3037:1: rule__Atomic__Group_2_5__2 : rule__Atomic__Group_2_5__2__Impl ;
    public final void rule__Atomic__Group_2_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3041:1: ( rule__Atomic__Group_2_5__2__Impl )
            // InternalMethodName.g:3042:2: rule__Atomic__Group_2_5__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Atomic__Group_2_5__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2_5__2"


    // $ANTLR start "rule__Atomic__Group_2_5__2__Impl"
    // InternalMethodName.g:3048:1: rule__Atomic__Group_2_5__2__Impl : ( ']' ) ;
    public final void rule__Atomic__Group_2_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3052:1: ( ( ']' ) )
            // InternalMethodName.g:3053:1: ( ']' )
            {
            // InternalMethodName.g:3053:1: ( ']' )
            // InternalMethodName.g:3054:2: ']'
            {
             before(grammarAccess.getAtomicAccess().getRightSquareBracketKeyword_2_5_2()); 
            match(input,90,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getRightSquareBracketKeyword_2_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__Group_2_5__2__Impl"


    // $ANTLR start "rule__Property__Group__0"
    // InternalMethodName.g:3064:1: rule__Property__Group__0 : rule__Property__Group__0__Impl rule__Property__Group__1 ;
    public final void rule__Property__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3068:1: ( rule__Property__Group__0__Impl rule__Property__Group__1 )
            // InternalMethodName.g:3069:2: rule__Property__Group__0__Impl rule__Property__Group__1
            {
            pushFollow(FOLLOW_40);
            rule__Property__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Property__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__0"


    // $ANTLR start "rule__Property__Group__0__Impl"
    // InternalMethodName.g:3076:1: rule__Property__Group__0__Impl : ( ( rule__Property__FrequencyAssignment_0 )? ) ;
    public final void rule__Property__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3080:1: ( ( ( rule__Property__FrequencyAssignment_0 )? ) )
            // InternalMethodName.g:3081:1: ( ( rule__Property__FrequencyAssignment_0 )? )
            {
            // InternalMethodName.g:3081:1: ( ( rule__Property__FrequencyAssignment_0 )? )
            // InternalMethodName.g:3082:2: ( rule__Property__FrequencyAssignment_0 )?
            {
             before(grammarAccess.getPropertyAccess().getFrequencyAssignment_0()); 
            // InternalMethodName.g:3083:2: ( rule__Property__FrequencyAssignment_0 )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( ((LA30_0>=65 && LA30_0<=70)) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalMethodName.g:3083:3: rule__Property__FrequencyAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Property__FrequencyAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPropertyAccess().getFrequencyAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__0__Impl"


    // $ANTLR start "rule__Property__Group__1"
    // InternalMethodName.g:3091:1: rule__Property__Group__1 : rule__Property__Group__1__Impl ;
    public final void rule__Property__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3095:1: ( rule__Property__Group__1__Impl )
            // InternalMethodName.g:3096:2: rule__Property__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Property__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__1"


    // $ANTLR start "rule__Property__Group__1__Impl"
    // InternalMethodName.g:3102:1: rule__Property__Group__1__Impl : ( ( rule__Property__CriterionAssignment_1 ) ) ;
    public final void rule__Property__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3106:1: ( ( ( rule__Property__CriterionAssignment_1 ) ) )
            // InternalMethodName.g:3107:1: ( ( rule__Property__CriterionAssignment_1 ) )
            {
            // InternalMethodName.g:3107:1: ( ( rule__Property__CriterionAssignment_1 ) )
            // InternalMethodName.g:3108:2: ( rule__Property__CriterionAssignment_1 )
            {
             before(grammarAccess.getPropertyAccess().getCriterionAssignment_1()); 
            // InternalMethodName.g:3109:2: ( rule__Property__CriterionAssignment_1 )
            // InternalMethodName.g:3109:3: rule__Property__CriterionAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Property__CriterionAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPropertyAccess().getCriterionAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__1__Impl"


    // $ANTLR start "rule__Model__DeclarationsAssignment_1"
    // InternalMethodName.g:3118:1: rule__Model__DeclarationsAssignment_1 : ( ruleDeclaration ) ;
    public final void rule__Model__DeclarationsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3122:1: ( ( ruleDeclaration ) )
            // InternalMethodName.g:3123:2: ( ruleDeclaration )
            {
            // InternalMethodName.g:3123:2: ( ruleDeclaration )
            // InternalMethodName.g:3124:3: ruleDeclaration
            {
             before(grammarAccess.getModelAccess().getDeclarationsDeclarationParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleDeclaration();

            state._fsp--;

             after(grammarAccess.getModelAccess().getDeclarationsDeclarationParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__DeclarationsAssignment_1"


    // $ANTLR start "rule__Model__FixedDeclarationsAssignment_2"
    // InternalMethodName.g:3133:1: rule__Model__FixedDeclarationsAssignment_2 : ( ruleFixedDeclaration ) ;
    public final void rule__Model__FixedDeclarationsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3137:1: ( ( ruleFixedDeclaration ) )
            // InternalMethodName.g:3138:2: ( ruleFixedDeclaration )
            {
            // InternalMethodName.g:3138:2: ( ruleFixedDeclaration )
            // InternalMethodName.g:3139:3: ruleFixedDeclaration
            {
             before(grammarAccess.getModelAccess().getFixedDeclarationsFixedDeclarationParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleFixedDeclaration();

            state._fsp--;

             after(grammarAccess.getModelAccess().getFixedDeclarationsFixedDeclarationParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__FixedDeclarationsAssignment_2"


    // $ANTLR start "rule__Model__RulesAssignment_4"
    // InternalMethodName.g:3148:1: rule__Model__RulesAssignment_4 : ( ruleRule ) ;
    public final void rule__Model__RulesAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3152:1: ( ( ruleRule ) )
            // InternalMethodName.g:3153:2: ( ruleRule )
            {
            // InternalMethodName.g:3153:2: ( ruleRule )
            // InternalMethodName.g:3154:3: ruleRule
            {
             before(grammarAccess.getModelAccess().getRulesRuleParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleRule();

            state._fsp--;

             after(grammarAccess.getModelAccess().getRulesRuleParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__RulesAssignment_4"


    // $ANTLR start "rule__Model__ElementsAssignment_6"
    // InternalMethodName.g:3163:1: rule__Model__ElementsAssignment_6 : ( ruleCase ) ;
    public final void rule__Model__ElementsAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3167:1: ( ( ruleCase ) )
            // InternalMethodName.g:3168:2: ( ruleCase )
            {
            // InternalMethodName.g:3168:2: ( ruleCase )
            // InternalMethodName.g:3169:3: ruleCase
            {
             before(grammarAccess.getModelAccess().getElementsCaseParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleCase();

            state._fsp--;

             after(grammarAccess.getModelAccess().getElementsCaseParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__ElementsAssignment_6"


    // $ANTLR start "rule__Declaration__NameAssignment_0"
    // InternalMethodName.g:3178:1: rule__Declaration__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Declaration__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3182:1: ( ( RULE_ID ) )
            // InternalMethodName.g:3183:2: ( RULE_ID )
            {
            // InternalMethodName.g:3183:2: ( RULE_ID )
            // InternalMethodName.g:3184:3: RULE_ID
            {
             before(grammarAccess.getDeclarationAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getDeclarationAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__NameAssignment_0"


    // $ANTLR start "rule__Declaration__PathAssignment_2"
    // InternalMethodName.g:3193:1: rule__Declaration__PathAssignment_2 : ( RULE_STRING ) ;
    public final void rule__Declaration__PathAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3197:1: ( ( RULE_STRING ) )
            // InternalMethodName.g:3198:2: ( RULE_STRING )
            {
            // InternalMethodName.g:3198:2: ( RULE_STRING )
            // InternalMethodName.g:3199:3: RULE_STRING
            {
             before(grammarAccess.getDeclarationAccess().getPathSTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getDeclarationAccess().getPathSTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__PathAssignment_2"


    // $ANTLR start "rule__FixedDeclaration__KindAssignment_0"
    // InternalMethodName.g:3208:1: rule__FixedDeclaration__KindAssignment_0 : ( ruleKind ) ;
    public final void rule__FixedDeclaration__KindAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3212:1: ( ( ruleKind ) )
            // InternalMethodName.g:3213:2: ( ruleKind )
            {
            // InternalMethodName.g:3213:2: ( ruleKind )
            // InternalMethodName.g:3214:3: ruleKind
            {
             before(grammarAccess.getFixedDeclarationAccess().getKindKindEnumRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleKind();

            state._fsp--;

             after(grammarAccess.getFixedDeclarationAccess().getKindKindEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedDeclaration__KindAssignment_0"


    // $ANTLR start "rule__FixedDeclaration__PathAssignment_2"
    // InternalMethodName.g:3223:1: rule__FixedDeclaration__PathAssignment_2 : ( RULE_STRING ) ;
    public final void rule__FixedDeclaration__PathAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3227:1: ( ( RULE_STRING ) )
            // InternalMethodName.g:3228:2: ( RULE_STRING )
            {
            // InternalMethodName.g:3228:2: ( RULE_STRING )
            // InternalMethodName.g:3229:3: RULE_STRING
            {
             before(grammarAccess.getFixedDeclarationAccess().getPathSTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getFixedDeclarationAccess().getPathSTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedDeclaration__PathAssignment_2"


    // $ANTLR start "rule__Rule__NameAssignment_0_1"
    // InternalMethodName.g:3238:1: rule__Rule__NameAssignment_0_1 : ( RULE_ID ) ;
    public final void rule__Rule__NameAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3242:1: ( ( RULE_ID ) )
            // InternalMethodName.g:3243:2: ( RULE_ID )
            {
            // InternalMethodName.g:3243:2: ( RULE_ID )
            // InternalMethodName.g:3244:3: RULE_ID
            {
             before(grammarAccess.getRuleAccess().getNameIDTerminalRuleCall_0_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getNameIDTerminalRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__NameAssignment_0_1"


    // $ANTLR start "rule__Rule__KindAssignment_0_3"
    // InternalMethodName.g:3253:1: rule__Rule__KindAssignment_0_3 : ( ruleKind ) ;
    public final void rule__Rule__KindAssignment_0_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3257:1: ( ( ruleKind ) )
            // InternalMethodName.g:3258:2: ( ruleKind )
            {
            // InternalMethodName.g:3258:2: ( ruleKind )
            // InternalMethodName.g:3259:3: ruleKind
            {
             before(grammarAccess.getRuleAccess().getKindKindEnumRuleCall_0_3_0()); 
            pushFollow(FOLLOW_2);
            ruleKind();

            state._fsp--;

             after(grammarAccess.getRuleAccess().getKindKindEnumRuleCall_0_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__KindAssignment_0_3"


    // $ANTLR start "rule__Rule__DeclsAssignment_0_6"
    // InternalMethodName.g:3268:1: rule__Rule__DeclsAssignment_0_6 : ( ( RULE_ID ) ) ;
    public final void rule__Rule__DeclsAssignment_0_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3272:1: ( ( ( RULE_ID ) ) )
            // InternalMethodName.g:3273:2: ( ( RULE_ID ) )
            {
            // InternalMethodName.g:3273:2: ( ( RULE_ID ) )
            // InternalMethodName.g:3274:3: ( RULE_ID )
            {
             before(grammarAccess.getRuleAccess().getDeclsDeclarationCrossReference_0_6_0()); 
            // InternalMethodName.g:3275:3: ( RULE_ID )
            // InternalMethodName.g:3276:4: RULE_ID
            {
             before(grammarAccess.getRuleAccess().getDeclsDeclarationIDTerminalRuleCall_0_6_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getDeclsDeclarationIDTerminalRuleCall_0_6_0_1()); 

            }

             after(grammarAccess.getRuleAccess().getDeclsDeclarationCrossReference_0_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__DeclsAssignment_0_6"


    // $ANTLR start "rule__Rule__DeclsAssignment_0_7_1"
    // InternalMethodName.g:3287:1: rule__Rule__DeclsAssignment_0_7_1 : ( ( RULE_ID ) ) ;
    public final void rule__Rule__DeclsAssignment_0_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3291:1: ( ( ( RULE_ID ) ) )
            // InternalMethodName.g:3292:2: ( ( RULE_ID ) )
            {
            // InternalMethodName.g:3292:2: ( ( RULE_ID ) )
            // InternalMethodName.g:3293:3: ( RULE_ID )
            {
             before(grammarAccess.getRuleAccess().getDeclsDeclarationCrossReference_0_7_1_0()); 
            // InternalMethodName.g:3294:3: ( RULE_ID )
            // InternalMethodName.g:3295:4: RULE_ID
            {
             before(grammarAccess.getRuleAccess().getDeclsDeclarationIDTerminalRuleCall_0_7_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getDeclsDeclarationIDTerminalRuleCall_0_7_1_0_1()); 

            }

             after(grammarAccess.getRuleAccess().getDeclsDeclarationCrossReference_0_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__DeclsAssignment_0_7_1"


    // $ANTLR start "rule__Rule__CompOpAssignment_0_11"
    // InternalMethodName.g:3306:1: rule__Rule__CompOpAssignment_0_11 : ( ( rule__Rule__CompOpAlternatives_0_11_0 ) ) ;
    public final void rule__Rule__CompOpAssignment_0_11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3310:1: ( ( ( rule__Rule__CompOpAlternatives_0_11_0 ) ) )
            // InternalMethodName.g:3311:2: ( ( rule__Rule__CompOpAlternatives_0_11_0 ) )
            {
            // InternalMethodName.g:3311:2: ( ( rule__Rule__CompOpAlternatives_0_11_0 ) )
            // InternalMethodName.g:3312:3: ( rule__Rule__CompOpAlternatives_0_11_0 )
            {
             before(grammarAccess.getRuleAccess().getCompOpAlternatives_0_11_0()); 
            // InternalMethodName.g:3313:3: ( rule__Rule__CompOpAlternatives_0_11_0 )
            // InternalMethodName.g:3313:4: rule__Rule__CompOpAlternatives_0_11_0
            {
            pushFollow(FOLLOW_2);
            rule__Rule__CompOpAlternatives_0_11_0();

            state._fsp--;


            }

             after(grammarAccess.getRuleAccess().getCompOpAlternatives_0_11_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__CompOpAssignment_0_11"


    // $ANTLR start "rule__Rule__CompValueAssignment_0_12"
    // InternalMethodName.g:3321:1: rule__Rule__CompValueAssignment_0_12 : ( RULE_INT ) ;
    public final void rule__Rule__CompValueAssignment_0_12() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3325:1: ( ( RULE_INT ) )
            // InternalMethodName.g:3326:2: ( RULE_INT )
            {
            // InternalMethodName.g:3326:2: ( RULE_INT )
            // InternalMethodName.g:3327:3: RULE_INT
            {
             before(grammarAccess.getRuleAccess().getCompValueINTTerminalRuleCall_0_12_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getCompValueINTTerminalRuleCall_0_12_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__CompValueAssignment_0_12"


    // $ANTLR start "rule__Rule__NameAssignment_1_2"
    // InternalMethodName.g:3336:1: rule__Rule__NameAssignment_1_2 : ( RULE_ID ) ;
    public final void rule__Rule__NameAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3340:1: ( ( RULE_ID ) )
            // InternalMethodName.g:3341:2: ( RULE_ID )
            {
            // InternalMethodName.g:3341:2: ( RULE_ID )
            // InternalMethodName.g:3342:3: RULE_ID
            {
             before(grammarAccess.getRuleAccess().getNameIDTerminalRuleCall_1_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getNameIDTerminalRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__NameAssignment_1_2"


    // $ANTLR start "rule__Case__KindAssignment_2"
    // InternalMethodName.g:3351:1: rule__Case__KindAssignment_2 : ( ruleKind ) ;
    public final void rule__Case__KindAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3355:1: ( ( ruleKind ) )
            // InternalMethodName.g:3356:2: ( ruleKind )
            {
            // InternalMethodName.g:3356:2: ( ruleKind )
            // InternalMethodName.g:3357:3: ruleKind
            {
             before(grammarAccess.getCaseAccess().getKindKindEnumRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleKind();

            state._fsp--;

             after(grammarAccess.getCaseAccess().getKindKindEnumRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__KindAssignment_2"


    // $ANTLR start "rule__Case__ConditionAssignment_3"
    // InternalMethodName.g:3366:1: rule__Case__ConditionAssignment_3 : ( ruleExpr ) ;
    public final void rule__Case__ConditionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3370:1: ( ( ruleExpr ) )
            // InternalMethodName.g:3371:2: ( ruleExpr )
            {
            // InternalMethodName.g:3371:2: ( ruleExpr )
            // InternalMethodName.g:3372:3: ruleExpr
            {
             before(grammarAccess.getCaseAccess().getConditionExprParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleExpr();

            state._fsp--;

             after(grammarAccess.getCaseAccess().getConditionExprParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__ConditionAssignment_3"


    // $ANTLR start "rule__Case__PropertiesAssignment_5"
    // InternalMethodName.g:3381:1: rule__Case__PropertiesAssignment_5 : ( ruleProperty ) ;
    public final void rule__Case__PropertiesAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3385:1: ( ( ruleProperty ) )
            // InternalMethodName.g:3386:2: ( ruleProperty )
            {
            // InternalMethodName.g:3386:2: ( ruleProperty )
            // InternalMethodName.g:3387:3: ruleProperty
            {
             before(grammarAccess.getCaseAccess().getPropertiesPropertyParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleProperty();

            state._fsp--;

             after(grammarAccess.getCaseAccess().getPropertiesPropertyParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__PropertiesAssignment_5"


    // $ANTLR start "rule__Or__RightAssignment_1_2"
    // InternalMethodName.g:3396:1: rule__Or__RightAssignment_1_2 : ( ruleAnd ) ;
    public final void rule__Or__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3400:1: ( ( ruleAnd ) )
            // InternalMethodName.g:3401:2: ( ruleAnd )
            {
            // InternalMethodName.g:3401:2: ( ruleAnd )
            // InternalMethodName.g:3402:3: ruleAnd
            {
             before(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__RightAssignment_1_2"


    // $ANTLR start "rule__And__ConcatKindAssignment_1_1"
    // InternalMethodName.g:3411:1: rule__And__ConcatKindAssignment_1_1 : ( ( rule__And__ConcatKindAlternatives_1_1_0 ) ) ;
    public final void rule__And__ConcatKindAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3415:1: ( ( ( rule__And__ConcatKindAlternatives_1_1_0 ) ) )
            // InternalMethodName.g:3416:2: ( ( rule__And__ConcatKindAlternatives_1_1_0 ) )
            {
            // InternalMethodName.g:3416:2: ( ( rule__And__ConcatKindAlternatives_1_1_0 ) )
            // InternalMethodName.g:3417:3: ( rule__And__ConcatKindAlternatives_1_1_0 )
            {
             before(grammarAccess.getAndAccess().getConcatKindAlternatives_1_1_0()); 
            // InternalMethodName.g:3418:3: ( rule__And__ConcatKindAlternatives_1_1_0 )
            // InternalMethodName.g:3418:4: rule__And__ConcatKindAlternatives_1_1_0
            {
            pushFollow(FOLLOW_2);
            rule__And__ConcatKindAlternatives_1_1_0();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getConcatKindAlternatives_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__ConcatKindAssignment_1_1"


    // $ANTLR start "rule__And__RightAssignment_1_2"
    // InternalMethodName.g:3426:1: rule__And__RightAssignment_1_2 : ( rulePrimary ) ;
    public final void rule__And__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3430:1: ( ( rulePrimary ) )
            // InternalMethodName.g:3431:2: ( rulePrimary )
            {
            // InternalMethodName.g:3431:2: ( rulePrimary )
            // InternalMethodName.g:3432:3: rulePrimary
            {
             before(grammarAccess.getAndAccess().getRightPrimaryParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getAndAccess().getRightPrimaryParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__RightAssignment_1_2"


    // $ANTLR start "rule__GroupExpr__HasNotAssignment_0"
    // InternalMethodName.g:3441:1: rule__GroupExpr__HasNotAssignment_0 : ( ( '!' ) ) ;
    public final void rule__GroupExpr__HasNotAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3445:1: ( ( ( '!' ) ) )
            // InternalMethodName.g:3446:2: ( ( '!' ) )
            {
            // InternalMethodName.g:3446:2: ( ( '!' ) )
            // InternalMethodName.g:3447:3: ( '!' )
            {
             before(grammarAccess.getGroupExprAccess().getHasNotExclamationMarkKeyword_0_0()); 
            // InternalMethodName.g:3448:3: ( '!' )
            // InternalMethodName.g:3449:4: '!'
            {
             before(grammarAccess.getGroupExprAccess().getHasNotExclamationMarkKeyword_0_0()); 
            match(input,92,FOLLOW_2); 
             after(grammarAccess.getGroupExprAccess().getHasNotExclamationMarkKeyword_0_0()); 

            }

             after(grammarAccess.getGroupExprAccess().getHasNotExclamationMarkKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupExpr__HasNotAssignment_0"


    // $ANTLR start "rule__GroupExpr__ExprAssignment_2"
    // InternalMethodName.g:3460:1: rule__GroupExpr__ExprAssignment_2 : ( ruleExpr ) ;
    public final void rule__GroupExpr__ExprAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3464:1: ( ( ruleExpr ) )
            // InternalMethodName.g:3465:2: ( ruleExpr )
            {
            // InternalMethodName.g:3465:2: ( ruleExpr )
            // InternalMethodName.g:3466:3: ruleExpr
            {
             before(grammarAccess.getGroupExprAccess().getExprExprParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExpr();

            state._fsp--;

             after(grammarAccess.getGroupExprAccess().getExprExprParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupExpr__ExprAssignment_2"


    // $ANTLR start "rule__GroupExpr__CardAssignment_4"
    // InternalMethodName.g:3475:1: rule__GroupExpr__CardAssignment_4 : ( ruleCardinalityModifier ) ;
    public final void rule__GroupExpr__CardAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3479:1: ( ( ruleCardinalityModifier ) )
            // InternalMethodName.g:3480:2: ( ruleCardinalityModifier )
            {
            // InternalMethodName.g:3480:2: ( ruleCardinalityModifier )
            // InternalMethodName.g:3481:3: ruleCardinalityModifier
            {
             before(grammarAccess.getGroupExprAccess().getCardCardinalityModifierEnumRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleCardinalityModifier();

            state._fsp--;

             after(grammarAccess.getGroupExprAccess().getCardCardinalityModifierEnumRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupExpr__CardAssignment_4"


    // $ANTLR start "rule__Atomic__HasNotAssignment_0_1"
    // InternalMethodName.g:3490:1: rule__Atomic__HasNotAssignment_0_1 : ( ( '!' ) ) ;
    public final void rule__Atomic__HasNotAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3494:1: ( ( ( '!' ) ) )
            // InternalMethodName.g:3495:2: ( ( '!' ) )
            {
            // InternalMethodName.g:3495:2: ( ( '!' ) )
            // InternalMethodName.g:3496:3: ( '!' )
            {
             before(grammarAccess.getAtomicAccess().getHasNotExclamationMarkKeyword_0_1_0()); 
            // InternalMethodName.g:3497:3: ( '!' )
            // InternalMethodName.g:3498:4: '!'
            {
             before(grammarAccess.getAtomicAccess().getHasNotExclamationMarkKeyword_0_1_0()); 
            match(input,92,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getHasNotExclamationMarkKeyword_0_1_0()); 

            }

             after(grammarAccess.getAtomicAccess().getHasNotExclamationMarkKeyword_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__HasNotAssignment_0_1"


    // $ANTLR start "rule__Atomic__ValueAssignment_0_2"
    // InternalMethodName.g:3509:1: rule__Atomic__ValueAssignment_0_2 : ( RULE_STRING ) ;
    public final void rule__Atomic__ValueAssignment_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3513:1: ( ( RULE_STRING ) )
            // InternalMethodName.g:3514:2: ( RULE_STRING )
            {
            // InternalMethodName.g:3514:2: ( RULE_STRING )
            // InternalMethodName.g:3515:3: RULE_STRING
            {
             before(grammarAccess.getAtomicAccess().getValueSTRINGTerminalRuleCall_0_2_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getValueSTRINGTerminalRuleCall_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__ValueAssignment_0_2"


    // $ANTLR start "rule__Atomic__CardAssignment_0_3"
    // InternalMethodName.g:3524:1: rule__Atomic__CardAssignment_0_3 : ( ruleCardinalityModifier ) ;
    public final void rule__Atomic__CardAssignment_0_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3528:1: ( ( ruleCardinalityModifier ) )
            // InternalMethodName.g:3529:2: ( ruleCardinalityModifier )
            {
            // InternalMethodName.g:3529:2: ( ruleCardinalityModifier )
            // InternalMethodName.g:3530:3: ruleCardinalityModifier
            {
             before(grammarAccess.getAtomicAccess().getCardCardinalityModifierEnumRuleCall_0_3_0()); 
            pushFollow(FOLLOW_2);
            ruleCardinalityModifier();

            state._fsp--;

             after(grammarAccess.getAtomicAccess().getCardCardinalityModifierEnumRuleCall_0_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__CardAssignment_0_3"


    // $ANTLR start "rule__Atomic__SentimentAssignment_0_4_1"
    // InternalMethodName.g:3539:1: rule__Atomic__SentimentAssignment_0_4_1 : ( ruleSentiment ) ;
    public final void rule__Atomic__SentimentAssignment_0_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3543:1: ( ( ruleSentiment ) )
            // InternalMethodName.g:3544:2: ( ruleSentiment )
            {
            // InternalMethodName.g:3544:2: ( ruleSentiment )
            // InternalMethodName.g:3545:3: ruleSentiment
            {
             before(grammarAccess.getAtomicAccess().getSentimentSentimentEnumRuleCall_0_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSentiment();

            state._fsp--;

             after(grammarAccess.getAtomicAccess().getSentimentSentimentEnumRuleCall_0_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__SentimentAssignment_0_4_1"


    // $ANTLR start "rule__Atomic__HasNotAssignment_1_1"
    // InternalMethodName.g:3554:1: rule__Atomic__HasNotAssignment_1_1 : ( ( '!' ) ) ;
    public final void rule__Atomic__HasNotAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3558:1: ( ( ( '!' ) ) )
            // InternalMethodName.g:3559:2: ( ( '!' ) )
            {
            // InternalMethodName.g:3559:2: ( ( '!' ) )
            // InternalMethodName.g:3560:3: ( '!' )
            {
             before(grammarAccess.getAtomicAccess().getHasNotExclamationMarkKeyword_1_1_0()); 
            // InternalMethodName.g:3561:3: ( '!' )
            // InternalMethodName.g:3562:4: '!'
            {
             before(grammarAccess.getAtomicAccess().getHasNotExclamationMarkKeyword_1_1_0()); 
            match(input,92,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getHasNotExclamationMarkKeyword_1_1_0()); 

            }

             after(grammarAccess.getAtomicAccess().getHasNotExclamationMarkKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__HasNotAssignment_1_1"


    // $ANTLR start "rule__Atomic__PosAssignment_1_2"
    // InternalMethodName.g:3573:1: rule__Atomic__PosAssignment_1_2 : ( rulePOS ) ;
    public final void rule__Atomic__PosAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3577:1: ( ( rulePOS ) )
            // InternalMethodName.g:3578:2: ( rulePOS )
            {
            // InternalMethodName.g:3578:2: ( rulePOS )
            // InternalMethodName.g:3579:3: rulePOS
            {
             before(grammarAccess.getAtomicAccess().getPosPOSEnumRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            rulePOS();

            state._fsp--;

             after(grammarAccess.getAtomicAccess().getPosPOSEnumRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__PosAssignment_1_2"


    // $ANTLR start "rule__Atomic__CardAssignment_1_3"
    // InternalMethodName.g:3588:1: rule__Atomic__CardAssignment_1_3 : ( ruleCardinalityModifier ) ;
    public final void rule__Atomic__CardAssignment_1_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3592:1: ( ( ruleCardinalityModifier ) )
            // InternalMethodName.g:3593:2: ( ruleCardinalityModifier )
            {
            // InternalMethodName.g:3593:2: ( ruleCardinalityModifier )
            // InternalMethodName.g:3594:3: ruleCardinalityModifier
            {
             before(grammarAccess.getAtomicAccess().getCardCardinalityModifierEnumRuleCall_1_3_0()); 
            pushFollow(FOLLOW_2);
            ruleCardinalityModifier();

            state._fsp--;

             after(grammarAccess.getAtomicAccess().getCardCardinalityModifierEnumRuleCall_1_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__CardAssignment_1_3"


    // $ANTLR start "rule__Atomic__SentimentAssignment_1_4_1"
    // InternalMethodName.g:3603:1: rule__Atomic__SentimentAssignment_1_4_1 : ( ruleSentiment ) ;
    public final void rule__Atomic__SentimentAssignment_1_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3607:1: ( ( ruleSentiment ) )
            // InternalMethodName.g:3608:2: ( ruleSentiment )
            {
            // InternalMethodName.g:3608:2: ( ruleSentiment )
            // InternalMethodName.g:3609:3: ruleSentiment
            {
             before(grammarAccess.getAtomicAccess().getSentimentSentimentEnumRuleCall_1_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSentiment();

            state._fsp--;

             after(grammarAccess.getAtomicAccess().getSentimentSentimentEnumRuleCall_1_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__SentimentAssignment_1_4_1"


    // $ANTLR start "rule__Atomic__HasNotAssignment_2_1"
    // InternalMethodName.g:3618:1: rule__Atomic__HasNotAssignment_2_1 : ( ( '!' ) ) ;
    public final void rule__Atomic__HasNotAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3622:1: ( ( ( '!' ) ) )
            // InternalMethodName.g:3623:2: ( ( '!' ) )
            {
            // InternalMethodName.g:3623:2: ( ( '!' ) )
            // InternalMethodName.g:3624:3: ( '!' )
            {
             before(grammarAccess.getAtomicAccess().getHasNotExclamationMarkKeyword_2_1_0()); 
            // InternalMethodName.g:3625:3: ( '!' )
            // InternalMethodName.g:3626:4: '!'
            {
             before(grammarAccess.getAtomicAccess().getHasNotExclamationMarkKeyword_2_1_0()); 
            match(input,92,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getHasNotExclamationMarkKeyword_2_1_0()); 

            }

             after(grammarAccess.getAtomicAccess().getHasNotExclamationMarkKeyword_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__HasNotAssignment_2_1"


    // $ANTLR start "rule__Atomic__ValueAssignment_2_3"
    // InternalMethodName.g:3637:1: rule__Atomic__ValueAssignment_2_3 : ( RULE_STRING ) ;
    public final void rule__Atomic__ValueAssignment_2_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3641:1: ( ( RULE_STRING ) )
            // InternalMethodName.g:3642:2: ( RULE_STRING )
            {
            // InternalMethodName.g:3642:2: ( RULE_STRING )
            // InternalMethodName.g:3643:3: RULE_STRING
            {
             before(grammarAccess.getAtomicAccess().getValueSTRINGTerminalRuleCall_2_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getAtomicAccess().getValueSTRINGTerminalRuleCall_2_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__ValueAssignment_2_3"


    // $ANTLR start "rule__Atomic__CardAssignment_2_4"
    // InternalMethodName.g:3652:1: rule__Atomic__CardAssignment_2_4 : ( ruleCardinalityModifier ) ;
    public final void rule__Atomic__CardAssignment_2_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3656:1: ( ( ruleCardinalityModifier ) )
            // InternalMethodName.g:3657:2: ( ruleCardinalityModifier )
            {
            // InternalMethodName.g:3657:2: ( ruleCardinalityModifier )
            // InternalMethodName.g:3658:3: ruleCardinalityModifier
            {
             before(grammarAccess.getAtomicAccess().getCardCardinalityModifierEnumRuleCall_2_4_0()); 
            pushFollow(FOLLOW_2);
            ruleCardinalityModifier();

            state._fsp--;

             after(grammarAccess.getAtomicAccess().getCardCardinalityModifierEnumRuleCall_2_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__CardAssignment_2_4"


    // $ANTLR start "rule__Atomic__SentimentAssignment_2_5_1"
    // InternalMethodName.g:3667:1: rule__Atomic__SentimentAssignment_2_5_1 : ( ruleSentiment ) ;
    public final void rule__Atomic__SentimentAssignment_2_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3671:1: ( ( ruleSentiment ) )
            // InternalMethodName.g:3672:2: ( ruleSentiment )
            {
            // InternalMethodName.g:3672:2: ( ruleSentiment )
            // InternalMethodName.g:3673:3: ruleSentiment
            {
             before(grammarAccess.getAtomicAccess().getSentimentSentimentEnumRuleCall_2_5_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSentiment();

            state._fsp--;

             after(grammarAccess.getAtomicAccess().getSentimentSentimentEnumRuleCall_2_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atomic__SentimentAssignment_2_5_1"


    // $ANTLR start "rule__Property__FrequencyAssignment_0"
    // InternalMethodName.g:3682:1: rule__Property__FrequencyAssignment_0 : ( ruleFrequency ) ;
    public final void rule__Property__FrequencyAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3686:1: ( ( ruleFrequency ) )
            // InternalMethodName.g:3687:2: ( ruleFrequency )
            {
            // InternalMethodName.g:3687:2: ( ruleFrequency )
            // InternalMethodName.g:3688:3: ruleFrequency
            {
             before(grammarAccess.getPropertyAccess().getFrequencyFrequencyEnumRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleFrequency();

            state._fsp--;

             after(grammarAccess.getPropertyAccess().getFrequencyFrequencyEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__FrequencyAssignment_0"


    // $ANTLR start "rule__Property__CriterionAssignment_1"
    // InternalMethodName.g:3697:1: rule__Property__CriterionAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__Property__CriterionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMethodName.g:3701:1: ( ( ( RULE_ID ) ) )
            // InternalMethodName.g:3702:2: ( ( RULE_ID ) )
            {
            // InternalMethodName.g:3702:2: ( ( RULE_ID ) )
            // InternalMethodName.g:3703:3: ( RULE_ID )
            {
             before(grammarAccess.getPropertyAccess().getCriterionRuleCrossReference_1_0()); 
            // InternalMethodName.g:3704:3: ( RULE_ID )
            // InternalMethodName.g:3705:4: RULE_ID
            {
             before(grammarAccess.getPropertyAccess().getCriterionRuleIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPropertyAccess().getCriterionRuleIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getPropertyAccess().getCriterionRuleCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__CriterionAssignment_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x00000000007C0010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x00000000007C0012L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000A00L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000800L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000000L,0x0000000000200000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000002L,0x0000000000200000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000000L,0x0000000000001000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000000L,0x0000000000088000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000000002L,0x0000000000080000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000000000L,0x0000000000010000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x000000000000F800L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000000000L,0x0000000000100000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x1FFFFFFFF8000020L,0x0000000018800000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000000000010L,0x000000000004007EL});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000000000012L,0x000000000000007EL});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000000000000L,0x0000000000400000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000000000002L,0x0000000000400000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000000030002L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000000000000L,0x0000000010800000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000000000000L,0x0000000001000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0xE000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000000000020L,0x0000000010000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0xE000000000000000L,0x0000000002000001L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000000007800000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x1FFFFFFFF8000000L,0x0000000010000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000000000000010L,0x000000000000007EL});

}